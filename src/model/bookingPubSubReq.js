const uuidv4 = require("uuid/v4");
const fp = require("fastify-plugin");

module.exports = fp((fastify, options, next) => {
  const bookingRequest = async (request, response) => {
    const { uuid, storeName, userId, startTime, endTime } = response;
    const { emailId, source, userTel, userName } = request;

    const localSlotStartTime = new Date(startTime);
    const localSlotEndTime = new Date(endTime);
    const encyptRequest = {
      userId: userId.replace(/\./g, "").split("-")[0],
      userName,
      userTel,
      emailId
    };
    const encyptResponse = await fastify.encryption.encryptData(
      encyptRequest,
      fastify.config
    );
    const pubSubRequest = {
      sourceType: source,
      sourceSubtype: "Fila Virtual / Application ",
      mallName: storeName,
      sourceName: "booking",
      customerIdentityType: "01",
      customerIdentityNumber: encyptResponse.userId,
      customerName: encyptResponse.userName,
      customerLastName: encyptResponse.userName,
      customerPhone: encyptResponse.userTel,
      customerEmail: encyptResponse.emailId,
      bookingId: uuid,
      bookingDateTime: localSlotStartTime.toISOString(),
      bookingDateTimeToGetOut: localSlotEndTime.toISOString(),
      referenceSiteUrl: ""
    };
    return pubSubRequest;
  };

  const bookingAttributes = async (
    response,
    storeData,
    customerIdentityNumber
  ) => {
    const { businessUnit } = response;
    const { country } = storeData;
    const now = new Date();
    const timeInSeconds = parseInt(now.getTime() / 1000);
    const pubSubAttrs = {
      eventId: uuidv4().toString(),
      eventType: "booking",
      entityId: customerIdentityNumber.toString(),
      entityType: "Booking",
      timeStamp: timeInSeconds.toString(),
      dateTime: now.toISOString(),
      version: "2.0",
      country: country.toLowerCase(),
      commerce: businessUnit.toLowerCase(),
      channel: "web",
      domain: "cust",
      capability: "csvm",
      issuer: "Fila Virtual",
      mimeType: "application/json"
    };
    return pubSubAttrs;
  };
  fastify.decorate("bookingreq", {
    bookingRequest,
    bookingAttributes
  });
  next();
});
