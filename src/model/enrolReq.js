const uuidv4 = require("uuid/v4");
const fp = require("fastify-plugin");

module.exports = fp((fastify, options, next) => {
  const enrolRequest = async (request, response, storeData) => {
    const { storeName, userId } = response;
    const { emailId, source, userTel, userName } = request;
    const { termsCondition } = storeData;
    const now = new Date();

    const encyptRequest = {
      userId: userId.replace(/\./g, "").split("-")[0],
      userName,
      userTel,
      emailId
    };
    const encyptResponse = await fastify.encryption.encryptData(
      encyptRequest,
      fastify.config
    );
    const pubSubRequest = {
      sourceType: source,
      sourceSubtype: "Fila Virtual / Application ",
      mallName: storeName,
      sourceName: "customerEnrolled",
      customerIdentityNumber: encyptResponse.userId,
      customerName: encyptResponse.userName,
      customerLastName: encyptResponse.userName,
      customerPhone: encyptResponse.userTel,
      customerEmail: encyptResponse.emailId,
      customerIdentityType: "01",
      acceptanceInfo: [
        {
          AcceptanceType: "Terms&Conditions",
          AcceptanceVersion: "1.0",
          AcceptanceName: termsCondition,
          AcceptanceDate: now.toISOString()
        }
      ],
      sourceSiteInfo: {
        siteUrl: ""
      }
    };
    return pubSubRequest;
  };

  const enrolAttributes = async (
    response,
    storeData,
    customerIdentityNumber
  ) => {
    const { businessUnit } = response;
    const { country } = storeData;
    const now = new Date();
    const timeInSeconds = parseInt(now.getTime() / 1000);
    const pubSubAttrs = {
      eventId: uuidv4().toString(),
      eventType: "customerEnrolled",
      entityId: customerIdentityNumber.toString(),
      entityType: "customer",
      timeStamp: timeInSeconds.toString(),
      dateTime: now.toISOString(),
      version: "3.0",
      country: country.toLowerCase(),
      commerce: businessUnit.toLowerCase(),
      channel: "web",
      domain: "cust",
      capability: "csvm",
      issuer: "Fila Virtual",
      mimeType: "application/json"
    };
    return pubSubAttrs;
  };
  fastify.decorate("enrolreq", {
    enrolRequest,
    enrolAttributes
  });
  next();
});
