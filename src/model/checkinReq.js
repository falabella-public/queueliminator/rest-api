const uuidv4 = require("uuid/v4");
const fp = require("fastify-plugin");

module.exports = fp((fastify, options, next) => {
  const checkinRequest = async (response, storeData) => {
    const {
      uuid,
      userId,
      customerPhoneNumber,
      customerName,
      source,
      emailId
    } = response;
    const { storeName } = storeData;
    const now = new Date();

    const encyptRequest = {
      userId: userId.replace(/\./g, "").split("-")[0],
      userName: customerName,
      userTel: customerPhoneNumber,
      emailId
    };
    const encyptResponse = await fastify.encryption.encryptData(
      encyptRequest,
      fastify.config
    );
    const pubSubRequest = {
      sourceType: source,
      sourceSubtype: "Fila Virtual / Application ",
      mallName: storeName,
      sourceName: "checkin",
      customerIdentityType: "01",
      customerIdentityNumber: encyptResponse.userId,
      customerName: encyptResponse.userName,
      customerLastName: encyptResponse.userName,
      customerPhone: encyptResponse.userTel,
      customerEmail: encyptResponse.emailId,
      bookingId: uuid,
      eventType: "checkin",
      eventDateTime: now.toISOString()
    };
    return pubSubRequest;
  };

  const checkinAttributes = async (storeData, customerIdentityNumber) => {
    const { country, businessUnit } = storeData;
    const now = new Date();
    const timeInSeconds = parseInt(now.getTime() / 1000);
    const pubSubAttributes = {
      eventId: uuidv4().toString(),
      eventType: "checkin",
      entityId: customerIdentityNumber,
      entityType: "CheckIn",
      timeStamp: timeInSeconds.toString(),
      dateTime: now.toISOString(),
      version: "2.0",
      country: country.toLowerCase(),
      commerce: businessUnit.toLowerCase(),
      channel: "web",
      domain: "cust",
      capability: "csvm",
      issuer: "Fila Virtual",
      mimeType: "application/json"
    };
    return pubSubAttributes;
  };
  fastify.decorate("checkinreq", {
    checkinRequest,
    checkinAttributes
  });
  next();
});
