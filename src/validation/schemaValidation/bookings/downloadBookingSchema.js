const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to download the booking details for a specific date range and filters at country, business unit or store level."
schema.body = {
  "type": "object",
  "required": [],
  "properties": {
    "storeid": {
      "type": "string"
    },
    "selectedCountry": {
      "type": "string"
    },
    "selectedBu": {
      "type": "string"
    },
    "userId": {
      "type": "string"
    },
    "fromDate": {
      "type": "string",
      "nullable": true
    },
    "toDate": {
      "type": "string",
      "nullable": true
    },
    "startTime": {
      "type": "string"
    },
    "endTime": {
      "type": "string"
    }
  }
};
schema.response = {
    ...schema.response,
    "200": {
        "type": "object",
        "required": [],
        "properties": {
          "bookings": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [],
              "properties": {
                "country": {
                  "type": "string",
                  "nullable": true
                },
                "businessUnit": {
                  "type": "string",
                  "nullable": true
                },
                "location": {
                  "type": "string"
                },
                "storeName": {
                  "type": "string"
                },
                "userId": {
                  "type": "string",
                  "nullable": true
                },
                "noOfPeople": {
                  "type": "number"
                },
                "bookingTime": {
                  "type": "string"
                },
                "startTime": {
                  "type": "string"
                },
                "endTime": {
                  "type": "string"
                },
                "checkinTime": {
                    "type": "string",
                    "nullable": true,
                },
                "checkoutTime": {
                    "type": "string",
                    "nullable": true,
                },
                "status": {
                  "type": "string"
                },
                "emailId": {
                  "type": "string",
                  "nullable": true
                },
                "customerName": {
                  "type": "string",
                  "nullable": true
                },
                "customerPhoneNumber": {
                  "type": "string",
                  "nullable": true
                },
                "acceptTermsConditions": {
                  "type": "boolean",
                  "nullable": true
                },
                "acceptUseOfMyInformation": {
                  "type": "boolean",
                  "nullable": true
                },
                "acceptReadTermsConditions": {
                  "type": "boolean",
                  "nullable": true
                },
                "serviceType": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  },
                  "nullable": true
                }
              }
            }
          }
        }
      }
}
module.exports = schema;
