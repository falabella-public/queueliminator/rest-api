const updateBookingStatusSchema = require("./updateBookingStatusSchema");
const postBookingsSchema = require("./postBookingsSchema");
const getBookingQrSchema = require("./getBookingQrSchema");
const getBookingServiceQrSchema = require("./getBookingServiceQrSchema");
const getLatestBookingsSchema = require("./getLatestBookingsSchema");
const getBookingDetailSchema = require("./getBookingDetailSchema");
const downloadBookingSchema = require("./downloadBookingSchema");
const downloadWalkinSchema = require("./downloadWalkinSchema");
const getAllUserBookingsSchema = require("./getAllUserBookingsSchema");
const expirePastBookingsSchema = require("./expirePastBookingsSchema");
const getBookingsByBuSchema = require("../bookings/getBookingsByBuSchema");
module.exports = {
  updateBookingStatusSchema,
  postBookingsSchema,
  getBookingQrSchema,
  getBookingServiceQrSchema,
  getLatestBookingsSchema,
  getBookingDetailSchema,
  downloadBookingSchema,
  downloadWalkinSchema,
  getAllUserBookingsSchema,
  expirePastBookingsSchema,
  getBookingsByBuSchema
};
