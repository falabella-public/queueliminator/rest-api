const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to get total bokings, check-in, checkout, walk-in and walkout based on business unit, country, store or for all."
schema.body = {
  "type": "object",
  "required": [],
  "properties": {
    "fromDate": {
      "type": "string"
    },
    "toDate": {
      "type": "string",
      "nullable": true
    },
    "startTime": {
      "type": "string"
    },
    "endTime": {
      "type": "string"
    },
    "location": {
      "type": "string"
    },
    "storeId": {
      "type": "string"
    },
    "emailid": {
      "type": "string",
      "nullable": true
    },
    "country": {
      "type": "string",
      "nullable": true
    },
    "businessUnit": {
      "type": "string",
      "nullable": true
    }
  }
}
schema.response = {
  ...schema.response,
  "200": {
    "type": "object",
    "required": [],
    "properties": {
      "totalStores": {
        "type": "object",
        "required": [],
        "properties": {
          "count": {
            "type": "string"
          }
        }
      },
      "stats": {
        "type": "array",
        "items": {
          "type": "object",
          "required": [],
          "properties": {
            "bu": {
              "type": "string"
            },
            "countries": {
              "type": "array",
              "items": {
                "type": "object",
                "required": [],
                "properties": {
                  "country": {
                    "type": "string",
                    "nullable": true
                  },
                  "stores": {
                    "type": "array",
                    "items": {
                      "type": "object",
                      "required": [],
                      "properties": {
                        "uuid": {
                          "type": "string"
                        },
                        "businessUnit": {
                          "type": "string",
                          "nullable": true
                        },
                        "storeName": {
                          "type": "string"
                        },
                        "country": {
                          "type": "string",
                          "nullable": true
                        },
                        "noOfBookings": {
                          "type": "string"
                        },
                        "noOfPeopleBooked": {
                          "type": "string"
                        },
                        "noOfCheckins": {
                          "type": "string"
                        },
                        "noOfCheckouts": {
                          "type": "string"
                        },
                        "noOfPeopleWalkedIn": {
                          "type": "string"
                        },
                        "noOfPeopleWalkedOut": {
                          "type": "string"
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
module.exports = schema;
