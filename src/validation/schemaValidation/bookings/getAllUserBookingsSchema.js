const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to get all active bookings of a user"
schema.querystring = {
    type: "object",
    properties: {
      country: { type: "string", minLength: 1 }
    }
};
schema.params = {
  type: "object",
  required: ["id"],
  additionalProperties: false,
  properties: {
    id: {
      type: "string",
      description: "Here id passed should be UserId."
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    example: [
      {
          "2021-01-18": [
              {
                  "uuid": "c8cc46ea-a72e-4851-9209-2c34b2405fad",
                  "noOfPeople": 1,
                  "serviceType": [
                      "Visit mall"
                  ],
                  "status": "active",
                  "userId": "15604931k",
                  "checkoutTime": null,
                  "businessUnit": "Central mall",
                  "storeName": "Bangalore-Central",
                  "storeType": "mall",
                  "storeId": "4c99aaa7-67f4-4f84-9266-242628aa7310",
                  "startTime": "2021-01-18T16:00:00.000Z",
                  "endTime": "2021-01-18T17:00:00.000Z",
                  "slotId": "debb579c-8d66-4319-bd0c-0e3e63d2e640",
                  "timeZone": "America/Santiago"
              },
              {
                  "uuid": "1f58322b-22f3-4677-9411-ced23099f2c4",
                  "noOfPeople": 1,
                  "serviceType": [
                      "Visit store"
                  ],
                  "status": "active",
                  "userId": "15604931k",
                  "checkoutTime": null,
                  "businessUnit": "HomeImprovements",
                  "storeName": "Bangalore-East",
                  "storeType": "store",
                  "storeId": "b9b663c9-6d8e-499a-ac0e-526ad12b6e6b",
                  "startTime": "2021-01-18T16:30:00.000Z",
                  "endTime": "2021-01-18T17:00:00.000Z",
                  "slotId": "91dedf05-43d3-4bb2-a0ba-b1dac03a72c1",
                  "timeZone": "America/Santiago"
              }
          ]
      }
    ]
  }
}
module.exports = schema;
