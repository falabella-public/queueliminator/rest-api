const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to get bookings details based on booking uuid."
schema.params = {
    type: "object",
    required: ["id"],
    additionalProperties: false,
    properties: {
      id: {
        type: "string",
        description: "Here id passed should be bookingId."
      }
    }
  };
schema.response = {
  ...schema.response,
  "200": {
    "type": "object",
    "required": [],
    "properties": {
      "uuid": {
        "type": "string"
      },
      "userId": {
        "type": "string",
        "nullable": true
      },
      "slotId": {
        "type": "string",
        "nullable": true
      },
      "noOfPeople": {
        "type": "number"
      },
      "bookingTime": {
        "type": "string"
      },
      "checkinTime": {
        "type": "string",
        "nullable": true
      },
      "checkoutTime": {
        "type": "string",
        "nullable": true
      },
      "customerType": {
        "type": "string",
        "nullable": true
      },
      "serviceType": {
        "type": "array",
        "items": {
          "type": "string"
        },
        "nullable": true
      },
      "status": {
        "type": "string"
      },
      "emailId": {
        "type": "string",
        "nullable": true
      },
      "customerName": {
        "type": "string",
        "nullable": true
      },
      "customerPhoneNumber": {
        "type": "string",
        "nullable": true
      },
      "source": {
        "type": "string",
        "nullable": true
      },
      "acceptTermsConditions": {
        "type": "boolean",
        "nullable": true
      },
      "acceptUseOfMyInformation": {
        "type": "boolean",
        "nullable": true
      },
      "acceptReadTermsConditions": {
        "type": "boolean",
        "nullable": true
      },
      "userIdType": {
        "type": "string",
        "nullable": true
      },
      "startTime": {
        "type": "string"
      },
      "endTime": {
        "type": "string"
      },
      "businessUnit": {
        "type": "string",
        "nullable": true
      },
      "location": {
        "type": "string"
      },
      "storeName": {
        "type": "string"
      },
      "timeZone": {
        "type": "string",
        "nullable": true
      },
      "storeId": {
        "type": "string",
        "nullable": true
      }
    }
  }
}
module.exports = schema;
