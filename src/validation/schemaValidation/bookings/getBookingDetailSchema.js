const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to get bookings, walk-in, walkout and how many people did check-in and checkout detail."
schema.body = {
    "type": "object",
    "required": [],
    "properties": {
        "toDate": {
            "type": "string",
            "nullable": true
        },
        "fromDate": {
            "type": "string",
            "nullable": true
        },
        "startTime": {
            "type": "string"
        },
        "endTime": {
            "type": "string"
        },
        "storeid": {
            "type": "string"
        }
    }
};
schema.response = {
    ...schema.response,
    "200": {
        "type": "object",
        "required": [],
        "properties": {
          "peopleInsideStore": {
            "type": "object",
            "required": [],
            "properties": {
              "occupancyCount": {
                "type": "number"
              },
              "timeZone": {
                "type": "string",
                "nullable": true
              }
            }
          },
          "responseData": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [],
              "properties": {
                "totalBookings": {
                  "type": "string"
                },
                "totalwalkins": {
                  "type": "string"
                },
                "totalCheckedIn": {
                  "type": "string"
                },
                "date": {
                  "type": "string"
                }
              }
            }
          },
          "customerNames": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        }
      }
}
module.exports = schema;
