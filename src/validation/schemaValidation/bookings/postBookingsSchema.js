const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description =
  "This API provides the functionality to book a slot in the store/mall/office etc. Note:- slotId in the request payload should be picked from uuid returned from the /api/v1/slots?storeid= api response.";
schema.body = {
  type: "object",
  required: [
    "userId",
    "slotId",
    "noOfPeople",
    "serviceTypes",
    "customerType",
    "source"
  ],
  properties: {
    userId: {
      type: "string",
      example: "19512418-3"
    },
    slotId: {
      type: "string",
      example: "ed1e1feb-8586-4761-a82b-d0bb7827ac12"
    },
    noOfPeople: {
      type: "number",
      example: 1
    },
    serviceTypes: {
      type: "array",
      example: ["Visit store"],
      items: {
        type: "string"
      }
    },
    customerType: {
      type: "string",
      example: "regular"
    },
    source: {
      type: "string",
      example: "HomeImprovements_ios",
    }
  },
}

schema.response = {
  ...schema.response,
  200: {
    type: "array",
    description: "uuid in response represent the booking id.",
    items: {
      type: "object",
      required: [],
      properties: {
        uuid: {
          type: "string"
        },
        userId: {
          type: "string"
        },
        slotId: {
          type: "string"
        },
        noOfPeople: {
          type: "number"
        },
        serviceType: {
          type: "array",
          items: {
            type: "string"
          }
        },
        startTime: {
          type: "string"
        },
        endTime: {
          type: "string"
        },
        businessUnit: {
          type: "string"
        },
        location: {
          type: "string"
        },
        storeName: {
          type: "string"
        },
        storeId: {
          type: "string"
        },
        timeZone: {
          type: "string"
        },
        storeType: {
          type: "string"
        }
      }
    }
  }
};
module.exports = schema;
