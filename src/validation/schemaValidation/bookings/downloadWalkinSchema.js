const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to download the walk-in/walkout details for a specific date range and filters at country, business unit or store level."
schema.body = {
    "type": "object",
    "required": [],
    "properties": {
        "storeid": {
            "type": "string",
            "nullable": true
        },
        "selectedCountry": {
            "type": "string"
        },
        "selectedBu": {
            "type": "string"
        },
        "userId": {
            "type": "string",
            "nullable": true
        },
        "fromDate": {
            "type": "string",
            "nullable": true
        },
        "toDate": {
            "type": "string",
            "nullable": true
        },
        "startTime": {
            "type": "string"
        },
        "endTime": {
            "type": "string"
        }
    }
};
schema.response = {
    ...schema.response,
    "200": {
        "type": "object",
        "required": [],
        "properties": {
          "walkin": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [],
              "properties": {
                "country": {
                  "type": "string",
                  "nullable": true
                },
                "businessUnit": {
                  "type": "string",
                  "nullable": true
                },
                "location": {
                  "type": "string"
                },
                "storeName": {
                  "type": "string"
                },
                "walkinAt": {
                  "type": "string"
                },
                "noOfPeople": {
                  "type": "number"
                },
                "type": {
                  "type": "string",
                  "nullable": true
                }
              }
            }
          }
        }
      }
}
module.exports = schema;
