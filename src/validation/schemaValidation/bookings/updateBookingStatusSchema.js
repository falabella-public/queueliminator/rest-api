const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to update QR code status. On the QR code first scan it will change booking status to checked in and on second scan it will change to checked out."
schema.body = {
  type: "object",
  properties: {
    storeId: {
      type: "string"
    }
  }
};

schema.params = {
  type: "object",
  required: ["id"],
  additionalProperties: false,
  properties: {
    id: {
      type: "string",
      description: "Here id passed should be bookingId."
    }
  }
};

schema.response = {
  ...schema.response,
  "200": {
    "type": "object",
    "required": [],
    "properties": {
      "status": {
        "type": "string"
      },
      "message": {
        "type": "string"
      },
      "storeData": {
        "type": "object",
        "required": [],
        "properties": {
          "uuid": {
            "type": "string"
          },
          "country": {
            "type": "string",
            "nullable": true
          },
          "businessUnit": {
            "type": "string",
            "nullable": true
          },
          "location": {
            "type": "string"
          },
          "storeName": {
            "type": "string"
          },
          "bookingEnabled": {
            "type": "boolean"
          },
          "storeStatus": {
            "type": "string"
          },
          "storeArea": {
            "type": "number"
          },
          "recommendedLimit": {
            "type": "number"
          },
          "legalLimit": {
            "type": "number"
          },
          "staffCount": {
            "type": "number"
          },
          "occupancyCount": {
            "type": "number"
          },
          "updatedAt": {
            "type": "string",
            "nullable": true
          },
          "qrCodeValidityBuffer": {
            "type": "number"
          },
          "noOfPeopleAllowedDefault": {
            "type": "number",
            "nullable": true
          },
          "noOfPeopleAllowedMax": {
            "type": "number",
            "nullable": true
          },
          "advanceBookingLimitDays": {
            "type": "number",
            "nullable": true
          },
          "storeType": {
            "type": "string",
            "nullable": true
          },
          "customerType": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          },
          "automaticCenterOccupancyCount": {
            "type": "number"
          },
          "allowCurrentSlotBooking": {
            "type": "boolean",
            "nullable": true
          },
          "stdCode": {
            "type": "number",
            "nullable": true
          },
          "termsCondition": {
            "type": "string",
            "nullable": true
          },
          "termsConditionsAdditional": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "nullable": true
          },
          "timeZone": {
            "type": "string",
            "nullable": true
          },
          "openTime": {
            "type": "string"
          },
          "closeTime": {
            "type": "string"
          },
          "serviceTypes": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [],
              "properties": {
                "uuid": {
                  "type": "string"
                },
                "serviceType": {
                  "type": "string"
                }
              }
            },
            "nullable": true
          }
        }
      }
    }
  }
}

module.exports = schema;
