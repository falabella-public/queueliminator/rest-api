const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["bookings"];
schema.description = "This API provides the functionality to expire bookings which are in past and user did not utilised them."
module.exports = schema;
