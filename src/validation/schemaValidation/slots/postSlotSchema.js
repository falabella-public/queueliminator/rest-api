const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];

// schema.body = {
//     "type": "object",
//     "required": [],
//     "properties": {
//         "storeId": {
//             "type": "string",
//             "example": "bced9cca-387d-4d81-ba25-ac836a29a3e"
//         }
//     }
// };
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      message: {
        type: "string",
        example: "Slots created"
      }
    }
  }
};
module.exports = schema;
