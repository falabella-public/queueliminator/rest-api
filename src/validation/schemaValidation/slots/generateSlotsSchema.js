const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];
schema.description =
  "This API provides the functionality to generate new slots premises.";
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      message: {
        type: "string",
        example: "Slots created"
      }
    }
  }
};
module.exports = schema;
