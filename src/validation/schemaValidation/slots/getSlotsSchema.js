const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];
schema.description =
  "This API provides the functionality to get all the slots from today for a premise.";
schema.querystring = {
  type: "object",
  required: ["storeid"],
  properties: {
    storeid: {
      type: "string",
      minLength: 1,
      example: "4c99aaa7-67f4-4f84-9266-242628aa7310"
    },
    slotDate: {
      type: "string",
      minLength: 10,
      example: "2021-01-04"
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: ["serviceTypeId", "slots"],
      properties: {
        serviceTypeId: {
          type: "string"
        },
        slots: {
          type: "array",
          items: {
            type: "object",
            required: [],
            properties: {
              uuid: {
                type: "string"
              },
              startTime: {
                type: "string"
              },
              endTime: {
                type: "string"
              },
              serviceType: {
                type: "string"
              },
              capacity: {
                type: "number"
              },
              reservedCapacity: {
                type: "number"
              },
              noOfPeopleAllowedMax: {
                type: "number"
              },
              noOfPeopleAllowedDefault: {
                type: "number"
              },
              status: {
                type: "string"
              }
            }
          }
        }
      }
    }
  }
};
module.exports = schema;
