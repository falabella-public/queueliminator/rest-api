const generateSlotsSchema = require("./generateSlotsSchema");
const getSlotsSchema = require("./getSlotsSchema");
const getRunningSlotSchema = require("./getRunningSlotSchema");
const deleteSlotsSchema = require("./deleteSlotsSchema");
const postSlotSchema = require("./postSlotSchema");
const updateSlotsSchema = require("./updateSlotsSchema");
const getSpecificSlotSchema = require("./getSpecificSlotSchema");
const createSpecificSlotSchema = require("./createSpecificSlotSchema");
module.exports = {
  generateSlotsSchema,
  getSlotsSchema,
  getRunningSlotSchema,
  deleteSlotsSchema,
  postSlotSchema,
  updateSlotsSchema,
  getSpecificSlotSchema,
  createSpecificSlotSchema
};
