const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];
schema.description =
  "This API provides the functionality to get current running slot of a premise";
schema.querystring = {
  type: "object",
  required: ["storeid"],
  additionalProperties: false,
  properties: {
    storeid: { type: "string", minLength: 1 },
    slotDate: { type: "string", minLength: 1 }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    example: {
      uuid: "5064cafc-554f-441a-9860-6807a39ccce8",
      storeId: "7d68a965-a402-475f-a287-b46ead83c22c",
      reservedCapacity: 0,
      showUps: 0,
      startTime: "2021-01-12T17:00:00.000Z",
      endTime: "2021-01-12T18:00:00.000Z"
    },
    type: "object",
    required: [],
    properties: {
      uuid: {
        type: "string"
      },
      storeId: {
        type: "string"
      },
      reservedCapacity: {
        type: "number"
      },
      showUps: {
        type: "number"
      },
      startTime: {
        type: "string"
      },
      endTime: {
        type: "string"
      }
    }
  }
};
module.exports = schema;
