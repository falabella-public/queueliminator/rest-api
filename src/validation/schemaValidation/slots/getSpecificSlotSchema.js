const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];
schema.description =
  "This API provides the functionality to get a slot based on uuid of a premise";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
const responseBodySchema = {
  type: "object",
  required: [],
  example: {
    uuid: "5064cafc-554f-441a-9860-6807a39ccce8",
    storeId: "7d68a965-a402-475f-a287-b46ead83c22c",
    startTime: "2021-01-12T17:00:00.000Z",
    endTime: "2021-01-12T18:00:00.000Z",
    duration: 60,
    serviceType: "Visit store",
    capacity: 5,
    reservedCapacity: 0,
    customerType: "Standard-QR booking",
    showUps: 0,
    noOfPeopleAllowedDefault: 1,
    noOfPeopleAllowedMax: 1,
    qrCodeValidityBuffer: 5,
    serviceTypeId: "f068096c-8b83-452b-ac3a-c35fb73c3daa",
    status: "active"
  },
  properties: {
    uuid: {
      type: "string"
    },
    storeId: {
      type: "string"
    },
    startTime: {
      type: "string"
    },
    endTime: {
      type: "string"
    },
    duration: {
      type: "number"
    },
    serviceType: {
      type: "string"
    },
    capacity: {
      type: "number"
    },
    reservedCapacity: {
      type: "number"
    },
    customerType: {
      type: "string"
    },
    showUps: {
      type: "number"
    },
    noOfPeopleAllowedDefault: {
      type: "number"
    },
    noOfPeopleAllowedMax: {
      type: "number"
    },
    qrCodeValidityBuffer: {
      type: "number"
    },
    serviceTypeId: {
      type: "string"
    },
    status: {
      type: "string"
    }
  }
};
schema.response = {
  ...schema.response,
  "200": responseBodySchema
};

module.exports = schema;
