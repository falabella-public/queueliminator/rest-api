const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];
schema.description =
  "This API provides the functionality to update slot capacity of a premise";
schema.body = {
  type: "object",
  required: ["slotId", "capacity"],
  additionalProperties: false,
  properties: {
    slotId: { type: "string", minLength: 10 },
    capacity: { type: "number", minimum: 0 }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    example: [],
    type: "array",
    items: {
      type: "string"
    }
  }
};
module.exports = schema;
