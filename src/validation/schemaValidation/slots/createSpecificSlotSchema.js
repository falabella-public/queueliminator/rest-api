const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];

schema.body = {
    "type": "object",
    "required": ["storeId", "fromDate"],
    "properties": {
        "storeId": {
            "type": "string"
        },
        "fromDate": {
            "type": "string"
        }
    }
};
schema.response = {
    ...schema.response,
    "200": {
        type: "array",
        required: []
    }
};
module.exports = schema;
