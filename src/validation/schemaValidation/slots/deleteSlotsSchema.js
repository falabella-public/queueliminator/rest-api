const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slots"];
schema.description =
  "This API provides the functionality to delete slots of a premise from a given date.";
schema.body = {
  type: "object",
  required: ["storeId", "deletedBy", "fromDate", "toDate"],
  example: {
    fromDate: "2021-01-12",
    toDate: "2021-01-13",
    storeId: "7d68a965-a402-475f-a287-b46ead83c22c",
    serviceTypeId: "7d68a965-a402-475f-a287-b46ead83c22c",
    deletedBy: "joe@gmail.com",
  },
  properties: {
    storeId: { type: "string", minLength: 10 },
    deletedBy: { type: "string", minLength: 5 },
    fromDate: { type: "string", minLength: 5 },
    toDate: { type: "string", minLength: 5 },
    serviceTypeId: { type: "string", minLength: 5 }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    example: [],
    type: "array",
    items: {
      type: "string"
    }
  }
};
module.exports = schema;
