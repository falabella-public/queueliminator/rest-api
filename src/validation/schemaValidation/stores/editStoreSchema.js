const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to update premise configuration.";
module.exports = schema;
schema.body = {
  type: "object",
  required: [],
  properties: {
    location: {
      type: "string"
    },
    storeName: {
      type: "string"
    }
  }
};
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
module.exports = schema;
