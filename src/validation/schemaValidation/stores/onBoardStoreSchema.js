const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to onboard a new store/mall/any physical premises.";
schema.body = {
  type: "object",
  required: [],
  properties: {
    country: {
      type: "string"
    },
    businessUnit: {
      type: "string"
    },
    location: {
      type: "string"
    },
    storeName: {
      type: "string",
      example: "sample Store"
    },
    storeArea: {
      type: "number"
    },
    recommendedLimit: {
      type: "number",
      example: 10
    },
    legalLimit: {
      type: "number",
      example: 10
    },
    occupancyCount: {
      type: "number",
      example: 10
    },
    qrCodeValidityBuffer: {
      type: "number"
    },
    noOfPeopleAllowedDefault: {
      type: "number",
      example: 10
    },
    noOfPeopleAllowedMax: {
      type: "number",
      example: 10
    },
    advanceBookingLimitDays: {
      type: "number",
      example: 7
    },
    staffCount: {
      type: "number"
    },
    userEmail: {
      type: "string",
      example: ""
    },
    timeZone: {
      type: "string",
      example: "America/Santiago"
    },
    tandc: {
      type: "string"
    },
    std: {
      type: "number",
      example: 12
    },
    storeType: {
      type: "string"
    },
    customerType: {
      type: "array",
      items: {
        type: "string",
        example: "Standard"
      }
    },
    serviceTypes: {
      type: "array",
      items: {
        type: "object",
        required: [],
        properties: {
          serviceType: {
            type: "string",
            example: "Customer care"
          },
          qrBookingCapacity: {
            type: "number"
          },
          duration: {
            type: "number",
            example: 60
          },
          walkinCapacity: {
            type: "number"
          }
        }
      }
    },
    configs: {
      type: "array",
      example: [
        {
          "customerType": "Standard",
          "startTime": "2020-12-19 18:30:00",
          "endTime": "2020-12-20 04:30:00",
          "storeStartTime": "2020-12-19 18:30:00",
          "storeEndTime": "2020-12-20 04:30:00",
          "isOpen": true,
          "dayOfWeek": 0
      },
      {
          "customerType": "Standard",
         "startTime": "2020-12-19 18:30:00",
          "endTime": "2020-12-20 04:30:00",
          "storeStartTime": "2020-12-19 18:30:00",
          "storeEndTime": "2020-12-20 04:30:00",
          "isOpen": true,
          "dayOfWeek": 1
      },
      {
          "customerType": "Standard",
         "startTime": "2020-12-19 18:30:00",
          "endTime": "2020-12-20 04:30:00",
          "storeStartTime": "2020-12-19 18:30:00",
          "storeEndTime": "2020-12-20 04:30:00",
          "isOpen": true,
          "dayOfWeek": 2
      },
      {
          "customerType": "Standard",
          "startTime": "2020-12-19 18:30:00",
          "endTime": "2020-12-20 04:30:00",
          "storeStartTime": "2020-12-19 18:30:00",
          "storeEndTime": "2020-12-20 04:30:00",
          "isOpen": true,
          "dayOfWeek": 3
      },
      {
          "customerType": "Standard",
          "startTime": "2020-12-19 18:30:00",
          "endTime": "2020-12-20 04:30:00",
          "storeStartTime": "2020-12-19 18:30:00",
          "storeEndTime": "2020-12-20 04:30:00",
          "isOpen": true,
          "dayOfWeek": 4
      },
      {
          "customerType": "Standard",
         "startTime": "2020-12-19 18:30:00",
          "endTime": "2020-12-20 04:30:00",
          "storeStartTime": "2020-12-19 18:30:00",
          "storeEndTime": "2020-12-20 04:30:00",
          "isOpen": true,
          "dayOfWeek": 5
      },
      {
          "customerType": "Standard",
          "startTime": "2020-12-19 18:30:00",
          "endTime": "2020-12-20 04:30:00",
          "storeStartTime": "2020-12-19 18:30:00",
          "storeEndTime": "2020-12-20 04:30:00",
          "isOpen": true,
          "dayOfWeek": 6
      }
      ],
      items: {
        type: "object",
        required: [],
        properties: {
          customerType: {
            type: "string"
          },
          startTime: {
            type: "string"
          },
          endTime: {
            type: "string"
          },
          storeStartTime: {
            type: "string"
          },
          storeEndTime: {
            type: "string"
          },
          isOpen: {
            type: "boolean"
          },
          dayOfWeek: {
            type: "number"
          }
        }
      }
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      message: {
        type: "string",
        example: "Onboarded"
      }
    }
  }
};
module.exports = schema;
