const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get premise information based on web booking requirement.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      storeName: {
        type: "string"
      },
      uuid: {
        type: "string"
      },
      noOfStoresAllowedMax: {
        type: "number"
      },
      stdCode: {
        type: "number"
      },
      termsCondition: {
        type: "string"
      },
      termsConditionsAdditional: {
        type: "array",
        items: {
          type: "string"
        },
        nullable: true
      },
      timeZone: {
        type: "string"
      },
      allowCurrentSlotBooking: {
        type: "boolean"
      },
      serviceTypes: {
        type: "array",
        items: {
          type: "object",
          required: [],
          properties: {
            uuid: {
              type: "string"
            },
            serviceType: {
              type: "string"
            }
          }
        }
      }
    }
  }
};
module.exports = schema;
