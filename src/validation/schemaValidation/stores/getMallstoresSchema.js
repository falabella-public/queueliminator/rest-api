const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get all the stores within the mall so user can book stores within mall using mall website/app.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: ["stores"],
    properties: {
      stores: {
        type: "array",
        items: {
          type: "object",
          required: ["uuid", "businessUnit"],
          properties: {
            businessUnit: {
              type: "string",
              example: "Retail Clothing"
            },
            uuid: {
              type: "string",
              example: "2c32d9bc-e415-43d1-a05c-2c20d46618d6"
            }
          }
        }
      }
    }
  }
};
module.exports = schema;
