const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get all the service types supported by a business unit.";
schema.querystring = {
  type: "object",
  required: ["businessunit"],
  additionalProperties: false,
  properties: {
    businessunit: { type: "string", minLength: 2 }
  }
};

schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      serviceTypes: {
        type: "array",
        items: {
          type: "string"
        }
      }
    }
  }
};
module.exports = schema;
