const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get all the stores on which user has access.";
schema.body = {
  type: "object",
  example: {
    country: "CL",
    businessunit: "HomeImprovements",
    emailId: "joe@gmail.com",
    userRole: "store"
  },
  properties: {
    country: {
      type: "string"
    },
    businessunit: {
      type: "string"
    },
    emailId: {
      type: "string"
    },
    userRole: {
      type: "string"
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    example: [
      {
        location: "Bangalore",
        stores: [
          {
            uuid: "93d81b53-b1ae-4381-b47e-185b9fe904fe",
            storeName: "Bangalore-east"
          },
          {
            uuid: "f4374f1f-38d2-42e0-9b89-a067385e9b98",
            storeName: "Bangalore-west"
          },
          {
            uuid: "685635c0-2db5-457e-8947-7c8390b3860a",
            storeName: "Bangalore-north"
          }
        ]
      }
    ],
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        location: {
          type: "string"
        },
        stores: {
          type: "array",
          items: {
            type: "object",
            required: [],
            properties: {
              uuid: {
                type: "string"
              },
              storeName: {
                type: "string"
              }
            }
          }
        }
      }
    }
  }
};

module.exports = schema;
