const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get all business units within a country.";
schema.querystring = {
  type: "object",
  properties: {
    country: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "string",
      example: "HomeImproments"
    }
  }
};

module.exports = schema;
