const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get premise information.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    example: {
      uuid: "c7707cdc-1482-4c06-988c-512c59745c6a",
      storeName: "Bangalore-east",
      recommendedLimit: 90,
      legalLimit: 100,
      occupancyCount: 0
    },
    type: "object",
    required: [],
    properties: {
      uuid: {
        type: "string"
      },
      storeName: {
        type: "string"
      },
      recommendedLimit: {
        type: "number"
      },
      legalLimit: {
        type: "number"
      },
      occupancyCount: {
        type: "number"
      }
    }
  }
};

module.exports = schema;
