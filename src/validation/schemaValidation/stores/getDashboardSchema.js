const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get premise configuration.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      uuid: {
        type: "string"
      },
      country: {
        type: "string"
      },
      businessUnit: {
        type: "string"
      },
      location: {
        type: "string"
      },
      storeName: {
        type: "string"
      },
      bookingEnabled: {
        type: "boolean"
      },
      storeStatus: {
        type: "string"
      },
      storeArea: {
        type: "number"
      },
      recommendedLimit: {
        type: "number"
      },
      legalLimit: {
        type: "number"
      },
      staffCount: {
        type: "number"
      },
      occupancyCount: {
        type: "number"
      },
      updatedAt: {
        type: "string"
      },
      qrCodeValidityBuffer: {
        type: "number"
      },
      noOfPeopleAllowedDefault: {
        type: "number"
      },
      noOfPeopleAllowedMax: {
        type: "number"
      },
      advanceBookingLimitDays: {
        type: "number"
      },
      storeType: {
        type: "string"
      },
      customerType: {
        type: "array",
        items: {
          type: "string"
        }
      },
      automaticCenterOccupancyCount: {
        type: "number"
      },
      allowCurrentSlotBooking: {
        type: "boolean"
      },
      stdCode: {
        type: "number"
      },
      termsCondition: {
        type: "string"
      },
      termsConditionsAdditional: {
        type: "array",
        items: {
          type: "string"
        },
        nullable: true
      },
      timeZone: {
        type: "string"
      },
      openTime: {
        type: "string"
      },
      closeTime: {
        type: "string"
      },
      serviceTypes: {
        type: "array",
        items: {
          type: "object",
          required: [],
          properties: {
            uuid: {
              type: "string"
            },
            serviceType: {
              type: "string"
            }
          }
        }
      }
    }
  }
};
module.exports = schema;
