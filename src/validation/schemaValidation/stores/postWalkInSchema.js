const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to update people inside store/mall/any physical premises whenever any customer arrives without booking. The walk-in customer will be allowed only if there is occupancy left after bookings.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      format: "uuid",
      description: "store/mall/any physical uuid from stores table"
    }
  }
};

schema.body = {
  type: "object",
  required: ["noOfPeople"],
  properties: {
    noOfPeople: {
      type: "number",
      example: "Number of people walking in i.e 1,2,3 etc."
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    properties: {
      uuid: {
        type: "string",
        example: "store id"
      },
      occupancyCount: {
        type: "number",
        example: "Current occupancy"
      },
      recommendedLimit: {
        type: "number",
        example: "Occupancy limit with buffer"
      },
      legalLimit: {
        type: "number",
        example: "Occupancy limit without buffer"
      }
    }
  }
};

module.exports = schema;
