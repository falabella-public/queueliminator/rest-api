const getStoresSchema = require("./getStoresSchema");
const getBusinessUnitsSchema = require("./getBusinessUnitsSchema");
const getBusinessUnitSchema = require("./getBusinessUnitSchema");
const getStoreCurrentRunningSlotsSchema = require("./getStoreCurrentRunningSlotsSchema");
const postWalkInSchema = require("./postWalkInSchema");
const postWalkOutSchema = require("./postWalkOutSchema");
const onBoardStoreSchema = require("./onBoardStoreSchema");
const getMallstoresSchema = require("./getMallstoresSchema");
const getSlotSchema = require("./getSlotSchema");
const getCurrentStatusSchema = require("./getCurrentStatusSchema");
const getDashboardSchema = require("./getDashboardSchema");
const getMallsSchema = require("./getMallsSchema");
const getServiceTypesSchema = require("./getServiceTypesSchema");
const getSpecificStoreSchema = require("./getSpecificStoreSchema");
const getUserStoresSchema = require("./getUserStoresSchema");
const resetoccupancySchema = require("./resetoccupancySchema");
const updatecountSchema = require("./updatecountSchema");
const editStoreSchema = require("./editStoreSchema");
const updateMallSchema = require("./updateMallSchema");
const getStoreForWebSchema = require("./getStoreForWebSchema");
const updateStoreSchema = require("./updateStoreSchema");
module.exports = {
  getStoresSchema,
  getBusinessUnitsSchema,
  getStoreCurrentRunningSlotsSchema,
  postWalkInSchema,
  postWalkOutSchema,
  onBoardStoreSchema,
  getMallstoresSchema,
  getSlotSchema,
  getBusinessUnitSchema,
  getUserStoresSchema,
  getCurrentStatusSchema,
  getDashboardSchema,
  getMallsSchema,
  getServiceTypesSchema,
  getSpecificStoreSchema,
  resetoccupancySchema,
  editStoreSchema,
  updatecountSchema,
  updateMallSchema,
  getStoreForWebSchema,
  updateStoreSchema
};
