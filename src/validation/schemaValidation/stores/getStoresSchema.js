const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get all the stores within a country and business unit.";
schema.querystring = {
  type: "object",
  required: ["businessunit"],
  properties: {
    country: {
      type: "string",
      minLength: 1
    },
    businessunit: {
      type: "string",
      minLength: 1
    },
    source: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        location: {
          type: "string"
        },
        stores: {
          type: "array",
          items: {
            type: "object",
            required: [],
            properties: {
              uuid: {
                type: "string"
              },
              storeName: {
                type: "string"
              },
              serviceTypes: {
                type: "array",
                items: {
                  type: "object",
                  required: [],
                  properties: {
                    uuid: {
                      type: "string"
                    },
                    serviceType: {
                      type: "string"
                    }
                  }
                }
              },
              timeZone: {
                type: "string"
              },
              mallId: {
                type: "string"
              },
              allowCurrentSlotBooking: {
                type: "boolean"
              }
            }
          }
        }
      }
    }
  }
};

module.exports = schema;
