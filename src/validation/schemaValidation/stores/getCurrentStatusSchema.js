const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get current status of a premise like how many people are in premise, how many people are expected and how many people walked in.";
schema.body = {
  type: "object",
  required: ["location", "storeId", "country", "businessUnit", "emailId"],
  properties: {
    location: {
      type: "string"
    },
    storeId: {
      type: "string"
    },
    country: {
      type: "string"
    },
    businessUnit: {
      type: "string"
    },
    emailId: {
      type: "string"
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        uuid: {
          type: "string"
        },
        country: {
          type: "string"
        },
        businessUnit: {
          type: "string"
        },
        location: {
          type: "string"
        },
        storeName: {
          type: "string"
        },
        legalLimit: {
          type: "number"
        },
        recommendedLimit: {
          type: "number"
        },
        occupancyCount: {
          type: "number"
        },
        isOpen: {
          type: "boolean"
        },
        timeZone: {
          type: "string"
        },
        startTime: {
          type: "string"
        },
        endTime: {
          type: "string"
        },
        walkin: {
          type: "string"
        },
        walkout: {
          type: "string"
        }
      }
    }
  }
};
module.exports = schema;
