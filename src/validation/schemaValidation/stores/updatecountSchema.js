const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to update occupancy count from automatic center.";
schema.body = {
  type: "object",
  required: [],
  properties: {
    storeId: {
      type: "string"
    },
    count: {
      type: "number"
    }
  }
};
module.exports = schema;
