const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to update mall configuration.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
schema.body = {
  type: "object",
  required: [],
  properties: {
    mallId: {
      type: "string",
      nullable: true
    },
    noOfStoresAllowedMax: {
      type: "number"
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    example: [],
    items: {
      type: "string"
    }
  }
};
module.exports = schema;
