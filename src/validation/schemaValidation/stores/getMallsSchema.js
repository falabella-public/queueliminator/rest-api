const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get mall in which store exist.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
schema.querystring = {
  type: "object",
  required: ["country"],
  properties: {
    country: {
      type: "string",
      minLength: 1
    }
  }
};

schema.response = {
  ...schema.response,
  "200": {
    "example": {
      "malls": [
          {
              "businessUnit": "Central Mall",
              "mallId": "c305dbfc-44c5-40f0-a39e-756fa6ed4e80",
              "location": "Bangalore",
              "country": "IN",
              "storeName": "Bangalore-East"
          }
          
      ],
      "storeMall": [
          {
              "storeId": "4c99aaa7-67f4-4f84-9266-242628aa7310",
              "mallId": null,
              "noOfStoresAllowedMax": 3
          }
      ]
    },
    "type": "object",
    "required": [],
    "properties": {
      "malls": {
        "type": "array",
        "items": {
          "type": "object",
          "required": [],
          "properties": {
            "businessUnit": {
              "type": "string"
            },
            "mallId": {
              "type": "string"
            },
            "location": {
              "type": "string"
            },
            "country": {
              "type": "string"
            },
            "storeName": {
              "type": "string"
            }
          }
        }
      },
      "storeMall": {
        "type": "array",
        "items": {
          "type": "object",
          "required": [],
          "properties": {
            "storeId": {
              "type": "string"
            },
            "mallId": {
              "type": "string",
              "nullable": true
            },
            "noOfStoresAllowedMax": {
              "type": "number"
            }
          }
        }
      }
    }
  }
};
module.exports = schema;
