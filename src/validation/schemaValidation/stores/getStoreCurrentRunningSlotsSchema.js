const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to get current running slot of the store/mall/any physical premises. In absence of current running slot, it sends open/close time if premises currently opened.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        serviceTypeId: {
          type: "string"
        },
        slots: {
          type: "array",
          items: {
            type: "object",
            required: [],
            properties: {
              uuid: {
                type: "string"
              },
              startTime: {
                type: "string"
              },
              endTime: {
                type: "string"
              },
              serviceType: {
                type: "string"
              },
              capacity: {
                type: "number"
              },
              reservedCapacity: {
                type: "string"
              },
              noOfPeopleAllowedMax: {
                type: "number"
              },
              noOfPeopleAllowedDefault: {
                type: "number"
              },
              status: {
                type: "string"
              }
            }
          }
        }
      }
    }
  }
};

module.exports = schema;
