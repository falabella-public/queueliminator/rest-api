const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["stores"];
schema.description =
  "This API provides the functionality to update people inside store/mall/any physical premises whenever any customer arrives without booking. The walk-in customer count will be decrease on walk out.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      format: "uuid",
      description: "store/mall/any physical uuid from stores table"
    }
  }
};
schema.body = {
  type: "object",
  required: ["noOfPeople"],
  properties: {
    noOfPeople: {
      type: "number",
      example: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    properties: {
      uuid: {
        type: "string"
      },
      occupancyCount: {
        type: "number"
      },
      recommendedLimit: {
        type: "number"
      },
      legalLimit: {
        type: "number"
      }
    }
  }
};

module.exports = schema;
