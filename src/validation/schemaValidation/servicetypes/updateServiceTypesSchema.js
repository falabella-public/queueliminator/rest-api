const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["servicetypes"];
schema.body = {
        "type": "object",
        "required": [],
        "properties": {
          "serviceTypes": {
            "type": "array",
            "items": {
              "type": "object",
              "required": [],
              "properties": {
                "uuid": {
                  "type": "string"
                },
                "serviceType": {
                  "type": "string"
                },
                "qrBookingCapacity": {
                  "type": "number"
                },
                "walkinCapacity": {
                  "type": "number"
                },
                "duration": {
                  "type": "number"
                },
                "defaultMallServiceType": {
                  "type": "boolean",
                  "nullable": true
                },
                "capacity": {
                  "type": "number"
                },
                "allocation": {
                  "type": "string"
                },
                "qrBookingPercentage": {
                  "type": "number"
                },
                "walkinPercentage": {
                  "type": "number"
                }
              }
            }
          },
          "storeId": {
            "type": "string",
            "nullable": true
          }
        }
}
schema.response = {
    ...schema.response,
    "200": {
        "type": "object",
        "required": [],
        "properties": {
          "message": {
            "type": "string"
          }
        }
    }
}
schema.description =
  "This API provides the functionality to update service types to a premise.";
module.exports = schema;
