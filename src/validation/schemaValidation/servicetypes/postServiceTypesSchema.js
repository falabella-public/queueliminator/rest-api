const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["servicetypes"];
schema.body = {
    "type": "object",
    "required": [],
    "properties": {
        "storeId": {
            "type": "string"
        },
        "serviceType": {
            "type": "string"
        },
        "duration": {
            "type": "number"
        },
        "qrBookingCapacity": {
            "type": "number"
        },
        "walkinCapacity": {
            "type": "number"
        },
        "defaultMallServiceType": {
            "type": "boolean",
            "nullable": true
        }
    }
};
schema.response = {
    ...schema.response,
    "200": {
        "type": "array",
        "example": [],
        "items": {
          "type": "string"
        }
    }
}
module.exports = schema;
schema.description =
  "This API provides the functionality to add new service type to a premise.";
module.exports = schema;
