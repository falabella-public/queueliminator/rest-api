const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["servicetypes"];
schema.body = {
    "type": "object",
    "required": [],
    "properties": {
        "uuid": {
            "type": "string"
        },
        "deletedBy": {
            "type": "string"
        }
    }
};
schema.response = {
    ...schema.response,
    "200": {
        "type": "array",
        "example": [],
        "items": {
          "type": "string"
        }
    }
}
schema.description =
  "This API provides the functionality to delete a service type from premise.";
module.exports = schema;
