const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["servicetypes"];
schema.params = {
    type: "object",
    required: ["id"],
    additionalProperties: false,
    properties: {
      id: {
        type: "string",
        description: "store/mall/any physical uuid from stores table"
      }
    }
};
schema.response = {
    ...schema.response,
    "200": {
        "type": "array",
        "items": {
          "type": "object",
          "required": [],
          "properties": {
            "uuid": {
              "type": "string"
            },
            "serviceType": {
              "type": "string"
            },
            "qrBookingCapacity": {
              "type": "number"
            },
            "walkinCapacity": {
              "type": "number"
            },
            "duration": {
              "type": "number"
            },
            "defaultMallServiceType": {
              "type": "boolean",
              "nullable": true
            }
          }
        }
    }
}
schema.description =
  "This API provides the functionality to get service types of a premise.";
module.exports = schema;
