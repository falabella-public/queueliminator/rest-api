const getServiceTypesSchema = require("./getServiceTypesSchema");
const deleteServiceTypesSchema = require("./deleteServiceTypesSchema");
const postServiceTypeSchema = require("./postServiceTypesSchema");
const updateServiceTypesSchema = require("./updateServiceTypesSchema");
module.exports = {
  getServiceTypesSchema,
  deleteServiceTypesSchema,
  updateServiceTypesSchema,
  postServiceTypeSchema
};
