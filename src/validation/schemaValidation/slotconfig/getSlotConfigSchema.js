const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slotconfig"];
schema.description =
  "This API provides the functionality to get slot configurtion like premise/slot open close time, is premise open based on a day of premise.";
schema.querystring = {
  type: "object",
  required: ["storeid"],
  additionalProperties: false,
  properties: {
    storeid: { type: "string", minLength: 1 }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        uuid: {
          type: "string"
        },
        storeId: {
          type: "string"
        },
        customerType: {
          type: "string"
        },
        startTime: {
          type: "string"
        },
        endTime: {
          type: "string"
        },
        isOpen: {
          type: "boolean"
        },
        staffCount: {
          type: "number"
        },
        serviceTypeAvailable: {
          type: "boolean"
        },
        dayOfWeek: {
          type: "number"
        },
        storeStartTime: {
          type: "string"
        },
        storeEndTime: {
          type: "string"
        },
        noOfPeopleAllowedDefault: {
          type: "number"
        },
        noOfPeopleAllowedMax: {
          type: "number"
        },
        qrCodeValidityBuffer: {
          type: "number"
        }
      }
    }
  }
};
module.exports = schema;
