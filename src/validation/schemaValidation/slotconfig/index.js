const editSlotConfigSchema = require("./editSlotConfigSchema");
const getSlotConfigSchema = require("./getSlotConfigSchema");
module.exports = {
  editSlotConfigSchema,
  getSlotConfigSchema
};
