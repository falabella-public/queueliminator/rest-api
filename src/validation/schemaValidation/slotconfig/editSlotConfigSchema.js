const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["slotconfig"];
schema.description =
  "This API provides the functionality to update slot configurtion like premise/slot open close time, is premise open based on a day of premise.";
schema.body = {
  type: "object",
  required: [],
  properties: {
    storeId: {
      type: "string"
    },
    configs: {
      type: "array",
      items: {
        type: "object",
        required: [],
        properties: {
          uuid: {
            type: "string"
          },
          storeId: {
            type: "string"
          },
          customerType: {
            type: "string"
          },
          startTime: {
            type: "string"
          },
          endTime: {
            type: "string"
          },
          isOpen: {
            type: "boolean"
          },
          staffCount: {
            type: "number"
          },
          serviceTypeAvailable: {
            type: "boolean"
          },
          dayOfWeek: {
            type: "number"
          },
          storeStartTime: {
            type: "string"
          },
          storeEndTime: {
            type: "string"
          },
          noOfPeopleAllowedDefault: {
            type: "number"
          },
          noOfPeopleAllowedMax: {
            type: "number"
          },
          qrCodeValidityBuffer: {
            type: "number"
          },
          origStartTime: {
            type: "string"
          },
          origEndTime: {
            type: "string"
          },
          diff: {
            type: "string"
          }
        }
      }
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      message: {
        type: "string"
      }
    }
  }
};
module.exports = schema;
