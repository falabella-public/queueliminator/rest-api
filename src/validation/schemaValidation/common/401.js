module.exports = {
  type: "object",
  required: ["errors"],
  properties: {
    errors: {
      type: "array",
      items: {
        type: "object",
        required: ["code", "status"],
        properties: {
          code: {
            type: "string",
            example: "Unauthorized"
          },
          status: {
            type: "number",
            example: 401
          }
        }
      }
    }
  }
};
