module.exports = {
  type: "object",
  required: ["errors"],
  properties: {
    errors: {
      type: "array",
      items: {
        type: "object",
        required: ["code", "status"],
        properties: {
          code: {
            type: "string",
            example: "Internal Server Error"
          },
          status: {
            type: "number",
            example: 500
          },
          detail: {
            type: "string",
            example: "Syntax error at..."
          }
        }
      }
    }
  }
};
