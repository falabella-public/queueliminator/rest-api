module.exports = {
  type: "object",
  required: ["errors"],
  properties: {
    errors: {
      type: "array",
      items: {
        type: "object",
        required: ["code", "status", "detail"],
        properties: {
          code: {
            type: "string",
            example: "Bad request"
          },
          status: {
            type: "number",
            example: 400
          },
          title: {
            type: "string",
            example: "Validation error"
          },
          detail: {
            type: "string",
            example: "should NOT be shorter than 1 characters"
          }
        }
      }
    }
  }
};
