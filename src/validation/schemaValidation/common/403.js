module.exports = {
  type: "object",
  required: ["errors"],
  properties: {
    errors: {
      type: "array",
      items: {
        type: "object",
        required: ["code", "status"],
        properties: {
          code: {
            type: "string",
            example: "Forbidden"
          },
          status: {
            type: "number",
            example: 403
          }
        }
      }
    }
  }
};
