module.exports = {
  headers: {
    type: "object",
    properties: {
      Authorization: {
        type: "string",
        minLength: 8,
        description: "Google/azure active directory bearer token"
      },
      userrole: {
        type: "string",
        minLength: 1,
        description: "User should have guard or store level role for this"
      }
    }
  },
  response: {
    500: require("./500"),
    400: require("./400"),
    401: require("./401"),
    403: require("./403")
  }
};
