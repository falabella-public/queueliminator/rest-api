const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["downloadhistory"];
schema.description =
  "This API provides the functionality to get the details of all the users which are downloading report on queuEliminator.";
schema.body = {
  type: "object",
  example: {
    storeid: "",
    selectedCountry: "AR",
    selectedBu: "HomeImprovements",
    userId: "joe@gmail.com",
    fromDate: "2020-01-13",
    toDate: "2021-01-13",
    startTime: "2021-01-12T23:30:00.000Z",
    endTime: "2021-01-13T18:00:00.000Z"
  },
  required: [],
  properties: {
    storeid: {
      type: "string"
    },
    selectedCountry: {
      type: "string"
    },
    selectedBu: {
      type: "string"
    },
    userId: {
      type: "string"
    },
    fromDate: {
      type: "string"
    },
    toDate: {
      type: "string"
    },
    startTime: {
      type: "string"
    },
    endTime: {
      type: "string"
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        uuid: {
          type: "string"
        },
        downloadType: {
          type: "string"
        },
        userId: {
          type: "string"
        },
        country: {
          type: "string",
          nullable: true
        },
        businessUnit: {
          type: "string"
        },
        storeId: {
          type: "string"
        },
        fromDate: {
          type: "string"
        },
        toDate: {
          type: "string"
        },
        downloadedAt: {
          type: "string",
          nullable: true
        }
      }
    }
  }
};
module.exports = schema;
