const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.description =
  "This API provides the functionality to add a new business unit to store user to get the permission on it";
schema.body = {
  type: "object",
  required: ["userId", "countryBUs"],
  properties: {
    userId: {
      type: "string",
      example: "e53a608a-e7b3-4fa3-8fbe-d7a43c406b54"
    },
    countryBUs: {
      type: "array",
      items: {
        type: "object",
        required: [],
        properties: {
          country: {
            type: "string",
            example: "CL"
          },
          businessUnit: {
            type: "string",
            example: "HomeImprovements"
          }
        }
      }
    }
  }
};

schema.response = {
  ...schema.response,
  "200": {
    example: [],
    type: "array",
    items: {
      type: "string"
    }
  }
};

module.exports = schema;
