[{ uuid: "2c2a5891-8ab0-4154-9630-c5ab8c8c6118" }];

const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.description =
  "This API provides the functionality to delete a user from all roles.";
const requestBodySchema = {
  type: "object",
  required: [],
  properties: {
    userEmail: {
      type: "string"
    },
    deletedByEmail: {
      type: "string"
    },
    userRole: {
      type: "string"
    }
  }
};
schema.body = requestBodySchema;
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        uuid: {
          type: "string"
        }
      }
    }
  }
};
module.exports = schema;
