const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.description =
  "This API provides the functionality to get all users who have access to a premise.";
schema.querystring = {
  type: "object",
  required: ["emailid"],
  properties: {
    emailid: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      required: [],
      properties: {
        userId: {
          type: "string"
        },
        userName: {
          type: "string"
        },
        userEmail: {
          type: "string"
        },
        userRole: {
          type: "string"
        },
        isSuperUser: {
          type: "boolean"
        },
        storeId: {
          type: "string",
          "nullable": true
        },
        country: {
          type: "string",
          "nullable": true
        },
        businessUnit: {
          type: "string",
          "nullable": true
        }
      }
    },
    example: [
      {
        userId: "Joe",
        userName: "Joe",
        userEmail: "joe@gmail.com",
        userRole: "store",
        isSuperUser: true,
        storeId: null,
        country: "CL",
        businessUnit: "HomeImprovements"
      },
      {
        userId: "Mathew",
        userName: "Mathew",
        userEmail: "mathew@gmail.com",
        userRole: "store",
        isSuperUser: true,
        storeId: null,
        country: null,
        businessUnit: null
      }
    ]
  }
};

module.exports = schema;
