const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.description =
  "This API provides the functionality to delete business unit access from a user.";
schema.body = {
  type: "object",
  required: ["userId", "countryBUs"],
  properties: {
    userId: {
      type: "string",
      example: "e53a608a-e7b3-4fa3-8fbe-d7a43c406b54"
    },
    countryBUs: {
      type: "array",
      items: {
        type: "object",
        required: [],
        properties: {
          country: {
            type: "string",
            example: "CL"
          },
          businessUnit: {
            type: "string",
            example: "HomeImprovements"
          }
        }
      }
    },
    deletedBy: {
      type: "string",
      example: "joe@gmail.com"
    }
  }
};

schema.response = {
  ...schema.response,
  "200": {
    example: [],
    type: "array",
    items: {
      type: "string"
    }
  }
};

module.exports = schema;
