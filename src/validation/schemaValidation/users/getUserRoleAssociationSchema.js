const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.description =
  "This API provides the functionality to get a user roles in country, business unit, store etc.";
schema.params = {
  type: "object",
  required: ["id"],
  properties: {
    id: {
      type: "string",
      minLength: 1,
      example: "joe@gmail.com"
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    example: [
      {
        userId: "Joe",
        uuid: "9d75f552-4f8d-4f2c-94f0-296e45d0933e",
        createdAt: "2020-12-22T12:08:57.169Z",
        updatedAt: "2020-12-22T12:08:57.169Z",
        isSuperUser: true,
        storeId: null,
        country: "CL",
        businessUnit: "HomeImprovements",
        userEmail: "joe@gmail.com",
        userName: "Joe",
        userRole: "store",
        onlyGuardUserCreationAccess: false,
        isDashboardDownloadAccess: true
      }
    ],
    items: {
      type: "object",
      required: [],
      properties: {
        userId: {
          type: "string"
        },
        uuid: {
          type: "string"
        },
        createdAt: {
          type: "string"
        },
        updatedAt: {
          type: "string"
        },
        isSuperUser: {
          type: "boolean"
        },
        storeId: {
          type: "string",
          nullable: true
        },
        country: {
          type: "string"
        },
        businessUnit: {
          type: "string"
        },
        userEmail: {
          type: "string"
        },
        userName: {
          type: "string"
        },
        userRole: {
          type: "string"
        },
        onlyGuardUserCreationAccess: {
          type: "boolean"
        },
        isDashboardDownloadAccess: {
          type: "boolean"
        }
      }
    }
  }
};
schema.body = {
  type: "object",
  required: ["requestedBy"],
  additionalProperties: false,
  properties: {
    requestedBy: {
      type: "string",
      minLength: 5
    }
  }
};

module.exports = schema;
