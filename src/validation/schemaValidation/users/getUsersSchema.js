const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.headers = {
  ...schema.headers
}
schema.headers.properties = {
  ...schema.headers.properties,
  customToken: {
    type: "boolean",
    example: true
  },
}
schema.querystring = {
  type: "object",
  required: ["emailid"],
  properties: {
    emailid: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "object",
    required: [],
    properties: {
      isSuperUser: {
        type: "boolean"
      },
      userEmail: {
        type: "string"
      },
      userName: {
        type: "string"
      },
      userRole: {
        type: "string"
      },
      storeId: {
        type: "string",
        nullable: true
      },
      onlyGuardAccess: {
        type: "boolean"
      },
      downloadAccess: {
        type: "boolean"
      },
      countryBUs: {
        type: "array",
        items: {
          type: "object",
          properties: {
            country: {
              type: "string"
            },
            businessUnit: {
              type: "string"
            }
          }
        }
      },
      uuid: {
        type: "string"
      },
      createdAt: {
        type: "string",
        nullable: true
      },
      updatedAt: {
        type: "string",
        nullable: true
      },
      userId: {
        type: "string",
        nullable: true
      },
      storeName: {
        type: "string",
        nullable: true
      },
      token: {
        type: "string",
        nullable: true
      }
    }
  }
};
schema.description =
  "This API provides the functionality to get a user detail.";
module.exports = schema;
