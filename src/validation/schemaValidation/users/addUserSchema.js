const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.description =
  "This API provides the functionality to add a new user for premise access at store, guard , country or business unit level.";
const requestBodySchema = {
  type: "object",
  required: [],
  properties: {
    countryBU: {
      type: "array",
      items: {
        type: "object",
        required: [],
        properties: {
          country: {
            type: "string",
            example: "CL"
          },
          businessUnit: {
            type: "string",
            example: "HomeImprovements"
          }
        }
      }
    },
    storeId: {
      type: "string"
    },
    userName: {
      type: "string"
    },
    userEmail: {
      type: "string"
    },
    userRole: {
      type: "string"
    },
    downloadAccess: {
      type: "boolean"
    },
    onlyGuardAccess: {
      type: "boolean"
    }
  }
};
schema.body = requestBodySchema;
module.exports = schema;
