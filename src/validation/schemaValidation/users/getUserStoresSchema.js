const commonJSONSchema = require("../common");
const schema = Object.create(commonJSONSchema);
schema.tags = ["users"];
schema.description =
  "This API provides the functionality to get all the premises on which user has access.";
schema.querystring = {
  type: "object",
  required: ["emailid"],
  properties: {
    emailid: {
      type: "string",
      minLength: 1
    }
  }
};
schema.response = {
  ...schema.response,
  "200": {
    type: "array",
    items: {
      type: "object",
      properties: {
        country: {
          type: "string",
          example: "AR"
        },
        businessunits: {
          type: "array",
          example: ["HomeImprovements"],
          items: {
            type: "string"
          }
        }
      }
    }
  }
};

module.exports = schema;
