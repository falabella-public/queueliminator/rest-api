const generateSlots = require("../slotsUtils");
const uuidv4 = require("uuid/v4");

jest.mock("uuid/v4");

describe("slot utils", () => {
  beforeAll(async () => {
    uuidv4.mockReturnValue("20da4f50-805e-11e9-9314-15634356145b");
  });

  it("should generate slots", () => {
    const actualSlots = generateSlots(
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        startTime: new Date("2020-05-12T04:30:00.000Z"),
        endTime: new Date("2020-05-12T11:30:00.000Z"),
        duration: 60,
        capacity: 10,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10
      },
      new Date("2020-05-12T04:30:00.000Z")
    );

    const expectedSlots = [
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        capacity: 10,
        duration: 60,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10,
        startTime: "2020-05-12T04:30:00.000Z",
        endTime: "2020-05-12T05:30:00.000Z",
        uuid: "20da4f50-805e-11e9-9314-15634356145b"
      },
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        capacity: 10,
        duration: 60,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10,
        startTime: "2020-05-12T05:30:00.000Z",
        endTime: "2020-05-12T06:30:00.000Z",
        uuid: "20da4f50-805e-11e9-9314-15634356145b"
      },
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        capacity: 10,
        duration: 60,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10,
        startTime: "2020-05-12T06:30:00.000Z",
        endTime: "2020-05-12T07:30:00.000Z",
        uuid: "20da4f50-805e-11e9-9314-15634356145b"
      },
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        capacity: 10,
        duration: 60,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10,
        startTime: "2020-05-12T07:30:00.000Z",
        endTime: "2020-05-12T08:30:00.000Z",
        uuid: "20da4f50-805e-11e9-9314-15634356145b"
      },
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        capacity: 10,
        duration: 60,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10,
        startTime: "2020-05-12T08:30:00.000Z",
        endTime: "2020-05-12T09:30:00.000Z",
        uuid: "20da4f50-805e-11e9-9314-15634356145b"
      },
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        capacity: 10,
        duration: 60,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10,
        startTime: "2020-05-12T09:30:00.000Z",
        endTime: "2020-05-12T10:30:00.000Z",
        uuid: "20da4f50-805e-11e9-9314-15634356145b"
      },
      {
        storeId: "c8537723-8dc2-460c-b968-41d9e28f543b",
        capacity: 10,
        duration: 60,
        serviceType: "PURCHASE",
        customerType: "normal",
        noOfPeopleAllowedDefault: 1,
        noOfPeopleAllowedMax: 2,
        qrCodeValidityBuffer: 10,
        startTime: "2020-05-12T10:30:00.000Z",
        endTime: "2020-05-12T11:30:00.000Z",
        uuid: "20da4f50-805e-11e9-9314-15634356145b"
      }
    ];

    expect(actualSlots).toEqual(expectedSlots);
  });
});
