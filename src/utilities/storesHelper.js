const generateUpdateString = store => {
  const getServices = services => {
    services = services.join(`","`);
    return `'{"` + services + `"}'`;
  };

  const values = [];
  if (store.country) values.push(`country = '${store.country}'`);
  if (store.businessUnit)
    values.push(`business_unit = '${store.businessUnit}'`);
  if (store.storeStatus) values.push(`store_status = '${store.storeStatus}'`);
  if (store.storeArea) values.push(`store_area = ${store.storeArea}`);
  if (store.recommendedLimit)
    values.push(`recommended_limit = ${store.recommendedLimit}`);
  if (store.legalLimit) values.push(`legal_limit = ${store.legalLimit}`);
  if (store.staffCount) values.push(`staff_count = ${store.staffCount}`);

  if (store.occupancyCount)
    values.push(`occupancy_count = ${store.occupancyCount}`);
  if (store.updatedAt) values.push(`updated_at = '${store.updatedAt}'`);
  if (store.customerType)
    values.push(`customer_type = ${getServices(store.customerType)}`);
  if (store.noOfPeopleAllowedDefault)
    values.push(
      `no_of_people_allowed_default = '${store.noOfPeopleAllowedDefault}'`
    );
  if (store.noOfPeopleAllowedMax)
    values.push(`no_of_people_allowed_max = '${store.noOfPeopleAllowedMax}'`);
  if (store.qrCodeValidityBuffer)
    values.push(`qr_code_validity_buffer = '${store.qrCodeValidityBuffer}'`);
  if (store.allowCurrentSlotBooking || store.allowCurrentSlotBooking === false)
    values.push(
      `allow_current_slot_booking = '${store.allowCurrentSlotBooking}'`
    );
  if (store.advanceBookingLimitDays)
    values.push(
      `advance_booking_limit_days = '${store.advanceBookingLimitDays}'`
    );

  return values.join(",");
};

const addServiceTypes = (stores, serviceTypes) => {
  if (serviceTypes.rowCount <= 0) {
    return stores.rowCount <= 0 ? null : stores.rows;
  }
  if (stores.rows <= 0) {
    return null;
  }
  const storesWithServiceTypes = [];
  const locations = new Set();
  stores.rows.map(store => {
    const serTypes = [];
    locations.add(store.location);
    for (let i = 0; i < serviceTypes.rows.length; i++) {
      if (store.uuid === serviceTypes.rows[i].storeId) {
        const serviceType = serviceTypes.rows[i].serviceType;
        const uuid = serviceTypes.rows[i].uuid;
        const values = { uuid, serviceType };
        serTypes.push(values);
      }
    }
    store.serviceTypes = serTypes;
    storesWithServiceTypes.push(store);
  });

  const response = [];
  locations.forEach(location => {
    const storesForLocation = [];
    storesWithServiceTypes.filter(store => {
      if (store.location === location)
        storesForLocation.push({
          uuid: store.uuid,
          storeName: store.storeName,
          serviceTypes: store.serviceTypes,
          timeZone: store.timeZone,
          mallId: store.mallId,
          defaultServiceType: store.defaultServiceType,
          allowCurrentSlotBooking: store.allowCurrentSlotBooking
        });
    });
    response.push({ location, stores: storesForLocation });
  });

  return response;
};

module.exports.storeHelper = {
  generateUpdateString,
  addServiceTypes
};
