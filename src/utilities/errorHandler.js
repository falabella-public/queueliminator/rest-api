const {
  INTERNAL_SERVER_ERROR,
  BAD_REQUEST,
  NOT_FOUND
} = require("http-status-codes");
const errorHandlerFactory = fastify => {
  const { ApiError } = fastify.coreErrors;
  return {
    repositoryErrorHandler: error => {
      const errorInfo = {
        status: INTERNAL_SERVER_ERROR,
        detail: error.detail || error.message
      };

      throw new ApiError(errorInfo);
    },
    apiErrorHandler: error => {
      let errorInfo = {};
      if (error.message === "EXISTING_BOOKING") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "user already has an active booking"
        };
      } else if (error.message === "INVALID_BOOKING_DETAILS") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "invalid slot"
        };
      } else if (error.message === "PEOPLE_COUNT_EXCEEDED") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "no of people exceeded maximum allowed limit"
        };
      } else if (error.message === "SLOT_RESERVATION_FAILED") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "failed to reserve the slot"
        };
      } else if (error.message === "BOOKING_NOT_FOUND") {
        errorInfo = {
          status: NOT_FOUND,
          detail: "No such booking found"
        };
      } else if (error.message === "SLOT_STORE_MISMATCH") {
        errorInfo = {
          status: NOT_FOUND,
          detail: "booking belongs to different store"
        };
      } else if (error.message === "BOOKING_EXPIRED") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "Booking has expired"
        };
      } else if (error.message === "FUTURE_BOOKING") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "Booking is of future date"
        };
      } else if (error.message === "ALREADY_CHECKED_OUT") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "User has already checkedout"
        };
      } else if (error.message === "CHECKOUT_ERROR") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "Can checkout only after 10s of checkin"
        };
      } else if (error.message === "USER_NOT_FOUND") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "user not found"
        };
      } else if (error.message === "STORE_NOT_FOUND") {
        errorInfo = {
          status: BAD_REQUEST,
          detail: "stores not found"
        };
      } else {
        errorInfo = {
          status: INTERNAL_SERVER_ERROR,
          detail: error.detail || error.message
        };
      }
      throw new ApiError(errorInfo);
    }
  };
};

module.exports = errorHandlerFactory;
