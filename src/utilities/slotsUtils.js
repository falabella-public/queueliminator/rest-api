const uuidv4 = require("uuid/v4");
const MS_PER_MINUTE = 60000;
const generateSlots = (
  {
    storeId,
    startTime,
    endTime,
    customerType,
    noOfPeopleAllowedDefault,
    noOfPeopleAllowedMax,
    qrCodeValidityBuffer
  },
  slotCreationStartDate,
  serviceType
) => {
  const finalStartDate = new Date(slotCreationStartDate);
  finalStartDate.setUTCHours(startTime.getUTCHours());
  finalStartDate.setUTCMinutes(startTime.getUTCMinutes());
  finalStartDate.setSeconds(startTime.getSeconds());

  const finalEndDate = new Date(slotCreationStartDate);
  finalEndDate.setUTCHours(endTime.getUTCHours());
  finalEndDate.setUTCMinutes(endTime.getUTCMinutes());
  finalEndDate.setUTCSeconds(endTime.getUTCSeconds());

  if (finalEndDate.getUTCHours() <= finalStartDate.getUTCHours()) {
    finalEndDate.setDate(finalEndDate.getDate() + 1);
  }

  const durationInMs = serviceType.duration * 1000 * 60;
  const parsedStartTime = new Date(finalStartDate).valueOf();
  const parsedEndTime = new Date(finalEndDate).valueOf();
  const availableDuration = parsedEndTime - parsedStartTime;
  const noOfSlots = Math.floor(availableDuration / durationInMs);

  const slots = [];
  for (let index = 0; index < noOfSlots; index++) {
    const currentSlotStartTime = parsedStartTime + index * durationInMs;
    const slot = {
      uuid: uuidv4(),
      storeId,
      capacity: serviceType.qrBookingCapacity,
      duration: serviceType.duration,
      serviceType: serviceType.serviceType,
      customerType,
      noOfPeopleAllowedDefault,
      noOfPeopleAllowedMax,
      qrCodeValidityBuffer,
      startTime: new Date(currentSlotStartTime).toISOString(),
      endTime: new Date(currentSlotStartTime + durationInMs).toISOString(),
      serviceTypeId: serviceType.uuid
    };
    slots.push(slot);
  }
  return slots;
};
const excludeSlotsAndGroupByServiceType = (slots, slotConfigs) => {
  const dataMap = new Map();
  for (let i = 0; i < slots.length; i++) {
    const slot = slots[i];
    const slotStartTime = new Date(slot.startTime);
    const slotEndTime = new Date(slot.endTime);
    const day = slotStartTime.getDay();
    for (let j = 0; j < slotConfigs.length; j++) {
      const slotConfig = slotConfigs[j];
      if (slotConfig.dayOfWeek == day && slotConfig.isOpen) {
        const startTime = new Date(slotConfig.startTime);
        const endTime = new Date(slotConfig.endTime);
        const configStartDate = new Date(slotStartTime);
        const configEndDate = new Date(slotStartTime);
        configStartDate.setUTCHours(startTime.getUTCHours());
        configStartDate.setUTCMinutes(startTime.getUTCMinutes());
        configStartDate.setSeconds(startTime.getSeconds());

        configEndDate.setUTCHours(endTime.getUTCHours());
        configEndDate.setUTCMinutes(endTime.getUTCMinutes());
        configEndDate.setUTCSeconds(endTime.getUTCSeconds());
        if (slotStartTime.getUTCHours() <= 6) {
          configStartDate.setDate(configStartDate.getDate() - 1);
        }
        if (configEndDate.getUTCHours() <= configStartDate.getUTCHours()) {
          configEndDate.setDate(configEndDate.getDate() + 1);
        }
        if (
          slotStartTime.getTime() >= configStartDate.getTime() &&
          slotEndTime.getTime() <= configEndDate.getTime()
        ) {
          setStatus(slot);
          const requiredValues = {};
          requiredValues.uuid = slot.uuid;
          requiredValues.startTime = slot.startTime;
          requiredValues.endTime = slot.endTime;
          requiredValues.serviceType = slot.serviceType;
          requiredValues.capacity = slot.capacity;
          requiredValues.reservedCapacity = slot.reservedCapacity;
          requiredValues.noOfPeopleAllowedMax = slot.noOfPeopleAllowedMax;
          requiredValues.noOfPeopleAllowedDefault =
            slot.noOfPeopleAllowedDefault;
          requiredValues.status = slot.status;
          const key = slot.serviceTypeId;
          if (dataMap.has(key)) {
            const data = dataMap.get(key);
            data.push(requiredValues);
            dataMap.set(key, data);
          } else {
            const filteredResponse = [];
            filteredResponse.push(requiredValues);
            dataMap.set(key, filteredResponse);
          }
        }
        break;
      }
    }
  }
  return map_to_object(dataMap);
};

function map_to_object(map) {
  const values = [];
  map.forEach((value, key) => {
    const object = {};
    object.serviceTypeId = key;
    object.slots = value;
    values.push(object);
  });
  return values;
}
function setStatus(slot) {
  const now = new Date();
  const utc_now = new Date(
    now.getUTCFullYear(),
    now.getUTCMonth(),
    now.getUTCDate(),
    now.getUTCHours(),
    now.getUTCMinutes(),
    now.getUTCSeconds(),
    now.getUTCMilliseconds()
  );

  let x = +(utc_now.getTime() - slot.duration * MS_PER_MINUTE);
  x -= new Date().getTimezoneOffset() * 60000;

  const slotStartTimeInMillis = new Date(slot.startTime).getTime();
  slot.status = x < slotStartTimeInMillis ? "active" : "expired";
}

module.exports = { generateSlots, excludeSlotsAndGroupByServiceType };
