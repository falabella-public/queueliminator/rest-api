const date_diff = (givenDate, currentDate, noOfHours) => {
  const diff = (currentDate.getTime() - givenDate.getTime()) / (1000 * 60 * 60);
  if (diff <= noOfHours) {
    return true;
  } else {
    return false;
  }
};
module.exports = { date_diff };
