const tranformCountryWise = data => {
  const dataMap = new Map();
  for (let i = 0; i < data.length; i++) {
    const country = data[i].country;
    if (dataMap.has(country)) {
      const values = dataMap.get(country);
      values.push(data[i].businessUnit);
      dataMap.set(country, values);
    } else {
      const values = [];
      values.push(data[i].businessUnit);
      dataMap.set(country, values);
    }
  }
  return map_to_object(dataMap);
};
function map_to_object(map) {
  const arrayData = [];
  map.forEach((value, key) => {
    const values = {};
    values.country = key;
    values.businessunits = value;
    arrayData.push(values);
  });
  return arrayData;
}
module.exports = tranformCountryWise;
