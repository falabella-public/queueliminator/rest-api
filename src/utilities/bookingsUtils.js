const moment = require("moment");
const transformDateWise = (bookings, qrCodeCheckins, walkin) => {
  const dataMap = new Map();

  for (let i = 0; i < bookings.length; i++) {
    const totalCount = bookings[i].totalBookings;
    const key = bookings[i].day;
    const values = { totalBookings: totalCount };
    dataMap.set(moment(key).format("DD/MM/YYYY"), values);
  }

  for (let i = 0; i < qrCodeCheckins.length; i++) {
    const key = moment(qrCodeCheckins[i].day).format("DD/MM/YYYY");
    const totalCheckedIn = qrCodeCheckins[i].totalCheckedIn;
    if (dataMap.has(key)) {
      const values = dataMap.get(key);
      values.totalCheckedIn = totalCheckedIn;
      dataMap.set(key, values);
    } else {
      const values = { totalCheckedIn: totalCheckedIn };
      dataMap.set(key, values);
    }
  }

  for (let i = 0; i < walkin.length; i++) {
    const key = moment(walkin[i].day).format("DD/MM/YYYY");
    const totalwalkins = walkin[i].totalwalkins;
    if (dataMap.has(key)) {
      const values = dataMap.get(key);
      values.totalwalkins = totalwalkins;
      dataMap.set(key, values);
    } else {
      const values = { totalwalkins: totalwalkins };
      dataMap.set(key, values);
    }
  }
  return map_to_object(dataMap);
};
const transformBUWise = results => {
  const dataMap = new Map();
  const bookingResults = results[0];
  const checkedinResults = results[1];
  const checkoutResults = results[2];
  const walkinsResult = results[3];
  const totalStores = results[4].rows[0];
  const walkoutResult = results[5];

  bookingResults.rows.forEach(bookings => {
    const data = bookings;
    if (data.businessUnit !== "Test") {
      if (checkedinResults.rowCount > 0) {
        const checkins = checkedinResults.rows;
        for (let i = 0; i < checkins.length; i++) {
          if (checkins[i].uuid === data.uuid) {
            data.noOfCheckins = checkins[i].checkins;
            break;
          }
        }
      }

      if (checkoutResults.rowCount > 0) {
        const checkouts = checkoutResults.rows;
        for (let j = 0; j < checkouts.length; j++) {
          if (checkouts[j].uuid === data.uuid) {
            data.noOfCheckouts = checkouts[j].checkouts;
            break;
          }
        }
      }
      if (walkinsResult.rowCount > 0) {
        const walkins = walkinsResult.rows;
        for (let j = 0; j < walkins.length; j++) {
          if (walkins[j].uuid === data.uuid) {
            data.noOfPeopleWalkedIn = walkins[j].noOfPeopleWalkedIn;
            break;
          }
        }
      }

      if (walkoutResult.rowCount > 0) {
        const walkout = walkoutResult.rows;
        for (let j = 0; j < walkout.length; j++) {
          if (walkout[j].uuid === data.uuid) {
            data.noOfPeopleWalkedOut = walkout[j].noOfPeopleWalkedOut;
            break;
          }
        }
      }
      const key = data.uuid;
      dataMap.set(key, data);
    }
  });

  walkinsResult.rows.forEach(walkins => {
    if (walkins.businessUnit !== "Test") {
      if (!dataMap.has(walkins.uuid)) {
        for (let j = 0; j < walkoutResult.rows.length; j++) {
          if (walkins.uuid == walkoutResult.rows[j].uuid) {
            walkins.noOfPeopleWalkedOut =
              walkoutResult.rows[j].noOfPeopleWalkedOut;
            break;
          }
        }
        dataMap.set(walkins.uuid, walkins);
      }
    }
  });
  walkoutResult.rows.forEach(walkout => {
    if (walkout.businessUnit !== "Test") {
      if (!dataMap.has(walkout.uuid)) {
        dataMap.set(walkout.uuid, walkout);
      }
    }
  });

  return transformByBU(dataMap, totalStores);
};
function map_to_object(map) {
  const values = [];
  map.forEach((value, key) => {
    value.date = key;
    values.push(value);
  });
  return values;
}
function mapToObjectWithKey(map) {
  const values = [];

  map.forEach((value, key) => {
    const obj = {};
    obj[key] = value;
    values.push(obj);
  });
  return values;
}
function transformByBU(map, totalStores) {
  const result = { totalStores };
  const buMap = new Map();

  map.forEach(value => {
    const bu = value.businessUnit;
    const country = value.country;
    if (buMap.has(bu)) {
      const countries = buMap.get(bu);
      let found = false;
      for (let i = 0; i < countries.length; i++) {
        if (countries[i].has(country)) {
          const vals = countries[i].get(country);
          vals.push(value);
          found = true;
          break;
        }
      }
      if (!found) {
        const vals = [];
        const countryMap = new Map();
        vals.push(value);
        countryMap.set(country, vals);
        countries.push(countryMap);
      }
    } else {
      const countryMap = new Map();
      const vals = [];
      vals.push(value);
      countryMap.set(country, vals);
      const countries = [countryMap];
      buMap.set(bu, countries);
    }
  });
  const buArr = [];

  buMap.forEach((value, key) => {
    const buObj = {};
    buObj.bu = key;
    const countryArr = [];
    value.forEach(countries => {
      const countryObj = {};
      countries.forEach((stores, countryKey) => {
        countryObj.country = countryKey;
        countryObj.stores = stores;
        countryArr.push(countryObj);
      });
    });
    buObj.countries = countryArr;
    buArr.push(buObj);
  });
  result.stats = buArr;
  return result;
}
const transformMap = dataMap => {
  return mapToObjectWithKey(dataMap);
};
module.exports = { transformDateWise, transformBUWise, transformMap };
