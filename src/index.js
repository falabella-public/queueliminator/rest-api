"use strict";
require("dotenv").config();
const _ = require("underscore");
const fastify = require("fastify")({
  logger: {
    redact: ["req.headers.authorization"],
    level: "info"
  },
  ajv: {
    customOptions: {
      removeAdditional: true,
      useDefaults: true,
      coerceTypes: true,
      allErrors: false,
      nullable: true
    },
    plugins: [require("ajv-keywords")]
  }
});
const autoload = require("fastify-autoload");
const path = require("path");
const postgres = require("./plugins/postgres");

const config = require("./plugins/config");
const auth = require("./plugins/auth");
const pubsub = require("./plugins/pubsub");
const swagger = require("./plugins/swagger");
const cors = require("./plugins/cors");
const errorHandler = require("./plugins/errorHandler");
const addSignalListeners = fastify => {
  async function close(fastify) {
    try {
      await stop(fastify);
    } catch (err) {
      fastify.log.error(`Error stopping server: ${err}`);
      process.exit(1);
    }
  }

  const closeOnce = _.once(close);

  const signals = ["SIGTERM", "SIGINT"];
  signals.forEach(signal => {
    process.on(signal, async () => {
      fastify.log.info(`Received ${signal}`);
      await closeOnce(fastify);
    });
  });
  return fastify;
};

const registerPlugins = fastify => {
  fastify.register(config);
  fastify.register(errorHandler);
  fastify.register(postgres);
  fastify.register(pubsub);

  if (process.env.IS_SWAGGER_DOC_ENABLED == "true") {
    fastify.register(swagger);
  }
  fastify.register(cors);
  if (process.env.AUTHENTICATION == "true") {
    fastify.register(auth);
  }
  fastify.register(autoload, {
    dir: path.join(__dirname, "plugins/repository"),
    ignorePattern: /^(__tests__)/
  });
  fastify.register(autoload, {
    dir: path.join(__dirname, "plugins/externalrestapis"),
    ignorePattern: /^(__tests__)/
  });
  fastify.register(autoload, {
    dir: path.join(__dirname, "api"),
    options: { prefix: "/api/v1" },
    ignorePattern: /^(__tests__|schema)/
  });
  fastify.register(autoload, {
    dir: path.join(__dirname, "model"),
    ignorePattern: /^(__tests__|schema)/
  });
  return fastify;
};
const start = async fastify => {
  fastify = registerPlugins(fastify);
  await fastify.ready();
  if (process.env.IS_SWAGGER_DOC_ENABLED == "true") {
    fastify.swagger();
  }
  await fastify.listen(fastify.config.PORT, function(err, address) {
    if (err) {
      fastify.log.error(err);
      process.exit(1);
    }
    fastify.log.info(`server listening on ${address}`);
  });
  fastify = addSignalListeners(fastify);
  return fastify;
};

const stop = async fastify => {
  fastify.log.info("Terminating service...");
  fastify.serviceAvailable = false;
  await fastify.close();
  fastify.log.info("Done.");
};

try {
  if (!process.argv[1].includes("jest")) {
    start(fastify);
  }
} catch (err) {
  // eslint-disable-next-line no-console
  console.error(
    err,
    `Invalid arg '${process.argv[2]}'. Please supply: 'start' OR 'docs'`
  );
}
