const { OK, NOT_FOUND, INTERNAL_SERVER_ERROR } = require("http-status-codes");
const {
  generateSlotsSchema,
  getSlotsSchema,
  getRunningSlotSchema,
  deleteSlotsSchema,
  updateSlotsSchema,
  getSpecificSlotSchema,
  createSpecificSlotSchema
} = require("../validation/schemaValidation/slots");

const slotsAPI = async fastify => {
  fastify.get("/", { schema: getSlotsSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get all slots for given storeid"
    });

    return fastify.slotRepository.getAll(req.query).then(data => {
      if (data === null) {
        reply.code(NOT_FOUND).send({ message: "No Data" });
      }
      reply.code(OK).send(data);
    });
  });

  fastify.get(
    "/running",
    { schema: getRunningSlotSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to   get current"
      });

      return fastify.slotRepository
        .getRunningSlot(req.query.storeid)
        .then(data => {
          data === null
            ? reply.code(NOT_FOUND).send("No Data")
            : reply.code(OK).send(data);
        });
    }
  );

  fastify.get("/:id", { schema: getSpecificSlotSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get slot by id"
    });

    const data = await fastify.slotRepository.getById(req.params.id);
    data === null
      ? reply.code(NOT_FOUND).send("No Data")
      : reply.code(OK).send(data);
  });

  fastify.get(
    "/generateslots",
    { schema: generateSlotsSchema },
    async (req, reply) => {
      fastify.log.info({
        message:
          "request received to generateslots for all the available stores for coming days, if not already created."
      });

      const data = await fastify.slotRepository.createSlots(req.query.storeid);
      data === null
        ? reply.code(INTERNAL_SERVER_ERROR).send({ message: "No Data" })
        : reply.code(OK).send(data);
    }
  );

  fastify.delete(
    "/deleteslots",
    { schema: deleteSlotsSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to delete slots"
      });

      const data = await fastify.slotRepository.deleteSlots(req.body);
      data === null
        ? reply.code(403).send("Don't have permission")
        : reply.code(OK).send(data);
    }
  );

  fastify.put("/update", { schema: updateSlotsSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to update slots"
    });

    const data = await fastify.slotRepository.updateSlots(req.body);
    data === null
      ? reply.code(404).send("No such slot")
      : reply.code(OK).send(data);
  });

  fastify.post("/generatespecificslots", { schema: createSpecificSlotSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to update slots"
    });

    const data = await fastify.slotRepository.createSpecificSlots(req.body);
    data === null
      ? reply.code(404).send("No such slot")
      : reply.code(OK).send(data);
  });
};

module.exports = slotsAPI;
module.exports.autoPrefix = "/slots";