const { OK, NOT_FOUND } = require("http-status-codes");
const errorHandlerFactory = require("../utilities/errorHandler");
const {
  updateBookingStatusSchema,
  postBookingsSchema,
  getBookingQrSchema,
  getBookingServiceQrSchema,
  getLatestBookingsSchema,
  getAllUserBookingsSchema,
  expirePastBookingsSchema
} = require("../validation/schemaValidation/bookings");
function verifyBooking(fastify, bookingData, slotData, storeData, storeId) {
  let status, message;
  if (storeData.uuid !== storeId) {
    status = "error";
    message = "booking is of a different store";

    fastify.log.info({
      message: "AUDIT LOG",
      status: "STORE ID MISMATCH",
      userId: bookingData.userId
    });
  }

  if (bookingData.status === "expired") {
    status = "error";
    message = "booking is expired";

    fastify.log.info({
      message: "AUDIT LOG",
      status: "QR CODE EXPIRED",
      userId: bookingData.userId
    });
  }

  if (bookingData.status === "checkedout") {
    status = "error";
    message = "ALREADY_CHECKED_OUT";

    fastify.log.info({
      message: "AUDIT LOG",
      status: "SCANNED CHECKEDOUT CODE",
      userId: bookingData.userId
    });
  }

  if (status !== "error") {
    status = "success";
    message = "success";
  }
  return { status, message };
}

const bookingsAPI = async fastify => {
  const { apiErrorHandler } = errorHandlerFactory(fastify);
  fastify.post(
    "/:id/updatestatus",
    { schema: updateBookingStatusSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to update status of booking"
      });

      //1. active -> checkedIn -> checkedOut
      //1. active -> expired

      const storeId = req.body.storeId;

      const bookingData = await fastify.bookingRepository.getById(
        req.params.id
      );
      if (bookingData === null) {
        reply.code(NOT_FOUND).send("No Data");
      }

      fastify.log.info({
        message: "AUDIT LOG",
        status: "QR CODE SCANNED",
        userId: bookingData.userId
      });

      const slotData = await fastify.slotRepository.getById(bookingData.slotId);
      let storeData = await fastify.storeRepository.getById(slotData.storeId);

      const bookingResponse = verifyBooking(
        fastify,
        bookingData,
        slotData,
        storeData,
        storeId
      );
      const { status } = bookingResponse;
      let { message } = bookingResponse;

      if (status === "error") {
        apiErrorHandler({ code: 400, status, message });
      }

      if (status === "expired") {
        fastify.bookingRepository.setBookingState(bookingData.uuid, "expired");
      }

      let updatedSlotData = {};
      if (bookingData.status === "active") {
        fastify.bookingRepository.setBookingState(
          bookingData.uuid,
          "checkedin"
        );
        const updatedStoreData = await fastify.storeRepository.walkIn(
          storeData.uuid,
          bookingData.noOfPeople,
          "qrcode"
        );
        storeData.occupancyCount = updatedStoreData.occupancyCount;
        updatedSlotData = await fastify.slotRepository.incrementShowUp(
          bookingData.slotId,
          bookingData.noOfPeople
        );

        message = "checkedin";
        fastify.log.info({
          message: "AUDIT LOG",
          status: "CHECKED IN",
          userId: bookingData.userId
        });
      }

      if (bookingData.status === "checkedin") {
        fastify.bookingRepository.setBookingState(
          bookingData.uuid,
          "checkedout"
        );
        const updatedStoreData = await fastify.storeRepository.walkOut(
          storeData.uuid,
          bookingData.noOfPeople,
          "checkedout"
        );
        storeData.occupancyCount = updatedStoreData.occupancyCount;
        message = "checkedout";
        fastify.log.info({
          message: "AUDIT LOG",
          status: "CHECKED OUT",
          userId: bookingData.userId
        });
      }
      if (message === "checkedout" || message === "checkedin") {
        fastify.firebaseService.updateFirebase(
          fastify.config,
          storeData,
          updatedSlotData
        );
      }
      if (message === "checkedin") {
        fastify.pubsub.publishCheckinTopic(bookingData, storeData);
      }
      if (message === "checkedout") {
        fastify.pubsub.publishCheckoutTopic(bookingData, storeData);
      }
      reply.code(OK).send({ status, message, storeData: storeData });
    }
  );
  fastify.get("/:id/qr", { schema: getBookingQrSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get booking id qr info"
    });

    const bookingData = await fastify.bookingRepository.getById(req.params.id);
    if (bookingData === null) {
      reply.code(NOT_FOUND).send("No Data");
    }

    const slotData = await fastify.slotRepository.getById(bookingData.slotId);
    const storeData = await fastify.storeRepository.getById(slotData.storeId);

    const sTime = new Date(slotData.startTime);
    if (bookingData.status === "active" && sTime.getTime() > Date.now()) {
      const day = sTime.getUTCDay();
      const slotConfig = await fastify.slotconfigRepository.getBasedOnDays(
        day,
        slotData.storeId
      );
      const slotStartTime = new Date(slotData.startTime);

      const slotConfigStartTime = new Date(slotConfig[0].startTime);
      const slotConfigEndTime = new Date(slotConfig[0].endTime);
      slotStartTime.setUTCHours(slotConfigStartTime.getUTCHours());
      slotStartTime.setUTCMinutes(slotConfigStartTime.getUTCMinutes());
      slotStartTime.setUTCSeconds(slotConfigStartTime.getUTCSeconds());

      const slotEndTime = new Date(slotData.startTime);
      slotEndTime.setUTCHours(slotConfigEndTime.getUTCHours());
      slotEndTime.setUTCMinutes(slotConfigEndTime.getUTCMinutes());
      slotEndTime.setSeconds(slotConfigEndTime.getSeconds());

      if (slotEndTime.getUTCHours() <= slotStartTime.getUTCHours()) {
        slotEndTime.setDate(slotEndTime.getDate() + 1);
      }

      if (
        slotData.startTime.getTime() >= slotEndTime.getTime() ||
        slotData.endTime.getTime() <= slotStartTime.getTime()
      ) {
        bookingData.status = "cancelled";
      }
    }
    if (
      bookingData.status === "active" &&
      slotData.endTime.getTime() <= Date.now()
    ) {
      await fastify.bookingRepository.setBookingState(
        bookingData.uuid,
        "expired"
      );
      bookingData.status = "expired";
    }

    const { startTime, endTime } = slotData;
    const { businessUnit, location, storeName, timeZone } = storeData;

    const outData = {
      ...bookingData,
      startTime,
      endTime,
      businessUnit,
      location,
      storeName,
      timeZone
    };
    outData.storeId = slotData.storeId;
    reply.code(OK).send(outData);
  });

  fastify.get(
    "/:id/serviceqr",
    { schema: getBookingServiceQrSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get booking id qr info"
      });

      const bookingData = await fastify.bookingRepository.getById(
        req.params.id
      );
      if (bookingData === null) {
        reply.code(NOT_FOUND).send("No Data");
      }

      const slotData = await fastify.slotRepository.getById(bookingData.slotId);
      const storeData = await fastify.storeRepository.getById(slotData.storeId);

      const sTime = new Date(slotData.startTime);
      const qrCodeValidityBuffer = slotData.qrCodeValidityBuffer;
      if (bookingData.status === "active" && sTime.getTime() > Date.now()) {
        const day = sTime.getUTCDay();
        const slotConfig = await fastify.slotconfigRepository.getBasedOnDays(
          day,
          slotData.storeId
        );
        const slotStartTime = new Date(slotData.startTime);

        const slotConfigStartTime = new Date(slotConfig[0].startTime);
        const slotConfigEndTime = new Date(slotConfig[0].endTime);
        slotStartTime.setUTCHours(slotConfigStartTime.getUTCHours());
        slotStartTime.setUTCMinutes(slotConfigStartTime.getUTCMinutes());
        slotStartTime.setUTCSeconds(slotConfigStartTime.getUTCSeconds());

        const slotEndTime = new Date(slotData.startTime);
        slotEndTime.setUTCHours(slotConfigEndTime.getUTCHours());
        slotEndTime.setUTCMinutes(slotConfigEndTime.getUTCMinutes());
        slotEndTime.setSeconds(slotConfigEndTime.getSeconds());

        if (slotEndTime.getUTCHours() <= slotStartTime.getUTCHours()) {
          slotEndTime.setDate(slotEndTime.getDate() + 1);
        }

        if (
          slotData.startTime.getTime() >= slotEndTime.getTime() ||
          slotData.endTime.getTime() <= slotStartTime.getTime()
        ) {
          bookingData.status = "cancelled";
        }
      }
      const eTime = new Date(slotData.endTime);
      eTime.setMinutes(eTime.getMinutes() + qrCodeValidityBuffer);

      if (bookingData.status === "active" && eTime.getTime() <= Date.now()) {
        await fastify.bookingRepository.setBookingState(
          bookingData.uuid,
          "expired"
        );
        bookingData.status = "expired";
      } else {
        const currentSlot = await fastify.slotRepository.getServiceRunningSlot(
          slotData.storeId,
          slotData.serviceType
        );
        if (!currentSlot || currentSlot.endTime <= slotData.startTime) {
          bookingData.status = "future";
        }
      }

      const { startTime, endTime } = slotData;
      const { businessUnit, location, storeName, timeZone } = storeData;

      const outData = {
        ...bookingData,
        startTime,
        endTime,
        businessUnit,
        location,
        storeName,
        timeZone
      };
      outData.storeId = slotData.storeId;
      reply.code(OK).send(outData);
    }
  );

  fastify.get(
    "/latest",
    { schema: getLatestBookingsSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get latest booking of a user"
      });

      const data = await fastify.bookingRepository.getLatestBooking(req.query);
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );

  fastify.post(
    "/detail",
    { schema: {} },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get latest booking of a user"
      });

      const data = await fastify.bookingRepository.getBookingDetail(req.body);
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );

  fastify.post(
    "/download",
    { schema: {} },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get download bookings data"
      });

      const data = await fastify.bookingRepository.getRawBookingData(req.body);
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );

  fastify.post(
    "/downloadwalkins",
    { schema: {} },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get download walkin data"
      });

      const data = await fastify.bookingRepository.getRawWalkinData(req.body);
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );

  fastify.post("/", { schema: postBookingsSchema }, async (req, reply) => {
    const booking = req.body;

    fastify.log.info({
      message: "request received to create a new booking"
    });
    if (!booking.slotId || !booking.userId) {
      apiErrorHandler({ message: "BAD_REQUEST" });
    }

    const slotStoreData = await fastify.slotRepository.getByIdWithStore(
      booking.slotId
    );
    if (!slotStoreData) {
      apiErrorHandler({ message: "INVALID_BOOKING_DETAILS" });
    }

    const slotData = {
      storeId: slotStoreData.storeId,
      startTime: slotStoreData.startTime,
      endTime: slotStoreData.endTime,
      noOfPeopleAllowedMax: slotStoreData.noOfPeopleAllowedMax,
      uuid: slotStoreData.uuid
    };

    const storeData = {
      storeName: slotStoreData.storeName,
      businessUnit: slotStoreData.businessUnit,
      uuid: slotStoreData.storeUuid,
      location: slotStoreData.location,
      timeZone: slotStoreData.timeZone,
      storeType: slotStoreData.storeType,
      occupancyCount: slotStoreData.occupancyCount,
      recommendedLimit: slotStoreData.recommendedLimit,
      legalLimit: slotStoreData.legalLimit,
      country: slotStoreData.country,
      termsCondition: slotStoreData.termsCondition
    };

    const storeBookingResponse = await fastify.bookingRepository.createStoreInMallBooking(
      booking,
      slotData
    );
    if (booking.bookingType === "storeOnly") {
      storeBookingResponse.push({
        storeId: slotData.storeId,
        storeName: storeData.storeName,
        businessUnit: storeData.businessUnit,
        error: "CUSTOMER_ALREADY_IN_MALL"
      });
    } else if (booking.noOfPeople > slotData.noOfPeopleAllowedMax) {
      storeBookingResponse.push({
        storeId: slotData.storeId,
        storeName: storeData.storeName,
        businessUnit: storeData.businessUnit,
        error: "PEOPLE_COUNT_EXCEEDED"
      });
    } else {
      //check mall booking
      const mallBooking = await fastify.bookingRepository.checkExistingBooking(
        slotData.uuid,
        booking.userId
      );
      if (mallBooking <= 0) {
        const reservationStatus = await fastify.slotRepository.createReservation(
          booking.slotId,
          booking.noOfPeople
        );

        if (reservationStatus === null) {
          apiErrorHandler({ message: "SLOT_RESERVATION_FAILED" });
        }

        const data = await fastify.bookingRepository.insert(booking);
        const { startTime, endTime } = slotData;
        const {
          businessUnit,
          location,
          storeName,
          uuid,
          timeZone,
          storeType
        } = storeData;

        const bookingResponse = {
          ...data,
          startTime,
          endTime,
          businessUnit,
          location,
          storeName,
          storeId: uuid,
          timeZone,
          storeType
        };
        storeBookingResponse.push(bookingResponse);
        if (booking.emailId) {
          fastify.emailSender.sendEmail(
            bookingResponse,
            fastify.config,
            booking,
            storeData
          );
          fastify.opEmailService.sendEmail(fastify.config, booking, storeData, bookingResponse);
          fastify.pubsub.publishEnrollTopic(
            booking,
            bookingResponse,
            storeData
          );
          fastify.pubsub.publishBookingTopic(
            booking,
            bookingResponse,
            storeData
          );
        }
        const now = new Date().toISOString();
        if (
          slotData.startTime.toISOString() <= now &&
          slotData.endTime.toISOString() >= now
        ) {
          fastify.firebaseService.updateFirebase(
            fastify.config,
            storeData,
            reservationStatus
          );
        }
      } else {
        storeBookingResponse.push({
          storeId: slotData.storeId,
          storeName: storeData.storeName,
          businessUnit: storeData.businessUnit,
          error: "USER_ALREADY_HAS_BOOKING"
        });
      }
    }
    reply.code(OK).send(storeBookingResponse);
  });

  fastify.get(
    "/user/:id",
    { schema: getAllUserBookingsSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get all lasted bookings per BU"
      });

      const data = await fastify.bookingRepository.getAllBuBookings(
        req.params.id,
        req.query
      );
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );

  fastify.get(
    "/expirepastbookings",
    { schema: expirePastBookingsSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to expire past bookings"
      });

      const data = await fastify.bookingRepository.expirePastBookings();
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );

  fastify.post(
    "/bookingsbybu",
    { schema: {} },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get bookings by BU"
      });

      const data = await fastify.bookingRepository.bookingsByBU(req.body);
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );
};

module.exports = bookingsAPI;
module.exports.autoPrefix = "/bookings";
