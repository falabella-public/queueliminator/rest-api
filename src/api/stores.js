const { OK, NOT_FOUND } = require("http-status-codes");
const errorHandlerFactory = require("../utilities/errorHandler");
const {
  postWalkInSchema,
  postWalkOutSchema,
  onBoardStoreSchema,
  getStoresSchema,
  getBusinessUnitsSchema,
  getSlotSchema,
  getBusinessUnitSchema,
  getUserStoresSchema,
  getDashboardSchema,
  getMallsSchema,
  getServiceTypesSchema,
  getSpecificStoreSchema,
  resetoccupancySchema,
  updateMallSchema,
  updatecountSchema,
  editStoreSchema,
  getStoreForWebSchema,
  updateStoreSchema,
  getMallstoresSchema
} = require("../validation/schemaValidation/stores");

const storesAPI = async fastify => {
  const { apiErrorHandler } = errorHandlerFactory(fastify);
  fastify.get("/", { schema: getStoresSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get all stores"
    });

    const data = await fastify.storeRepository.getAll(req.query);
    if (data === null) {
      apiErrorHandler({ message: "STORE_NOT_FOUND" });
    }
    reply.code(OK).send(data);
  });

  fastify.get(
    "/:id",
    { schema: getSpecificStoreSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get store by id for app"
      });

      return fastify.storeRepository.getByIdForApp(req.params.id).then(data => {
        data === null
          ? reply.code(NOT_FOUND).send("No Data")
          : reply.code(OK).send(data);
      });
    }
  );

  fastify.get(
    "/:id/web",
    { schema: getStoreForWebSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get store by id for web"
      });

      return fastify.storeRepository.getByIdWeb(req.params.id).then(data => {
        data === null
          ? reply.code(NOT_FOUND).send("No Data")
          : reply.code(OK).send(data);
      });
    }
  );

  fastify.get(
    "/:id/dashboard",
    { schema: getDashboardSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get store by id for web"
      });

      return fastify.storeRepository.getById(req.params.id).then(data => {
        data === null
          ? reply.code(NOT_FOUND).send("No Data")
          : reply.code(OK).send(data);
      });
    }
  );

  fastify.put("/:id", { schema: updateStoreSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to put store by id"
    });

    const {
      body,
      params: { id }
    } = req;
    const data = await fastify.storeRepository.updateStore(id, body);
    if (data === null) {
      reply.code(NOT_FOUND).send("No Data");
    } else {
      data.storeTimeUpdated = true;
      fastify.firebaseService.updateFirebase(fastify.config, data);
      reply.code(OK).send(data);
    }
  });

  fastify.put(
    "/updatecount",
    { schema: updatecountSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to add automatic counter value"
      });

      const { body } = req;
      return fastify.storeRepository.updateCount(body).then(res => {
        reply.code(OK).send(res);
      });
    }
  );

  fastify.post("/:id", { schema: editStoreSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to update store name and location"
    });

    const {
      body,
      params: { id }
    } = req;
    const data = await fastify.storeRepository.editStore(id, body);
    data === null
      ? reply.code(NOT_FOUND).send("No Data")
      : reply.code(OK).send(data);
  });

  fastify.post(
    "/:id/walkin",
    { schema: postWalkInSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received for walkin"
      });

      const data = await fastify.storeRepository.walkIn(
        req.params.id,
        req.body.noOfPeople,
        "walkin"
      );
      fastify.firebaseService.updateFirebase(fastify.config, data);
      reply.code(OK).send(data);
    }
  );
  fastify.post(
    "/:id/walkout",
    { schema: postWalkOutSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received for walkout"
      });

      const data = await fastify.storeRepository.walkOut(
        req.params.id,
        req.body.noOfPeople,
        "walkout"
      );
      fastify.firebaseService.updateFirebase(fastify.config, data);
      reply.code(OK).send(data);
    }
  );

  fastify.get(
    "/resetoccupancy",
    { schema: resetoccupancySchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to reset occupancy count"
      });
      const data = await fastify.storeRepository.resetOccupancy(
        req.query.country
      );
      reply.code(OK).send(data);
    }
  );

  fastify.get(
    "/businessunits",
    { schema: getBusinessUnitsSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get active BUs"
      });
      const data = await fastify.storeRepository.getActiveBUs(
        req.query.country,
        req.query.office
      );
      const values = [];
      data.forEach(value => {
        values.push(value.businessUnit);
      });
      reply.code(OK).send(values);
    }
  );

  fastify.get(
    "/servicetypes",
    { schema: getServiceTypesSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get service types"
      });
      const data = await fastify.storeRepository.getServiceTypes(
        req.query.businessunit
      );
      if (data === null) {
        apiErrorHandler({ message: "SERVICE_TYPE_NOT_FOUND" });
      }
      reply.code(OK).send(data);
    }
  );

  fastify.post(
    "/userstores",
    { schema: getUserStoresSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get user specific stores based on role"
      });
      const data = await fastify.storeRepository.getAllUserStores(req.body);
      if (data === null) {
        apiErrorHandler({ message: "STORE_NOT_FOUND" });
      }

      const locations = new Set();
      data.forEach(store => {
        locations.add(store.location);
      });

      const responseObject = [];
      locations.forEach(location => {
        const storesForLocation = [];
        data.filter(store => {
          if (store.location === location)
            storesForLocation.push({
              uuid: store.uuid,
              storeName: store.storeName,
              serviceTypes: store.serviceTypes
            });
        });

        responseObject.push({ location, stores: storesForLocation });
      });

      reply.code(OK).send(responseObject);
    }
  );
  fastify.post(
    "/onboardstore",
    { schema: onBoardStoreSchema },
    async (req, reply) => {
      const data = await fastify.storeRepository.createStore(req.body);
      reply.code(OK).send(data);
    }
  );

  fastify.get("/:id/slots", { schema: getSlotSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get store slots"
    });

    return fastify.storeRepository
      .getByIdWithRunningSlot(req.params.id)
      .then(data => {
        data === null
          ? reply.code(NOT_FOUND).send("No Data")
          : reply.code(OK).send(data);
      });
  });

  fastify.get(
    "/:id/mallstores",
    { schema: getMallstoresSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get stores within mall"
      });

      return fastify.storeRepository
        .getStoresWithinMall(req.params.id)
        .then(data => {
          data === null
            ? reply.code(NOT_FOUND).send("No Data")
            : reply.code(OK).send(data);
        });
    }
  );

  fastify.get("/:id/malls", { schema: getMallsSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get all malls"
    });

    return fastify.storeRepository
      .getMalls(req.params.id, req.query.country)
      .then(data => {
        data === null
          ? reply.code(NOT_FOUND).send("No Data")
          : reply.code(OK).send(data);
      });
  });

  fastify.post(
    "/:id/updatemall",
    { schema: updateMallSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to update mall"
      });

      return fastify.storeRepository
        .updateMall(req.params.id, req.body)
        .then(data => {
          data === null
            ? reply.code(NOT_FOUND).send("No Data")
            : reply.code(OK).send(data);
        });
    }
  );

  fastify.post(
    "/currentstatus",
    { schema: {} },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get current store status"
      });
      return fastify.storeRepository
        .getStoreCurrentStatus(req.body)
        .then(data => {
          data === null
            ? reply.code(NOT_FOUND).send("No Data")
            : reply.code(OK).send(data);
        });
    }
  );

  fastify.get(
    "/businessunit",
    { schema: getBusinessUnitSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get BUs"
      });
      return fastify.storeRepository
        .getBusinessUnits(req.query.country)
        .then(data => {
          data === null
            ? reply.code(NOT_FOUND).send("No Data")
            : reply.code(OK).send(data);
        });
    }
  );
  fastify.get(
    "/countries",
    { schema: {} },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get countries"
      });
      return fastify.storeRepository
        .getCountries()
        .then(data => {
          data === null
            ? reply.code(NOT_FOUND).send("No Data")
            : reply.code(OK).send(data);
        });
    }
  );
};

module.exports = storesAPI;
module.exports.autoPrefix = "/stores";
