const { OK, NOT_FOUND } = require("http-status-codes");
const {
  getServiceTypesSchema,
  deleteServiceTypesSchema,
  updateServiceTypesSchema,
  postServiceTypeSchema
} = require("../validation/schemaValidation/servicetypes");
const serviceTypesAPI = async fastify => {
  fastify.get("/:id", { schema: getServiceTypesSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to fetch service types"
    });
    const data = await fastify.servicetypesRepository.getServiceTypes(
      req.params.id
    );
    data === null
      ? reply.code(NOT_FOUND).send("No Data")
      : reply.code(OK).send(data);
  });

  fastify.put("/", { schema: updateServiceTypesSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to update service types"
    });

    const data = await fastify.servicetypesRepository.update(req.body);
    reply.code(OK).send(data);
  });

  fastify.post("/", { schema: postServiceTypeSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to insert service types"
    });

    const data = await fastify.servicetypesRepository.insert(req.body);
    reply.code(OK).send(data);
  });

  fastify.delete(
    "/",
    { schema: deleteServiceTypesSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to delete service type"
      });

      const data = await fastify.servicetypesRepository.deleteServiceType(
        req.body
      );
      reply.code(OK).send(data);
    }
  );
};

module.exports = serviceTypesAPI;
module.exports.autoPrefix = "/servicetypes";
