const { OK, NOT_FOUND } = require("http-status-codes");
const {
  postDownloadHistorySchema
} = require("../validation/schemaValidation/downloadhistory");
const downloadHistoryAPI = async fastify => {
  fastify.post(
    "/",
    { schema: postDownloadHistorySchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to fetch download history"
      });
      const data = await fastify.downloadhistoryRepository.getdownloadHistory(
        req.body
      );
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    }
  );
};

module.exports = downloadHistoryAPI;
module.exports.autoPrefix = "/downloadhistory";
