const { OK, NOT_FOUND } = require("http-status-codes");
const {
  getSlotConfigSchema,
  editSlotConfigSchema
} = require("../validation/schemaValidation/slotconfig");

const slotConfigApi = async fastify => {
  fastify.put("/", { schema: editSlotConfigSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to create slot configs"
    });
    const data = await fastify.slotconfigRepository.update(req.body);
    reply.code(OK).send(data);
  });

  fastify.get("/", { schema: getSlotConfigSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get all slots"
    });

    const data = await fastify.slotconfigRepository.getAll(req.query);
    data === null
      ? reply.code(NOT_FOUND).send("No Data")
      : reply.code(OK).send(data);
  });
};

module.exports = slotConfigApi;
module.exports.autoPrefix = "/slotconfig";