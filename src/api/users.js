const { OK, CREATED } = require("http-status-codes");
const errorHandlerFactory = require("../utilities/errorHandler");
const {
  getUsersSchema,
  getUserRoleAssociationSchema,
  getUserStoresSchema,
  getSpecificUserSchema,
  addBuSchema,
  addUserSchema,
  deleteBuSchema,
  deleteUserSchema
} = require("../validation/schemaValidation/users");

const usersAPI = async fastify => {
  const { apiErrorHandler } = errorHandlerFactory(fastify);
  fastify.get("/", { schema: getUsersSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get user by email"
    });

    const data = await fastify.userRepository.getByEmail(req.query.emailid);
    const { customtoken } = req.headers;
    if (customtoken === "true" || customtoken === true) {
      const token = await fastify.firebaseService.generateCustomToken(
        fastify.config
      );
      data.token = token;
    }
    if (data === null) {
      apiErrorHandler({ message: "USER_NOT_FOUND" });
    }
    reply.code(OK).send(data);
  });

  fastify.get(
    "/stores",
    { schema: getUserStoresSchema },
    async (req, reply) => {
      const emailId = req.query.emailid;
      fastify.log.info({
        message: "request received to get user by email"
      });

      const data = await fastify.userRepository.getUserStores(emailId);
      if (data === null) {
        apiErrorHandler({ message: "USER_NOT_FOUND" });
      }
      reply.code(OK).send(data);
    }
  );

  fastify.post("/user", { schema: addUserSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to add guard user"
    });

    const data = await fastify.userRepository.addUser(req.body);
    if (data === null) {
      apiErrorHandler({ message: "SOMETHING_WENT_WRONG" });
    }
    if (data === "USER ALREADY EXISTS") {
      apiErrorHandler({ message: "USER_ALREADY_EXISTS" });
    }
    reply.code(CREATED).send(data);
  });

  fastify.post("/delete", { schema: deleteUserSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to delete user"
    });

    const data = await fastify.userRepository.deleteUser(req.body);
    if (data === null) {
      apiErrorHandler({ message: "USER_NOT_FOUND" });
    }
    reply.code(OK).send(data);
  });

  fastify.get("/:id", { schema: getSpecificUserSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to get users of a store"
    });

    const data = await fastify.userRepository.getUsers(
      req.params.id,
      req.query
    );
    if (data === null) {
      apiErrorHandler({ message: "USER_NOT_FOUND" });
    }
    reply.code(OK).send(data);
  });

  fastify.put("/addbu", { schema: addBuSchema }, async (req, reply) => {
    fastify.log.info({
      message: "request received to add bu"
    });

    return fastify.userRepository.addBu(req.body).then(data => {
      if (data === null) {
        apiErrorHandler({ message: "USER_NOT_FOUND" });
      }
      reply.code(OK).send(data);
    });
  });

  fastify.delete(
    "/deletebu",
    { schema: deleteBuSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to delete bu"
      });

      return fastify.userRepository.deleteBu(req.body).then(data => {
        if (data === null) {
          apiErrorHandler({ message: "USER_NOT_FOUND" });
        }
        reply.code(OK).send(data);
      });
    }
  );

  fastify.post(
    "/:id",
    { schema: getUserRoleAssociationSchema },
    async (req, reply) => {
      fastify.log.info({
        message: "request received to get user role permissions"
      });

      return fastify.userRepository
        .getUserRoleAssociation(req.params.id, req.body)
        .then(data => {
          if (data === null) {
            apiErrorHandler({ message: "USER_NOT_FOUND" });
          }
          reply.code(OK).send(data);
        });
    }
  );
};

module.exports = usersAPI;
module.exports.autoPrefix = "/users";
