const { OK, NOT_FOUND } = require("http-status-codes");
const businessUnitAPI = async fastify => {
  fastify.get("/", { schema: {} }, async (req, reply) => {
    fastify.log.info({
      message: "request received to fetch business unit"
    });
    const data = await fastify.businessunitRepository.getBusinessUnit(
      req.query.businessunit
    );
    data === null
      ? reply.code(NOT_FOUND).send("No Data")
      : reply.code(OK).send(data);
  });
  fastify.post("/", { schema: {} }, async (req, reply) => {
    fastify.log.info({
      message: "request received to add/update business unit"
    });
    return fastify.businessunitRepository.updateBusinessUnit(
      req.body
    ).then(data => {
      data === null
        ? reply.code(NOT_FOUND).send("No Data")
        : reply.code(OK).send(data);
    })

  });

};

module.exports = businessUnitAPI;
module.exports.autoPrefix = "/businessunit";
