const fastifySwagger = require("fastify-swagger");
const fastifyPlugin = require("fastify-plugin");
const swaggerConfig = {
  routePrefix: "/documentation",
  swagger: {
    info: {
      title: "QueuEliminator",
      description:
        "This project aims for creating REST API interfaces for implementing the slot booking mechanism for physical stores / malls / office spaces.",
      version: "0.1.0"
    },
    schemes: ["http", "https"],
    consumes: ["application/json"],
    produces: ["application/json"],
    tags: [
      { name: "stores", description: "User rest API end points" },
      { name: "servicetypes", description: "Servicetypes API end points" },
      { name: "slots", description: "Slots rest API end points" },
      { name: "slotconfig", description: "Slotconfig API end points" },
      { name: "users", description: "Users rest API end points" },
      { name: "bookings", description: "Bookings rest API end points" },
      {
        name: "downloadhistory",
        description: "Downloadhistory API end points"
      },
      {
        name: "general",
        description:
          "General rest API end points, includes slack and other integrations."
      }
    ]
  },
  exposeRoute: true
};

module.exports = fastifyPlugin(async fastify => {
  fastify.register(fastifySwagger, swaggerConfig);
});
