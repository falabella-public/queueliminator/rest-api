const fp = require("fastify-plugin");
const { PubSub } = require("@google-cloud/pubsub");

module.exports = fp(async (fastify, opts, next) => {
  const {
    PUB_SUB_CLIENT_EMAIL,
    PUB_SUB_PRIVATE_KEY,
    PUB_SUB_PROJECT_ID,
    PUB_SUB_ENROL_TOPIC,
    PUB_SUB_BOOKING_TOPIC,
    PUB_SUB_CHECKINOUT_TOPIC,
    MALL_BU
  } = fastify.config;
  const credentials = {
    client_email: PUB_SUB_CLIENT_EMAIL,
    private_key: PUB_SUB_PRIVATE_KEY
  };
  const pubSubClient = new PubSub({
    projectId: PUB_SUB_PROJECT_ID,
    credentials: credentials
  });
  const publishEnrollTopic = async (request, response, storeData) => {
    const { businessUnit } = storeData;
    if (businessUnit == MALL_BU) {
      const template = await fastify.enrolreq.enrolRequest(
        request,
        response,
        storeData
      );
      const data = JSON.stringify(template);
      const attributes = await fastify.enrolreq.enrolAttributes(
        response,
        storeData,
        template.customerIdentityNumber
      );
      const dataBuffer = Buffer.from(data);
      pubSubClient
        .topic(PUB_SUB_ENROL_TOPIC)
        .publish(dataBuffer, attributes)
        .then(res => {
          fastify.log.info({
            message: "enroll topic" + res + " published."
          });
        })
        .catch(err => {
          fastify.log.error({
            message: "Error while publishing enroll topic ",
            err
          });
        });
    }
  };
  const publishBookingTopic = async (request, response, storeData) => {
    const { businessUnit } = storeData;
    if (businessUnit == MALL_BU) {
      const template = await fastify.bookingreq.bookingRequest(
        request,
        response
      );
      const data = JSON.stringify(template);
      const attributes = await fastify.bookingreq.bookingAttributes(
        response,
        storeData,
        template.customerIdentityNumber
      );
      const dataBuffer = Buffer.from(data);
      pubSubClient
        .topic(PUB_SUB_BOOKING_TOPIC)
        .publish(dataBuffer, attributes)
        .then(res => {
          fastify.log.info({
            message: "booking topic" + res + " published."
          });
        })
        .catch(err => {
          fastify.log.error({
            message: "Error while publishing booking topic ",
            err
          });
        });
    }
  };
  const publishCheckinTopic = async (response, storeData) => {
    const { businessUnit } = storeData;
    if (businessUnit == MALL_BU) {
      const template = await fastify.checkinreq.checkinRequest(
        response,
        storeData
      );
      const data = JSON.stringify(template);
      const attributes = await fastify.checkinreq.checkinAttributes(
        storeData,
        template.customerIdentityNumber
      );
      const dataBuffer = Buffer.from(data);
      pubSubClient
        .topic(PUB_SUB_CHECKINOUT_TOPIC)
        .publish(dataBuffer, attributes)
        .then(res => {
          fastify.log.info({
            message: "chckin topic " + res + " published."
          });
        })
        .catch(err => {
          fastify.log.error({
            message: "Error while publishing checkin topic ",
            err
          });
        });
    }
  };
  const publishCheckoutTopic = async (response, storeData) => {
    const { businessUnit } = storeData;
    if (businessUnit == MALL_BU) {
      const template = await fastify.checkoutreq.checkoutRequest(
        response,
        storeData
      );
      const data = JSON.stringify(template);
      const attributes = await fastify.checkoutreq.checkoutAttributes(
        storeData,
        template.customerIdentityNumber
      );
      const dataBuffer = Buffer.from(data);
      pubSubClient
        .topic(PUB_SUB_CHECKINOUT_TOPIC)
        .publish(dataBuffer, attributes)
        .then(res => {
          fastify.log.info({
            message: "checkout topic" + res + " published."
          });
        })
        .catch(err => {
          fastify.log.error({
            message: "Error while publishing checkout topic ",
            err
          });
        });
    }
  };
  fastify.decorate("pubsub", {
    publishBookingTopic,
    publishCheckinTopic,
    publishCheckoutTopic,
    publishEnrollTopic
  });
  next();
});
