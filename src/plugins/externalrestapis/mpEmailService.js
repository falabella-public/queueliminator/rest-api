const fp = require("fastify-plugin");
const axios = require("axios");
const qs = require("querystring");
const generateHtml = require("../../utilities/emailTemplate");
module.exports = fp((fastify, options, next) => {
  const sendEmail = async (request, config, bookingRequest, storeData) => {
    const {
      HOST_DOMAIN,
      FROM_EMAIL_ID,
      USER_NAME,
      EMAIL_PWD,
      CM_REF,
      MALL_DOMAIN,
      MALL_BU,
      EMAIL_AUTH_HOST_URL,
      EMAIL_HOST_URL
    } = config;
    const { storeName, startTime, endTime, uuid, businessUnit } = request;
    if (businessUnit === MALL_BU) {
      const { emailId, userName } = bookingRequest;
      const { timeZone } = storeData;
      fastify.log.info({
        message: "send email",
        fromEmail: FROM_EMAIL_ID,
        toEmail: emailId,
        timeZone: timeZone
      });

      const authBody = {
        grant_type: "client_credentials"
      };
      const authConfig = {
        method: "post",
        url: EMAIL_AUTH_HOST_URL,
        data: qs.stringify(authBody),
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "X-cmRef": CM_REF,
          "X-rhsRef": MALL_DOMAIN,
          "X-chRef": "WEB",
          "X-country": "CL",
          "X-commerce": "Marketing",
          "X-usrTx": "Fila Virtual",
          "X-prRef": "WEB 2.0 Landings"
        },
        auth: {
          username: USER_NAME,
          password: EMAIL_PWD
        }
      };
      const data = {
        url: HOST_DOMAIN + "/api/v1/general/" + uuid,
        fromEmailAddress: FROM_EMAIL_ID,
        toEmailAddress: emailId,
        userName: userName,
        startTime: startTime,
        endTime: endTime,
        storeName: storeName,
        timeZone: timeZone
      };

      axios(authConfig)
        .then(res => {
          const access_token = res.data.access_token;

          const emailConfig = {
            method: "post",
            url: EMAIL_HOST_URL,
            data: generateHtml(data),
            headers: {
              "Content-Type": "application/json",
              "X-cmRef": CM_REF,
              "X-rhsRef": MALL_DOMAIN,
              "X-chRef": "WEB",
              "X-country": "CL",
              "X-commerce": "Marketing",
              "X-usrTx": "Fila Virtual",
              "X-prRef": "WEB 2.0 Landings",
              Authorization: "Bearer " + access_token,
              "X-txRef": uuid
            }
          };
          axios(emailConfig)
            .then(response => {
              fastify.log.info({
                message: "Email sent with status code " + response.status
              });
            })
            .catch(error => {
              fastify.log.error(error);
            });
        })
        .catch(error => {
          fastify.log.error(error);
        });
    }
  };
  fastify.decorate("emailSender", {
    sendEmail
  });
  next();
});
