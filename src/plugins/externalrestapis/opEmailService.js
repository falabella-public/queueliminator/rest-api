const fp = require("fastify-plugin");
const axios = require("axios");
const moment_timezone = require("moment-timezone");
const moment = require("moment");

module.exports = fp((fastify, options, next) => {
  const getToken = async (config) => {
    const {
      OP_CLIENT_ID,
      OP_CLIENT_SECRET,
      OP_AUDIENCE,
      OP_EMAIL_AUTH_HOST_URL,
    } = config;
    const tokenRequestBody = {
      grant_type: "client_credentials",
      audience: OP_AUDIENCE,
      client_secret: OP_CLIENT_SECRET,
      client_id: OP_CLIENT_ID
    };
    const authRequest = {
      method: "post",
      url: OP_EMAIL_AUTH_HOST_URL,
      data: tokenRequestBody,
      headers: {
        "content-type": "application/json"
      }
    };
    return axios(authRequest)
      .then(res => {
        return res.data.access_token
      }).catch(error => {
        fastify.log.error("Error getting op token:", error);
        throw new Error();
      });
  }
  const sendEmail = async (config, request, storeData, response) => {
    const {
      OP_TENANT_ID,
      OP_EMAIL_HOST_URL,
      OP_BU,
      HOST_DOMAIN
    } = config;
    const { emailId, userName } = request;
    const { businessUnit, storeName } = storeData;
    const { uuid, timeZone, startTime, endTime } = response;
    if (businessUnit === OP_BU) {
      fastify.log.info({
        message: "send email for op"
      });

      const startDate = moment.utc(startTime);
      const endDate = moment.utc(endTime);
      let date;
      let eDate;
      if (timeZone) {
        date = moment_timezone.tz(startDate, timeZone);
        eDate = moment_timezone.tz(endDate, timeZone);
      } else {
        date = moment_timezone.tz(startDate, "America/Santiago");
        eDate = moment_timezone.tz(endDate, "America/Santiago");
      }
      const localDate = date.format("DD/MM/YYYY");
      const localTime = date.format("hh:mm a");
      const localEndTime = eDate.format("hh:mm a");
      const emailRequestBody = {
        data: {
          notification: {
            action: "transactional_email",
            attributes: {
              to: {
                recipient: emailId
              },
              template: {
                id: "OPCL_Piloto_Agendamiento"
              }
            },
            content: {
              NOMBRE_CLIENTE: userName,
              NOMBRE_MALL: storeName,
              TEXTO_ADICIONAL: "",
              TEXTO_ADICIONAL_2: "",
              IMG_QR: HOST_DOMAIN + "/api/v1/general/" + uuid,
              FECHA_HORA: localDate + " de " + localTime + " a " + localEndTime
            }
          }
        }
      };

      fastify.firestoreService.getToken(config).then(token => {
        const emailRequest = {
          method: "post",
          url: OP_EMAIL_HOST_URL,
          data: emailRequestBody,
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + token,
            "x-tenant-id": OP_TENANT_ID
          }
        };

        axios(emailRequest)
          .then(response => {
            fastify.log.info({
              message: "Email sent with status code " + response.status
            });
          }).catch(error => {
            fastify.log.error("Error sending OP email", error);
          });
      }).catch(error => {
        fastify.log.error("Error getting token", error);
      });
    }
  };
  fastify.decorate("opEmailService", {
    sendEmail,
    getToken
  });
  next();
});
