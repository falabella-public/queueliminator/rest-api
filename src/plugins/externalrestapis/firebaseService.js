const fp = require("fastify-plugin");
const admin = require("firebase-admin");
const { v4: uuidv4 } = require("uuid");
module.exports = fp((fastify, options, next) => {
  const initializeAdmin = async config => {
    const {
      FIRESTORE_PPROJECT_ID,
      FIRESTORE_PRIVATE_KEY,
      FIRESTORE_CLIENT_EMAIL,
      REALTIME_DB_URL
    } = config;
    const cred = {
      project_id: FIRESTORE_PPROJECT_ID,
      private_key: FIRESTORE_PRIVATE_KEY,
      client_email: FIRESTORE_CLIENT_EMAIL
    };
    if (!admin.apps.length) {
      admin.initializeApp({
        credential: admin.credential.cert(cred),
        databaseURL: REALTIME_DB_URL
      });
    }
    return admin;
  };
  const generateCustomToken = async config => {
    const fireBaseAdmin = await fastify.firebaseService.initializeAdmin(config);
    return fireBaseAdmin
      .auth()
      .createCustomToken(uuidv4())
      .then(customToken => {
        return customToken;
      })
      .catch(error => {
        fastify.log.error("Error creating custom token:", error);
      });
  };
  const updateFirebase = async (config, storeData, slotData) => {
    const {
      uuid,
      occupancyCount,
      recommendedLimit,
      legalLimit,
      storeTimeUpdated
    } = storeData;
    if (!config.FIRESTORE_PPROJECT_ID) {
      return;
    }
    const fireBaseAdmin = await fastify.firebaseService.initializeAdmin(config);
    const db = fireBaseAdmin.database();
    const currentTime = new Date();
    const fbData = {
      timeStamp: currentTime.toISOString(),
      occupancyCount: occupancyCount,
      recommendedLimit: recommendedLimit,
      legalLimit: legalLimit
    };
    if (slotData) {
      if (slotData.storeId) {
        const capacityResult = await fastify.slotRepository.getReservedCapacityShowUps(slotData.storeId);
        if (capacityResult) {
          slotData.showUps = parseInt(capacityResult.showUps);
          slotData.reservedCapacity = parseInt(capacityResult.reservedCapacity);
        }
      }
      if (slotData.reservedCapacity) {
        fbData.reservedCapacity = slotData.reservedCapacity;
      }
      if (slotData.showUps) {
        fbData.showUps = slotData.showUps;
      }
    }
    if (storeTimeUpdated) {
      fbData.storeTimeUpdated = storeTimeUpdated;
    }

    const ref = db.ref("/");
    const docRef = ref.child(uuid);
    docRef
      .set(fbData)
      .then(res => {
        fastify.log.info("updated time for uuid ", uuid, res);
      })
      .catch(err => {
        fastify.log.error({
          message: "error while updating time",
          err
        });
      });
  };
  fastify.decorate("firebaseService", {
    updateFirebase,
    initializeAdmin,
    generateCustomToken
  });
  next();
});
