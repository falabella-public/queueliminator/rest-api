const fp = require("fastify-plugin");
const firestore = require("@google-cloud/firestore");

module.exports = fp((fastify, options, next) => {
  let db;
  const initializeDb = async config => {
    const {
      FIRESTORE_PPROJECT_ID,
      FIRESTORE_PRIVATE_KEY,
      FIRESTORE_CLIENT_EMAIL
    } = config;
    const cred = {
      private_key: FIRESTORE_PRIVATE_KEY,
      client_email: FIRESTORE_CLIENT_EMAIL
    };
    if (!db) {
      db = new firestore({
        projectId: FIRESTORE_PPROJECT_ID,
        credentials: cred
      });
    }
    return db;
  };

  const getToken = async (config) => {
    const { COLLECTION, DOCUMENT, VALIDATY_DAYS } = config;
    const firestoreDb = await fastify.firestoreService.initializeDb(config);
    return firestoreDb.collection(COLLECTION).doc(DOCUMENT).get().then(record => {
      if (record.data()) {
        const { token, date } = record.data();
        const today = new Date();
        const tokenDate = new Date(date);
        const timeDiff = today.getTime() - tokenDate.getTime();
        const daysDiff = Math.floor(timeDiff / (1000 * 3600 * 24));
        if (date && token && daysDiff <= VALIDATY_DAYS) {
          fastify.log.info("send from db");
          return token;
        }

      }
      return fastify.opEmailService.getToken(config).then(token => {
        return fastify.firestoreService.updateToken(config, token).then(res => {
          fastify.log.info("send from api :", res);
          return token;
        }).catch(error => {
          fastify.log.error("Error while updating document ", error);
        });
      }).catch(error => {
        fastify.log.error("Error while getting token ", error);
      });;
    }).catch(error => {
      fastify.log.error("Error while getting document ", error);
    });
  };

  const updateToken = async (config, token) => {
    const { COLLECTION, DOCUMENT } = config;
    const firestoreDb = await fastify.firestoreService.initializeDb(config);
    const docRef = firestoreDb.collection(COLLECTION).doc(DOCUMENT);
    const today = new Date();
    docRef.set({
      token: token,
      date: today.toISOString()
    }).then(res => res).catch(error => {
      fastify.log.error("Error updating token:", error);
    });
  };

  fastify.decorate("firestoreService", {
    initializeDb,
    updateToken,
    getToken
  });
  next();
});
