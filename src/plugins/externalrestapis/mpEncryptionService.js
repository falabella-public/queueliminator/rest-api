const fp = require("fastify-plugin");
const axios = require("axios");
const qs = require("querystring");

module.exports = fp((fastify, options, next) => {
  const encryptData = async (request, config) => {
    const {
      ENCYPT_AUTH_HOST_DOMAIN,
      ENCRYP_CLIENT_ID,
      ENCRYP_SECRET,
      ENCYPT_HOST_DOMAIN,
      ENCRYPT_CM_REF,
      MALL_BU,
      API_GOVERNANCE_DOMAIN
    } = config;

    fastify.log.info({
      message: "call api to encrypt data"
    });

    const authBody = {
      grant_type: "client_credentials",
      client_id: ENCRYP_CLIENT_ID,
      client_secret: ENCRYP_SECRET
    };
    const authApi = {
      method: "post",
      url: ENCYPT_AUTH_HOST_DOMAIN,
      data: qs.stringify(authBody),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    try {
      const authRes = await axios(authApi);
      const access_token = authRes.data.access_token;
      const encyptApi = {
        method: "post",
        url: ENCYPT_HOST_DOMAIN,
        data: JSON.stringify(request),
        headers: {
          "Content-Type": "application/json",
          "X-cmRef": ENCRYPT_CM_REF,
          "X-rhsRef": API_GOVERNANCE_DOMAIN,
          "X-chRef": "WEB",
          "X-country": "CL",
          "X-commerce": MALL_BU,
          Authorization: "Bearer " + access_token
        }
      };
      const encyptedResponse = await axios(encyptApi);
      return encyptedResponse.data;
    } catch (err) {
      fastify.log.info(err);
    }
  };
  fastify.decorate("encryption", {
    encryptData
  });
  next();
});
