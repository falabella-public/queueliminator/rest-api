const fastifyPlugin = require("fastify-plugin");
const fastifyEnv = require("fastify-env");

module.exports = fastifyPlugin((fastify, opts, next) => {
  const configSchema = {
    type: "object",
    properties: {
      NODE_ENV: {
        type: "string",
        default: "development"
      },
      HOST: {
        type: "string",
        default: "127.0.0.1"
      },
      PORT: {
        type: "integer",
        default: 4444
      },
      COUNTRY_CODE: {
        type: "string",
        default: "CL"
      },
      APP_CLIENT_ID: {
        type: "string"
      },
      WEB_CLIENT_ID: {
        type: "string"
      },
      JWT_SECRET: {
        type: "string"
      },
      TID: {
        type: "string"
      },
      HOST_DOMAIN: {
        type: "string"
      },
      EMAIL_AUTH_HOST_URL: {
        type: "string"
      },
      EMAIL_HOST_URL: {
        type: "string"
      },
      FROM_EMAIL_ID: {
        type: "string"
      },
      USER_NAME: {
        type: "string"
      },
      EMAIL_PWD: {
        type: "string"
      },
      CM_REF: {
        type: "string"
      },
      REALTIME_DB_URL: {
        type: "string"
      },
      FIRESTORE_PPROJECT_ID: {
        type: "string"
      },
      FIRESTORE_PRIVATE_KEY: {
        type: "string"
      },
      FIRESTORE_CLIENT_EMAIL: {
        type: "string"
      },
      PUB_SUB_CLIENT_EMAIL: {
        type: "string"
      },
      PUB_SUB_PRIVATE_KEY: {
        type: "string"
      },
      PUB_SUB_PROJECT_ID: {
        type: "string"
      },
      PUB_SUB_ENROL_TOPIC: {
        type: "string"
      },
      PUB_SUB_BOOKING_TOPIC: {
        type: "string"
      },
      PUB_SUB_CHECKINOUT_TOPIC: {
        type: "string"
      },
      ENCYPT_AUTH_HOST_DOMAIN: {
        type: "string"
      },
      ENCRYP_CLIENT_ID: {
        type: "string"
      },
      ENCRYP_SECRET: {
        type: "string"
      },
      ENCYPT_HOST_DOMAIN: {
        type: "string"
      },
      ENCRYPT_CM_REF: {
        type: "string"
      },
      OP_TENANT_ID: {
        type: "string"
      },
      OP_CLIENT_ID: {
        type: "string"
      },
      OP_CLIENT_SECRET: {
        type: "string"
      },
      OP_AUDIENCE: {
        type: "string"
      },
      OP_EMAIL_AUTH_HOST_URL: {
        type: "string"
      },
      OP_EMAIL_HOST_URL: {
        type: "string"
      },
      CACHE_TTL: {
        type: "integer"
      },
      AUTHENTICATION: {
        type: "boolean"
      },
      COLLECTION: {
        type: "string"
      },
      DOCUMENT: {
        type: "string"
      },
      VALIDATY_DAYS: {
        type: "integer"
      },
      MALL_BU: {
        type: "string"
      },
      MALL_DOMAIN: {
        type: "string"
      },
      API_GOVERNANCE_DOMAIN: {
        type: "string"
      },
      OP_BU: {
        type: "string"
      }
    }
  };

  fastify.register(fastifyEnv, {
    schema: configSchema,
    data: opts
  });

  next();
});
