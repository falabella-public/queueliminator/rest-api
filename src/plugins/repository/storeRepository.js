const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");
const uuidv4 = require("uuid/v4");
const { storeHelper } = require("../../utilities/storesHelper");

const errorHandlerFactory = require("../../utilities/errorHandler");
const config = require("../config/configs.js");

module.exports = fp((fastify, options, next) => {
  const { repositoryErrorHandler } = errorHandlerFactory(fastify);
  const getAll = async queryParams => {
    fastify.log.info({
      message: `Invoking repository to fetch all stores for ${queryParams.country}, 
      ${queryParams.businessunit}, ${queryParams.source}`
    });
    const sql = SQL`SELECT stores.*, time_zone, mall_id FROM stores LEFT JOIN storeadditionalinfo ON  storeadditionalinfo.store_id=stores.uuid where booking_enabled=true`;
    const serviceTypesSQL = SQL`select servicetypeconfigs.store_id, servicetypeconfigs.uuid, servicetypeconfigs.service_type  
                                from servicetypeconfigs, stores where servicetypeconfigs.store_id=stores.uuid and servicetypeconfigs.status='active' `;
    if (queryParams.country) {
      sql.append(SQL` AND country = ${queryParams.country}`);
      serviceTypesSQL.append(SQL` AND country = ${queryParams.country}`);
    }
    if (queryParams.office) {
      sql.append(SQL` AND store_type = 'office'`);
      serviceTypesSQL.append(SQL` AND store_type = 'office'`);
    } else {
      sql.append(SQL` AND store_type !='office'`);
      serviceTypesSQL.append(SQL` AND store_type !='office'`);
    }
    if (queryParams.source && config.sourceAsBU.includes(queryParams.source)) {
      sql.append(SQL` AND business_unit = ${queryParams.source}`);
      serviceTypesSQL.append(SQL` AND business_unit = ${queryParams.source}`);
    } else if (queryParams.businessunit) {
      sql.append(SQL` AND business_unit = ${queryParams.businessunit}`);
      serviceTypesSQL.append(
        SQL` AND business_unit = ${queryParams.businessunit}`
      );
    }
    serviceTypesSQL.append(SQL` order by servicetypeconfigs.service_type asc`);
    const promises = [];
    promises.push(fastify.pg.query(sql));
    promises.push(fastify.pg.query(serviceTypesSQL));
    return Promise.all(promises)
      .then(results => {
        const stores = results[0];
        const serviceTypes = results[1];
        return storeHelper.addServiceTypes(stores, serviceTypes);
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching stores"
        });
        repositoryErrorHandler(err);
      });
  };

  const getAllUserStores = async request => {
    fastify.log.info({
      message: `Invoking repository to fetch all stores for ${request.emailId}`
    });
    try {
      const sql = SQL`SELECT store_id, is_super_user
                      FROM users
                      where user_email=${request.emailId} and status='active' and user_role='store'`;
      const result = await fastify.pg.query(sql);
      if (result.rowCount > 0) {
        const userData = result.rows[0];

        let storeSql = SQL`SELECT uuid,location,store_name 
                         FROM stores where booking_enabled=true `;
        if (userData.storeId) {
          storeSql = SQL`SELECT stores.uuid,location,store_name 
                         FROM stores, users 
                         where booking_enabled=true and stores.uuid=users.store_id 
                         and users.user_email=${request.emailId} and users.user_role='store' 
                         and users.status='active'`;
        }

        storeSql.append(SQL` AND country = ${request.country}`);
        storeSql.append(
          SQL` AND business_unit = ${request.businessunit} order by location, store_name`
        );
        const storeResult = await fastify.pg.query(storeSql);
        return storeResult.rowCount <= 0 ? null : storeResult.rows;
      } else {
        return { message: "Invalid User" };
      }
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching stores"
      });
      repositoryErrorHandler(err);
    }
  };

  const getAllStores = async id => {
    fastify.log.info({
      message: `Invoking repository to fetch all stores`
    });
    const sql = SQL`SELECT * FROM stores where booking_enabled=true`;
    if (id) {
      sql.append(SQL` and uuid=${id}`);
    }
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching stores"
      });
      repositoryErrorHandler(err);
    }
  };

  const getByIdForApp = async id => {
    fastify.log.info({
      message: "Invoking repository to fetch store by id for app"
    });
    const sql = SQL`SELECT uuid, store_name, recommended_limit, legal_limit, occupancy_count from stores where uuid=${id}`;
    return fastify.pg
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows[0];
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching store by id for app"
        });
        repositoryErrorHandler(err);
      });
  };
  const getStoreByIds = async ids => {
    fastify.log.info({
      message:
        "Invoking repository to fetch store by id for store in mall booking"
    });
    const sql = `SELECT store_name, stores.uuid, business_unit, location, time_zone, store_type FROM stores 
                    LEFT JOIN storeadditionalinfo ON  storeadditionalinfo.store_id=stores.uuid 
                    where stores.uuid in ('${ids.join("','")}')`;

    return fastify.pg
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching store by id for store in mall"
        });
        repositoryErrorHandler(err);
      });
  };
  const getByIdWeb = async id => {
    fastify.log.info({
      message: "Invoking repository to fetch store by id for web"
    });
    const sql = SQL`SELECT store_name, stores.uuid, no_of_stores_allowed_max ,std_code, terms_condition, terms_conditions_additional, 
                    time_zone, allow_current_slot_booking FROM stores 
                    LEFT JOIN storeadditionalinfo ON  storeadditionalinfo.store_id=stores.uuid 
                    where stores.uuid=${id}`;
    const serviceTypesSQL = SQL`select store_id, uuid, service_type  
                    from servicetypeconfigs where store_id=${id} and status='active' `;

    const promises = [];
    promises.push(fastify.pg.query(sql));
    promises.push(fastify.pg.query(serviceTypesSQL));
    return Promise.all(promises)
      .then(results => {
        const result = results[0].rows[0];
        const resultServiceTypes = results[1];
        if (resultServiceTypes.rowCount > 0) {
          const serviceTypes = resultServiceTypes.rows;
          const serTypes = [];
          for (let i = 0; i < serviceTypes.length; i++) {
            const serviceType = serviceTypes[i].serviceType;
            const uuid = serviceTypes[i].uuid;
            const values = { uuid, serviceType };
            serTypes.push(values);
          }
          result.serviceTypes = serTypes;
        }
        return result;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching store by id for web"
        });
        repositoryErrorHandler(err);
      });
  };
  const getById = async id => {
    fastify.log.info({
      message: "Invoking repository to fetch store by id"
    });
    const sql = SQL`SELECT stores.*, std_code, terms_condition, terms_conditions_additional, time_zone FROM stores 
                    LEFT JOIN storeadditionalinfo ON  storeadditionalinfo.store_id=stores.uuid 
                    where stores.uuid=${id}`;
    const serviceTypesSQL = SQL`select store_id, uuid, service_type  
                    from servicetypeconfigs where store_id=${id} and status='active' `;

    let openTime = new Date();
    let closeTime = new Date();
    const promises = [];
    promises.push(
      fastify.slotconfigRepository.getBasedOnDays(openTime.getDay(), id)
    );
    promises.push(fastify.pg.query(sql));
    promises.push(fastify.pg.query(serviceTypesSQL));
    return Promise.all(promises)
      .then(results => {
        const slotConfigs = results[0];
        if (slotConfigs) {
          const storeOpenTime = new Date(slotConfigs[0].storeStartTime);
          const storeCloseTime = new Date(slotConfigs[0].storeEndTime);
          openTime.setUTCHours(storeOpenTime.getUTCHours());
          openTime.setUTCMinutes(storeOpenTime.getUTCMinutes());
          openTime.setSeconds(storeOpenTime.getSeconds());

          closeTime.setUTCHours(storeCloseTime.getUTCHours());
          closeTime.setUTCMinutes(storeCloseTime.getUTCMinutes());
          closeTime.setSeconds(storeCloseTime.getSeconds());
          if (closeTime.getUTCHours() <= storeOpenTime.getUTCHours()) {
            closeTime.setDate(closeTime.getDate() + 1);
          }
        } else {
          openTime = "";
          closeTime = "";
        }

        const result = results[1];
        const resultServiceTypes = results[2];
        if (result.rowCount <= 0) {
          return null;
        } else {
          const data = result.rows[0];
          data.openTime = openTime;
          data.closeTime = closeTime;

          if (resultServiceTypes.rowCount > 0) {
            const serviceTypes = resultServiceTypes.rows;
            const serTypes = [];
            for (let i = 0; i < serviceTypes.length; i++) {
              const serviceType = serviceTypes[i].serviceType;
              const uuid = serviceTypes[i].uuid;
              const values = { uuid, serviceType };
              serTypes.push(values);
            }
            data.serviceTypes = serTypes;
          }
          return data;
        }
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching store by id"
        });
        repositoryErrorHandler(err);
      });
  };

  const getByIdWithRunningSlot = async storeId => {
    fastify.log.info({
      message:
        "Invoking repository to fetch store by id with running slot and with limited data"
    });
    const sql = SQL`SELECT uuid, store_name, recommended_limit, legal_limit, occupancy_count from stores where uuid=${storeId}`;
    const promises = [];
    promises.push(fastify.pg.query(sql));
    promises.push(fastify.slotRepository.getRunningSlot(storeId));
    return Promise.all(promises)
      .then(results => {
        const result = {};
        if (results[0].rowCount > 0) {
          result.store = results[0].rows[0];
        }

        if (results[1] != null) {
          result.slot = results[1];
        }
        return result;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching store by id with limited data"
        });
        repositoryErrorHandler(err);
      });
  };

  const updateStore = async (uuid, store) => {
    fastify.log.info({
      message: "Invoking repository to put store by id"
    });

    const values = storeHelper.generateUpdateString(store);
    const sql = `UPDATE stores SET ${values} WHERE uuid = '${uuid}' RETURNING uuid, occupancy_count, recommended_limit, legal_limit;`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while updating store by id"
      });
      repositoryErrorHandler(err);
    }
  };
  const editStore = async (storeId, requestBody) => {
    fastify.log.info({
      message: "Invoking repository to edit name and location"
    });
    const location = requestBody.location;
    const name = requestBody.storeName;
    if (!storeId || !location || !name) {
      return null;
    }
    const sql = SQL`UPDATE stores SET location=${location}, store_name=${name} WHERE uuid = ${storeId};`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while updating store name and location"
      });
      repositoryErrorHandler(err);
    }
  };

  const walkIn = async (storeId, noOfPeople, type) => {
    fastify.log.info({
      message: "Invoking repository to increase occupancy"
    });
    const sql = SQL`UPDATE stores
    SET occupancy_count = stores.occupancy_count + ${noOfPeople}
    WHERE uuid = ${storeId} RETURNING uuid, occupancy_count, recommended_limit, legal_limit`;

    try {
      const result = await fastify.pg.query(sql);
      if (type === "walkin") {
        const now = new Date().toISOString();
        const customerSql = SQL`INSERT INTO walkincustomers(uuid,store_id,no_of_people,created_at, type) 
                             values(${uuidv4()}, ${storeId},${noOfPeople}, ${now}, ${type});`;
        fastify.pg.query(customerSql);
      }
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while increasing occupancy count"
      });
      repositoryErrorHandler(err);
    }
  };
  const walkOut = async (storeId, noOfPeople, type) => {
    fastify.log.info({
      message: "Invoking repository to decrease occupancy"
    });
    const sql = SQL`UPDATE stores
    SET occupancy_count = stores.occupancy_count - ${noOfPeople}
    WHERE uuid = ${storeId} RETURNING uuid, occupancy_count, recommended_limit, legal_limit`;

    if (type === "walkout") {
      try {
        const now = new Date().toISOString();
        const customerSql = SQL`INSERT INTO walkincustomers(uuid,store_id,no_of_people,created_at, type) 
                        values(${uuidv4()}, ${storeId},${noOfPeople}, ${now}, ${type});`;
        fastify.pg.query(customerSql);
      } catch (err) {
        fastify.log.error({
          err,
          message: "Request Failed while adding walkout entry"
        });
      }
    }

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while decreasing occupancy count"
      });
      repositoryErrorHandler(err);
    }
  };

  const resetOccupancy = async country => {
    fastify.log.info({
      message: "Invoking repository to reset occupancy count of each store"
    });
    const sql = SQL`UPDATE stores SET occupancy_count = 0`;
    if (country) {
      sql.append(SQL` where country=${country}`);
    } else {
      sql.append(SQL` where country != 'IN'`);
    }
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed to reset occupancy count"
      });
      repositoryErrorHandler(err);
    }
  };

  const getActiveBUs = async (country, office) => {
    fastify.log.info({
      message: "Invoking repository to get active BUs"
    });
    const sql = SQL`select DISTINCT (business_unit) from stores, slotconfigs where slotconfigs.store_id=stores.uuid and slotconfigs.is_open`;
    if (country) {
      sql.append(SQL` and country=${country}`);
    }
    if (office) {
      sql.append(SQL` and store_type='office'`);
    } else {
      sql.append(SQL` and store_type !='office'`);
    }
    try {
      const result = await fastify.pg.readreplica.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed to fetch active BUs"
      });
      repositoryErrorHandler(err);
    }
  };

  const createStore = async body => {
    fastify.log.info({
      message: "Onboarding store"
    });

    const getServices = services => {
      services = services.join(`","`);
      return `{"` + services + `"}`;
    };

    const currentDate = new Date();
    let storeId = uuidv4();
    if (body.uuid) {
      storeId = body.uuid;
    }
    const sql = SQL`insert into stores(uuid, country,business_unit,location,store_name,
                     booking_enabled,store_status,store_area,recommended_limit,legal_limit,
                     occupancy_count,staff_count,updated_at,customer_type,
                     qr_code_validity_buffer, no_of_people_allowed_default, no_of_people_allowed_max,
                     advance_booking_limit_days, store_type) 
                     values(${storeId},${body.country},${body.businessUnit}, ${
      body.location
      },
                     ${body.storeName},true,'open',${body.storeArea},${
      body.recommendedLimit
      },
                     ${body.legalLimit},${body.occupancyCount}, ${
      body.staffCount
      },
                     ${currentDate},${getServices(body.customerType)},${
      body.qrCodeValidityBuffer
      },${body.noOfPeopleAllowedDefault},
                     ${body.noOfPeopleAllowedMax}, ${
      body.advanceBookingLimitDays
      }, ${body.storeType}) RETURNING uuid`;

    const result = await fastify.pg.query(sql);
    if (result.rowCount > 0) {
      const storeId = result.rows[0].uuid;
      if (body.configs) {
        const values = body.configs.map(config => {
          const slotStartTime = new Date(config.startTime);
          const slotEndTime = new Date(config.endTime);
          const storeStartTime = new Date(config.storeStartTime);
          const storeEndTime = new Date(config.storeEndTime);
          return `('${uuidv4()}','${storeId}', '${config.customerType}'
        , '${slotStartTime.toISOString()}', '${slotEndTime.toISOString()}', ${
            config.isOpen
            }
        , ${body.staffCount}, true, ${
            config.dayOfWeek
            }, '${storeStartTime.toISOString()}', '${storeEndTime.toISOString()}')`;
        });

        const slotCongigsSql = `insert into slotconfigs(uuid, store_id,customer_type,
        start_time,end_time,is_open,staff_count,service_type_available,day_of_week,store_start_time,
        store_end_time) values ${values.join(",")};`;
        await fastify.pg.query(slotCongigsSql);
      }
      const serviceTypes = body.serviceTypes;

      if (serviceTypes) {
        const serviceTypeValues = serviceTypes.map(serviceType => {
          return `('${uuidv4()}','${storeId}','${serviceType.serviceType}',${
            serviceType.qrBookingCapacity
            },${serviceType.duration},${serviceType.walkinCapacity})`;
        });
        const serviceTypesSQL = `insert into servicetypeconfigs(uuid,store_id,service_type,qr_booking_capacity,duration,walkin_capacity) values ${serviceTypeValues.join(
          ","
        )};`;

        await fastify.pg.query(serviceTypesSQL);
      }

      if (body.timeZone) {
        const timeZone = SQL`insert into storeadditionalinfo values(${uuidv4()},${storeId},${
          body.std
          },${body.tandc},${body.timeZone});`;
        await fastify.pg.query(timeZone);
      }
      if (body.userEmail) {
        const guardUser = SQL`insert into users values(${uuidv4()},${
          body.storeName
          },${
          body.storeName
          },${body.userEmail
            .trim()
            .toLowerCase()},'guard',${storeId},${currentDate},${currentDate});`;
        await fastify.pg.query(guardUser);
      }
      return { message: "Onboarded" };
    }
  };
  const getServiceTypes = async businessUnit => {
    fastify.log.info({
      message: "Invoking repository to get service types"
    });
    const sql = SQL`select service_types from buservicemapping where business_unit=${businessUnit}`;
    return fastify.pg
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows[0];
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed to get service types of bu : ",
          businessUnit
        });
        repositoryErrorHandler(err);
      });
  };

  const getStoresWithinMall = async mallId => {
    fastify.log.info({
      message: "Invoking repository to get stores in mall"
    });
    const sql = SQL`select business_unit, stores.uuid from stores, storeadditionalinfo 
                    where stores.uuid=storeadditionalinfo.store_id and mall_id=${mallId} order by business_unit`;
    return fastify.pg
      .query(sql)
      .then(result => {
        if (result.rowCount <= 0) {
          return null;
        }
        const data = {
          stores: result.rows
        };
        return data;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed to get stores in mall : ",
          mallId
        });
        repositoryErrorHandler(err);
      });
  };
  const updateCount = async body => {
    fastify.log.info({
      message: "Invoking repository to update count"
    });
    const sql = SQL`UPDATE STORES SET automatic_center_occupancy_count=${body.count} WHERE UUID=${body.storeId}`;
    return fastify.pg
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows[0];
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while updating count"
        });
        repositoryErrorHandler(err);
      });
  };
  const getMalls = async (storeId, country) => {
    fastify.log.info({
      message: "Invoking repository to get all malls"
    });
    const sql = SQL`select business_unit, stores.uuid as mall_id, location, country, store_name from stores 
                    where store_type='mall' and country=${country}`;
    const storeMallSql = SQL`select store_id, mall_id, no_of_stores_allowed_max from storeadditionalinfo 
                    where store_id=${storeId}`;
    const promises = [];
    promises.push(fastify.pg.query(sql));
    promises.push(fastify.pg.query(storeMallSql));
    return Promise.all(promises)
      .then(results => {
        const result = results[0];
        const storeMall = results[1];
        if (result.rowCount <= 0) {
          return null;
        }
        const data = {
          malls: result.rows
        };
        if (storeMall.rowCount <= 0) {
          data.storeMall = null;
        } else {
          data.storeMall = storeMall.rows;
        }
        return data;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed to get all malls"
        });
        repositoryErrorHandler(err);
      });
  };

  const updateMall = async (storeId, body) => {
    fastify.log.info({
      message: "Invoking repository to update mall"
    });
    const mallId = body.mallId;
    const noOfStores = body.noOfStoresAllowedMax;
    const sql = SQL`update storeadditionalinfo set mall_id=${mallId}`;
    if (noOfStores) {
      sql.append(SQL` ,no_of_stores_allowed_max=${noOfStores}`);
    }
    sql.append(SQL` where store_id=${storeId}`);

    return fastify.pg
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while updating mall"
        });
        repositoryErrorHandler(err);
      });
  };
  const getStoreCurrentStatus = async body => {
    fastify.log.info({
      message: "Invoking repository to get store status"
    });
    const today = new Date();
    const day = today.getDay();
    const { location, businessUnit, storeId, country, emailId } = body;
    const storeSql = SQL`SELECT stores.uuid, country, business_unit, location, store_name, legal_limit, recommended_limit, occupancy_count, is_open, time_zone
                    from stores, slotconfigs, storeadditionalinfo
                    where stores.uuid=slotconfigs.store_id and location=${location} and day_of_week=${day}
                    and storeadditionalinfo.store_id=stores.uuid and country=${country} and business_unit=${businessUnit}`;

    const slotSql = SQL`SELECT stores.uuid, start_time, end_time
                    from stores, slots
                    where stores.uuid=slots.store_id and location=${location}
                    and country=${country} and business_unit=${businessUnit} 
                    and start_time <= now() and end_time >= now() and slots.status='active'`;

    const checkInSql = SQL`SELECT stores.uuid, count(checkin_time) as checkins
                    from stores, slots, bookings
                    where stores.uuid=slots.store_id and location=${location}
                    and country=${country} and business_unit=${businessUnit} 
                    and start_time <= now() and end_time >= now() and bookings.slot_id=slots.uuid and checkin_time is not null`;

    const checkOutSql = SQL`SELECT stores.uuid, count(checkout_time) as checkouts
                    from stores, slots, bookings
                    where stores.uuid=slots.store_id and location=${location}
                    and country=${country} and business_unit=${businessUnit} 
                    and start_time <= now() and end_time >= now() and bookings.slot_id=slots.uuid and checkout_time is not null`;

    const user = await fastify.userRepository.getUserDetail(emailId, "store");
    if (user) {
      if (user[0].storeId) {
        storeSql.append(SQL` and slotconfigs.store_id=${user[0].storeId}`);
        slotSql.append(SQL` and slots.store_id=${user[0].storeId}`);
        checkInSql.append(SQL` and slots.store_id=${user[0].storeId}`);
        checkOutSql.append(SQL` and slots.store_id=${user[0].storeId}`);
      } else if (user[0].isSuperUser) {
        if (storeId) {
          storeSql.append(SQL` and slotconfigs.store_id=${storeId}`);
          slotSql.append(SQL` and slots.store_id=${storeId}`);
          checkInSql.append(SQL` and slots.store_id=${storeId}`);
          checkOutSql.append(SQL` and slots.store_id=${storeId}`);
        }
      } else {
        let isBUAccess = false;
        let isCountryAccess = false;
        user.forEach(userRole => {
          if (userRole.country === country) {
            isCountryAccess = true;
          }
          if (userRole.businessUnit === businessUnit) {
            isBUAccess = true;
          }
        });
        if (isBUAccess && isCountryAccess) {
          if (storeId) {
            storeSql.append(SQL` and slotconfigs.store_id=${storeId}`);
            slotSql.append(SQL` and slots.store_id=${storeId}`);
            checkInSql.append(SQL` and slots.store_id=${storeId}`);
            checkOutSql.append(SQL` and slots.store_id=${storeId}`);
          }
        } else {
          return null;
        }
      }
    } else {
      return null;
    }
    checkInSql.append(SQL` group by stores.uuid`);
    checkOutSql.append(SQL` group by stores.uuid`);

    const promises = [];
    promises.push(fastify.pg.readreplica.query(storeSql));
    promises.push(fastify.pg.readreplica.query(slotSql));
    promises.push(fastify.pg.readreplica.query(checkInSql));
    promises.push(fastify.pg.readreplica.query(checkOutSql));

    return Promise.all(promises)
      .then(results => {
        const data = results[0];
        const slotData = results[1];
        if (data.rowCount <= 0) {
          return null;
        }
        const stores = data.rows;
        const slots = slotData.rows;
        const checkins = results[2].rows;
        const checkouts = results[3].rows;
        stores.forEach(store => {
          slots.forEach(slot => {
            if (slot.uuid == store.uuid) {
              if (store.startTime) {
                if (
                  store.startTime.getTime() > slot.startTime.getTime() ||
                  store.endTime.getTime() < slot.endTime.getTime()
                ) {
                  store.startTime = slot.startTime;
                  store.endTime = slot.endTime;
                }
              } else {
                store.startTime = slot.startTime;
                store.endTime = slot.endTime;
              }
            }
          });

          checkins.forEach(checkin => {
            if (checkin.uuid == store.uuid) {
              store.checkins = checkin.checkins;
            }
          });

          checkouts.forEach(checkout => {
            if (checkout.uuid == store.uuid) {
              store.checkouts = checkout.checkouts;
            }
          });
        });
        const walkinoutPromises = [];
        stores.forEach(store => {
          if (store.startTime) {
            const sql = SQL`select store_id, sum(no_of_people), type 
                        from walkincustomers
                        where store_id=${
              store.uuid
              } and created_at between ${store.startTime.toISOString()} and ${store.endTime.toISOString()}
                        group by store_id, type order by store_id`;

            walkinoutPromises.push(fastify.pg.query(sql));
          }
        });
        if (walkinoutPromises.length > 0) {
          return Promise.all(walkinoutPromises).then(walkinoutResults => {
            if (walkinoutResults.length > 0) {
              stores.forEach(store => {
                walkinoutResults.forEach(walkinout => {
                  if (walkinout.rowCount > 0) {
                    walkinout.rows.forEach(result => {
                      if (result.storeId == store.uuid) {
                        const type = result.type;
                        store[type] = result.sum;
                      }
                    });
                  }
                });
              });
            }
            return stores;
          });
        } else {
          return stores;
        }
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching current store status"
        });
        repositoryErrorHandler(err);
      });
  };
  const getBusinessUnits = async country => {
    fastify.log.info({
      message: "Invoking repository to get business units"
    });
    const sql = SQL`SELECT DISTINCT business_unit from stores`;
    if (country) {
      sql.append(SQL` where country=${country}`);
    }
    return fastify.pg.readreplica
      .query(sql)
      .then(res => {
        return res.rowCount == 0 ? null : res.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching business units"
        });
        repositoryErrorHandler(err);
      });
  };

  const getCountries = async () => {
    fastify.log.info({
      message: "Invoking repository to get countries"
    });
    const sql = SQL`SELECT DISTINCT country from stores order by country`;
    return fastify.pg
      .query(sql)
      .then(res => {
        if (res.rowCount == 0) {
          return null;
        }
        const countries = [];
        res.rows.map(country => {
          countries.push(country.country);
        });
        return { countries: countries };
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching countries"
        });
        repositoryErrorHandler(err);
      });
  };

  const getTimeZone = async (storeId) => {
    fastify.log.info({
      message: "Invoking repository to get timezone"
    });
    const sql = SQL`select time_zone from storeadditionalinfo where store_id=${storeId}`;
    return fastify.pg.readreplica
      .query(sql)
      .then(res => {
        return res.rowCount <= 0 ? null : res.rows[0];
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching timezone"
        });
        repositoryErrorHandler(err);
      });
  };

  fastify.decorate("storeRepository", {
    getAll,
    getById,
    getByIdWeb,
    getStoreByIds,
    walkIn,
    walkOut,
    updateStore,
    getAllStores,
    resetOccupancy,
    getAllUserStores,
    createStore,
    getActiveBUs,
    getServiceTypes,
    editStore,
    getByIdWithRunningSlot,
    getByIdForApp,
    getStoresWithinMall,
    getMalls,
    updateMall,
    getStoreCurrentStatus,
    getBusinessUnits,
    updateCount,
    getCountries,
    getTimeZone
  });
  next();
});
