const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");
const moment_timezone = require("moment-timezone");

const errorHandlerFactory = require("../../utilities/errorHandler");
const generateSlots = require("../../utilities/slotsUtils");
module.exports = fp((fastify, options, next) => {
  const { repositoryErrorHandler } = errorHandlerFactory(fastify);
  const getAll = async (queryParams = {}) => {
    fastify.log.info({
      message: "Invoking repository to fetch all slots"
    });

    const sql = SQL`SELECT * from slots `;
    const slotConfigSql = SQL`select start_time, end_time, is_open,day_of_week
                              from slotconfigs where store_id=${queryParams.storeid}`;

    if (queryParams.slotDate) {
      sql.append(SQL`where CAST(start_time AS DATE) = ${queryParams.slotDate}`);
    } else {
      sql.append(SQL`where start_time > CURRENT_DATE`);
    }

    if (queryParams.storeid)
      sql.append(SQL` AND store_id = ${queryParams.storeid}`);

    sql.append(SQL` AND status='active' ORDER BY start_time `);

    const promises = [];
    promises.push(fastify.pg.readreplica.query(sql));
    promises.push(fastify.pg.readreplica.query(slotConfigSql));
    return Promise.all(promises)
      .then(results => {
        const slots = results[0];
        const slotConfigs = results[1];
        if (slots.rowCount <= 0) {
          return null;
        }
        if (slotConfigs.rowCount <= 0) {
          return slots.rows;
        }
        return generateSlots.excludeSlotsAndGroupByServiceType(
          slots.rows,
          slotConfigs.rows
        );
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching slots"
        });
        repositoryErrorHandler(err);
      });
  };
  const getReservedCapacityShowUps = async (storeId) => {
    const now = new Date().toISOString();
    fastify.log.info({
      message: "Invoking repository to fetch reserved capacity and show ups"
    });
    const capacitySql = SQL`SELECT sum(reserved_capacity) as reserved_capacity, sum(show_ups) as show_ups
    from slots 
    where start_time <= ${now} and end_time >= ${now} AND store_id = ${storeId} group by store_id`;
    return fastify.pg.query(capacitySql).then(result => {
      return result.rowCount <= 0 ? null : result.rows[0];
    }).catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching reserved capacity and show ups"
      });
      repositoryErrorHandler(err);
    });
  };

  const getRunningSlot = async (storeid = {}) => {
    fastify.log.info({
      message: "Invoking repository to fetch running slot for given store"
    });

    const now = new Date().toISOString();
    const sql = SQL`SELECT uuid,store_id,reserved_capacity,show_ups,start_time,end_time 
                    from slots where start_time <= ${now} and end_time >= ${now} AND store_id = ${storeid} 
                    AND status='active' ORDER BY start_time`;

    const promises = [];
    promises.push(fastify.pg.readreplica.query(sql));
    promises.push(fastify.storeRepository.getTimeZone(storeid));
    return Promise.all(promises)
      .then(results => {
        const slots = results[0];
        const slotResult = {
          rowCount: 0
        };
        if (slots.rowCount > 0) {
          let showUps = 0;
          let reservedCapacity = 0;
          slots.rows.forEach(slot => {
            showUps += slot.showUps;
            reservedCapacity += slot.reservedCapacity;
          });
          slotResult.data = slots.rows[0];
          slotResult.data.showUps = showUps;
          slotResult.data.reservedCapacity = reservedCapacity;
          slotResult.rowCount = 1;
        }

        const openTime = new Date();
        const closeTime = new Date();
        if (results[1]) {
          const storeTime = moment_timezone.tz(new Date(), results[1].timeZone);
          openTime.setDate(storeTime.date());
          openTime.setFullYear(storeTime.year());
          openTime.setMonth(storeTime.month());

          closeTime.setDate(storeTime.date());
          closeTime.setFullYear(storeTime.year());
          closeTime.setMonth(storeTime.month());
        }

        let noData = false;
        return fastify.slotconfigRepository
          .getBasedOnDay(openTime.getDay(), storeid)
          .then(slotConfigs => {
            if (slotConfigs) {
              const storeOpenTime = new Date(slotConfigs[0].storeStartTime);
              const storeCloseTime = new Date(slotConfigs[0].storeEndTime);
              openTime.setUTCHours(storeOpenTime.getUTCHours());
              openTime.setUTCMinutes(storeOpenTime.getUTCMinutes());
              openTime.setSeconds(storeOpenTime.getSeconds() - 1);

              closeTime.setUTCHours(storeCloseTime.getUTCHours());
              closeTime.setUTCMinutes(storeCloseTime.getUTCMinutes());
              closeTime.setSeconds(storeCloseTime.getSeconds() + 1);
              if (closeTime.getUTCHours() <= openTime.getUTCHours()) {
                closeTime.setDate(closeTime.getDate() + 1);
              }
              if (slotResult.rowCount > 0) {
                const data = slotResult.data;
                if (
                  data.startTime.toISOString() >= openTime.toISOString() &&
                  data.endTime.toISOString() <= closeTime.toISOString()
                ) {
                  return data;
                } else {
                  noData = true;
                }
              }
              if (slotResult.rowCount <= 0 || noData) {
                if (
                  openTime.toISOString() <= now &&
                  closeTime.toISOString() >= now
                ) {
                  return {
                    reservedCapacity: 0,
                    showUps: 0,
                    startTime: openTime,
                    endTime: closeTime
                  };
                } else {
                  fastify.log.info({
                    message:
                      "store " +
                      storeid +
                      " is closed at " +
                      now +
                      " store open and closed time is " +
                      openTime.toISOString() +
                      closeTime.toISOString()
                  });
                  return null;
                }
              } else {
                return null;
              }
            } else {
              return null;
            }
          });
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching slots"
        });
        repositoryErrorHandler(err);
      });
  };

  const getServiceRunningSlot = async (storeid, servicetype) => {
    fastify.log.info({
      message: "Invoking repository to fetch running slot for given id"
    });

    const now = new Date().toISOString();
    const sql = SQL`SELECT * from slots where start_time <= ${now} and end_time >= ${now}`;

    if (storeid) sql.append(SQL` AND store_id = ${storeid}`);
    if (servicetype) sql.append(SQL` AND service_type = ${servicetype}`);

    sql.append(SQL` AND status='active' ORDER BY start_time limit 1`);

    try {
      const result = await fastify.pg.query(sql);

      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching slots"
      });
      repositoryErrorHandler(err);
    }
  };

  const createReservation = async (slotId, noOfPeople) => {
    fastify.log.info({
      message: "Invoking repository to reserve slot"
    });

    const sql = SQL`UPDATE slots
    SET reserved_capacity = slots.reserved_capacity + ${noOfPeople}
    where uuid=${slotId}  RETURNING show_ups, reserved_capacity, start_time, end_time, store_id;`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];

    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while reserving slot"
      });
      repositoryErrorHandler(err);
    }
  };

  const incrementShowUp = async (slotId, noOfPeople) => {
    fastify.log.info({
      message: "Invoking repository to increase no of ShowUps"
    });

    const sql = SQL`UPDATE slots
    SET show_ups = slots.show_ups + ${noOfPeople}
    where uuid=${slotId} RETURNING show_ups, reserved_capacity, start_time, end_time, store_id;`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while increasing showup count"
      });
      repositoryErrorHandler(err);
    }
  };

  const getById = async id => {
    fastify.log.info({
      message: "Invoking repository to fetch all slots"
    });
    const sql = SQL`SELECT * from slots where uuid=${id} AND status='active'`;
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching slot for the given id"
      });
      repositoryErrorHandler(err);
    }
  };

  const getByIdWithStore = async id => {
    fastify.log.info({
      message: "Invoking repository to fetch slot with store data"
    });
    const sql = SQL`SELECT slots.store_id, slots.no_of_people_allowed_max, slots.uuid, start_time, end_time,
                    store_name, business_unit, stores.uuid as store_uuid, location, time_zone, store_type,
                    occupancy_count, recommended_limit, legal_limit, country, terms_condition
                    from slots, stores, storeadditionalinfo where slots.uuid=${id} and stores.uuid=slots.store_id
                    and storeadditionalinfo.store_id=stores.uuid AND slots.status='active'`;
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching slot with store data"
      });
      repositoryErrorHandler(err);
    }
  };

  const getStoreSlot = async (
    storeId,
    startTime,
    endTime,
    serviceType,
    noOfPeople
  ) => {
    fastify.log.info({
      message: "Invoking repository to fetch current store slot"
    });
    const sql = SQL`SELECT uuid, service_type, store_id, start_time, end_time from slots 
                 where store_id=${storeId} and ((start_time >= ${startTime.toISOString()} 
                 and start_time < ${endTime.toISOString()}) or (start_time <= ${startTime.toISOString()} 
                 and end_time >= ${endTime.toISOString()})) and end_time >= now() and service_type=${serviceType}`;
    if (noOfPeople) {
      sql.append(SQL` and capacity>= reserved_capacity + ${noOfPeople}`);
    }
    sql.append(SQL` AND status='active' order by start_time asc`);

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching current store slot"
      });
      repositoryErrorHandler(err);
    }
  };
  const getNextAvailableSlot = async (storeId, serviceType, noOfPeople) => {
    fastify.log.info({
      message: "Invoking repository to fetch next available slot"
    });
    const sql = SQL`select start_time, end_time, no_of_people_allowed_default, uuid, store_id from slots 
                 where store_id=${storeId} and start_time > now() and (reserved_capacity + ${noOfPeople}) <= capacity 
                 and service_type=${serviceType} AND status='active' 
                 order by start_time limit 1`;
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching next available slot"
      });
      repositoryErrorHandler(err);
    }
  };
  const getLastCreatedRecordDate = async (id, serviceType) => {
    fastify.log.info({
      message: "Invoking repository to getLastCreatedRecordDate"
    });
    const sql = SQL`select max(end_time) from slots where store_id=${id} and service_type=${serviceType} AND status='active'`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while increasing showup count"
      });
      repositoryErrorHandler(err);
    }
  };
  const updateCapacity = async id => {
    fastify.log.info({
      message: "Invoking repository to update slot capacity"
    });

    try {
      const serviceTypes = await fastify.servicetypesRepository.getServiceTypes(
        id
      );
      if (serviceTypes) {
        const now = new Date().toISOString();
        for (let sType = 0; sType < serviceTypes.length; sType++) {
          const serviceTypeId = serviceTypes[sType].uuid;
          const capacity = serviceTypes[sType].qrBookingCapacity;
          const sql = SQL`UPDATE slots set capacity= ${capacity} where store_id=${id} and service_type_id=${serviceTypeId} and start_time >= ${now}`;
          await fastify.pg.query(sql);
        }
      }
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while increasing showup count"
      });
      repositoryErrorHandler(err);
    }
  };

  const deleteSlots = async body => {
    fastify.log.info({
      message: "Invoking repository to delete slots"
    });
    const { deletedBy, storeId, fromDate, toDate, serviceTypeId } = body;
    try {
      const user = await fastify.userRepository.getUserDetail(
        deletedBy,
        "store"
      );
      if (!user || !user[0].isSuperUser) {
        return null;
      } else {
        const fDate = new Date(fromDate);
        const tDate = new Date(toDate);

        const fSlotConfigs = await fastify.slotconfigRepository.getBasedOnDays(
          fDate.getDay(),
          storeId
        );

        const tSlotConfigs = await fastify.slotconfigRepository.getBasedOnDays(
          tDate.getDay(),
          storeId
        );
        if (fSlotConfigs && tSlotConfigs) {
          fDate.setUTCHours(fSlotConfigs[0].startTime.getUTCHours());
          fDate.setUTCMinutes(fSlotConfigs[0].startTime.getUTCMinutes());
          fDate.setSeconds(fSlotConfigs[0].startTime.getSeconds());
          fDate.setMilliseconds(0);

          tDate.setUTCHours(tSlotConfigs[0].endTime.getUTCHours());
          tDate.setUTCMinutes(tSlotConfigs[0].endTime.getUTCMinutes());
          tDate.setSeconds(tSlotConfigs[0].endTime.getSeconds());

          if (tDate.getUTCHours() <= fDate.getUTCHours()) {
            tDate.setDate(tDate.getDate() + 1);
          }
          fastify.log.info({
            message: "Deleting the slots",
            fromDate: fDate.toISOString(),
            toDate: tDate.toISOString()
          });
          const sql = SQL`update slots set status='deleted' 
                        where store_id=${storeId} and start_time>=${fDate.toISOString()} and end_time<=${tDate.toISOString()}`;
          if (serviceTypeId) {
            sql.append(SQL` and service_type_id=${serviceTypeId}`);
          }
          return fastify.pg.query(sql).then(result => {
            return result.rowCount <= 0 ? "No records found!" : result.rows;
          });
        } else {
          return null;
        }
      }
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while deleting the slots"
      });
      repositoryErrorHandler(err);
    }
  };

  const updateSlots = async body => {
    fastify.log.info({
      message: "Invoking repository to update slots"
    });
    const { slotId, capacity } = body;
    try {
      const sql = SQL`update slots set capacity=${capacity} where uuid=${slotId}`;
      return fastify.pg.query(sql).then(result => {
        return result.rowCount <= 0 ? null : result.rows;
      });
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while updating slots"
      });
      repositoryErrorHandler(err);
    }
  };

  const createSlots = async id => {
    fastify.log.info({
      message: "Invoking repository to create slots"
    });

    const stores = await fastify.storeRepository.getAllStores(id);

    for (let j = 0; j < stores.length; j++) {
      const store = stores[j];
      const numOfDays = store.advanceBookingLimitDays;
      const serviceTypes = await fastify.servicetypesRepository.getServiceTypes(
        store.uuid
      );
      if (serviceTypes) {
        for (let sType = 0; sType < serviceTypes.length; sType++) {
          const lastRecordDate = await fastify.slotRepository.getLastCreatedRecordDate(
            store.uuid,
            serviceTypes[sType].serviceType
          );

          let lastCreatedDate = new Date();
          if (
            lastRecordDate.max &&
            lastRecordDate.max.getTime() > lastCreatedDate.getTime()
          ) {
            lastCreatedDate = lastRecordDate.max;
            const utcHour = lastCreatedDate.getUTCHours();
            if (utcHour > 6) {
              lastCreatedDate.setDate(lastCreatedDate.getDate() + 1);
            }
          }

          const currentDate = new Date();

          lastCreatedDate.setUTCHours(0);
          lastCreatedDate.setUTCMinutes(0);
          lastCreatedDate.setUTCSeconds(0);
          lastCreatedDate.setUTCMilliseconds(0);
          currentDate.setUTCHours(0);
          currentDate.setUTCMinutes(0);
          currentDate.setUTCSeconds(0);
          currentDate.setUTCMilliseconds(0);

          const timeDiff = lastCreatedDate.getTime() - currentDate.getTime();
          const numOfDaysForSlot = Math.ceil(
            numOfDays - Math.abs(timeDiff / (1000 * 3600 * 24))
          );

          let day;
          const slotCreationStartDate = new Date(lastCreatedDate);
          if (numOfDaysForSlot >= 0) {
            for (let i = 0; i < numOfDaysForSlot; i++) {
              day = (lastCreatedDate.getDay() + i) % 7;

              fastify.log.info({
                message:
                  "lastCreatedDate : " +
                  slotCreationStartDate.toISOString() +
                  " Day:" +
                  day +
                  " store id : " +
                  store.uuid +
                  " advance booking days : " +
                  numOfDays +
                  " numOfDaysForSlot: " +
                  numOfDaysForSlot
              });

              const slotConfigs = await fastify.slotconfigRepository.getBasedOnDays(
                day,
                store.uuid
              );

              if (slotConfigs) {
                const insertConfigQueries = slotConfigs.map(config => {
                  const slots = generateSlots.generateSlots(
                    config,
                    slotCreationStartDate,
                    serviceTypes[sType]
                  );
                  const values = slots.map(slot => {
                    return `('${slot.uuid}',
                '${slot.storeId}',
                '${slot.startTime}',
                '${slot.endTime}',
                ${slot.duration},
                '${slot.serviceType}',
                ${slot.capacity},
                '${slot.customerType}',
                ${slot.noOfPeopleAllowedDefault}, 
                ${slot.noOfPeopleAllowedMax}, 
                ${slot.qrCodeValidityBuffer},
                '${slot.serviceTypeId}')`;
                  });
                  return `INSERT INTO slots(
                                uuid,
                                store_id,
                                start_time,
                                end_time,
                                duration,
                                service_type,
                                capacity,
                                customer_type,
                                no_of_people_allowed_default,
                                no_of_people_allowed_max,
                                qr_code_validity_buffer,
                                service_type_id
                                )
                                values ${values.join(",")}`;
                });

                try {
                  await fastify.pg.query(insertConfigQueries.join(";"));
                  fastify.log.info({
                    message: "Slots created for store : " + store.uuid
                  });
                  slotCreationStartDate.setDate(
                    slotCreationStartDate.getDate() + 1
                  );
                } catch (err) {
                  fastify.log.error({
                    err,
                    message: "Request Failed while inserting slots"
                  });
                  repositoryErrorHandler(err);
                }
              } else {
                slotCreationStartDate.setDate(
                  slotCreationStartDate.getDate() + 1
                );
                fastify.log.info({
                  message:
                    "No mapping for store id " +
                    store.uuid +
                    " in slotconfigs or store is closed on : " +
                    day
                });
              }
            }
          }
        }
      } else {
        fastify.log.info({
          message: "Slots already created for given store id : " + store.uuid
        });
      }
    }
    return { message: "Slots created" };
  };

  const createSpecificSlots = async body => {
    fastify.log.info({
      message: "Invoking repository to create specific slots"
    });
    const { storeId, fromDate } = body;
    const slotFromDate = new Date(fromDate);
    const messages = [];
    const serviceTypes = await fastify.servicetypesRepository.getServiceTypes(storeId);
    if (serviceTypes) {
      for (let sType = 0; sType < serviceTypes.length; sType++) {
        const slotConfigs = await fastify.slotconfigRepository.getBasedOnDays(
          slotFromDate.getDay(),
          storeId
        );

        if (slotConfigs) {
          const isSlotExist = await fastify.slotRepository.checkSlots(
            slotFromDate,
            serviceTypes[sType].uuid,
            slotConfigs[0].startTime,
            slotConfigs[0].endTime,
            storeId
          );
          if (!isSlotExist) {
            const insertConfigQueries = slotConfigs.map(config => {
              const slots = generateSlots.generateSlots(
                config,
                slotFromDate,
                serviceTypes[sType]
              );
              const values = slots.map(slot => {
                return `('${slot.uuid}',
                '${slot.storeId}',
                '${slot.startTime}',
                '${slot.endTime}',
                ${slot.duration},
                '${slot.serviceType}',
                ${slot.capacity},
                '${slot.customerType}',
                ${slot.noOfPeopleAllowedDefault}, 
                ${slot.noOfPeopleAllowedMax}, 
                ${slot.qrCodeValidityBuffer},
                '${slot.serviceTypeId}')`;
              });
              return `INSERT INTO slots(
                                uuid,
                                store_id,
                                start_time,
                                end_time,
                                duration,
                                service_type,
                                capacity,
                                customer_type,
                                no_of_people_allowed_default,
                                no_of_people_allowed_max,
                                qr_code_validity_buffer,
                                service_type_id
                                )
                                values ${values.join(",")}`;
            });

            try {
              await fastify.pg.query(insertConfigQueries.join(";"));
              fastify.log.info({
                message: "Slots created for store : " + storeId
              });
              const message = {
                message: "Slots created for service type " + serviceTypes[sType].serviceType,
                status: "CREATED"
              }
              messages.push(message);
            } catch (err) {
              fastify.log.error({
                err,
                message: "Request Failed while inserting slots"
              });
              repositoryErrorHandler(err);
            }
          } else {
            fastify.log.info({
              message: "Slots already exist for store : " + storeId + " on date : " + slotFromDate.toISOString()
            });
            const message = {
              message: "Slots already exist for service type " + serviceTypes[sType].serviceType,
              status: "ALREADY_EXIST"
            }
            messages.push(message);
          }
        } else {
          fastify.log.info({
            message: "Store : " + storeId + " is not configued"
          });
        }
      }
    }
    return messages;
  };

  const checkSlots = async (slotFromDate, serviceTypeId, startTime, endTime, storeId) => {
    fastify.log.info({
      message: "Invoking repository to check existing slots"
    });
    const slotStartTime = new Date(slotFromDate);
    slotStartTime.setUTCHours(startTime.getUTCHours());
    slotStartTime.setUTCMinutes(startTime.getUTCMinutes());
    slotStartTime.setSeconds(startTime.getSeconds());

    const slotEndTime = new Date(slotFromDate);
    slotEndTime.setUTCHours(endTime.getUTCHours());
    slotEndTime.setUTCMinutes(endTime.getUTCMinutes());
    slotEndTime.setSeconds(endTime.getSeconds());

    if (slotEndTime.getUTCHours() <= slotStartTime.getUTCHours()) {
      slotEndTime.setDate(slotEndTime.getDate() + 1);
    }

    const sql = SQL`select start_time from slots 
                    where store_id=${storeId} and start_time>=${slotStartTime} 
                    and end_time<=${slotEndTime} and status='active' and service_type_id=${serviceTypeId}`;
    try {
      const result = await fastify.pg.readreplica.query(sql);
      return result.rowCount <= 0 ? null : result.rowCount;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while inserting slots"
      });
      repositoryErrorHandler(err);
    }
  }
  fastify.decorate("slotRepository", {
    getAll,
    getById,
    getByIdWithStore,
    createSlots,
    getRunningSlot,
    createReservation,
    incrementShowUp,
    getLastCreatedRecordDate,
    getServiceRunningSlot,
    updateCapacity,
    getStoreSlot,
    getNextAvailableSlot,
    deleteSlots,
    updateSlots,
    getReservedCapacityShowUps,
    createSpecificSlots,
    checkSlots
  });
  next();
});
