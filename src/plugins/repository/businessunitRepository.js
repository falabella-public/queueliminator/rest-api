const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");
const { v4: uuidv4 } = require("uuid");

const errorHandlerFactory = require("../../utilities/errorHandler");
module.exports = fp((fastify, options, next) => {
    const { repositoryErrorHandler } = errorHandlerFactory(fastify);
    const getBusinessUnit = async businessUnit => {
        fastify.log.info({
            message: "Invoking repository to get all business units"
        });
        const sql = SQL`SELECT DISTINCT business_unit from buservicemapping`;
        if (businessUnit) {
            sql.append(SQL` where business_unit=${businessUnit}`);
        }
        sql.append(SQL` order by business_unit`);
        return fastify.pg
            .query(sql)
            .then(res => {
                if (res.rowCount == 0) {
                    return null;
                }
                const businessunits = [];
                res.rows.map(businessunit => {
                    businessunits.push(businessunit.businessUnit);
                });
                return { businessunits: businessunits };
            })
            .catch(err => {
                fastify.log.error({
                    err,
                    message: "Request Failed while fetching business units"
                });
                repositoryErrorHandler(err);
            });
    };

    const getBusinessUnitWithServiceTypes = async businessUnit => {
        fastify.log.info({
            message: "Invoking repository to get business unit with service types"
        });
        const sql = SQL`SELECT business_unit, service_types from buservicemapping where business_unit=${businessUnit}`;

        return fastify.pg
            .query(sql)
            .then(res => {
                return res.rowCount == 0 ? null : res.rows[0];
            })
            .catch(err => {
                fastify.log.error({
                    err,
                    message: "Request Failed while fetching business unit with service type"
                });
                repositoryErrorHandler(err);
            });
    };

    const addBusinessUnit = async request => {
        fastify.log.info({
            message: "Invoking repository to add new business units"
        });
        const serviceTypes = `{"` + request.serviceTypes.join(`","`) + `"}`
        const sql = SQL`INSERT INTO buservicemapping values(${uuidv4()}, ${request.businessUnit}, ${serviceTypes})`;
        return fastify.pg
            .query(sql)
            .then(res => {
                return res.rowCount == 0 ? null : res.rows;
            })
            .catch(err => {
                fastify.log.error({
                    err,
                    message: "Request Failed while adding business unit"
                });
                repositoryErrorHandler(err);
            });
    };

    const updateBusinessUnit = async request => {
        fastify.log.info({
            message: "Invoking repository to get all business units"
        });

        const data = await fastify.businessunitRepository.getBusinessUnitWithServiceTypes(
            request.businessUnit
        );
        if (data) {
            const existingServiceTypes = [];
            data.serviceTypes.map(serviceType => {
                existingServiceTypes.push(serviceType);
            });
            request.serviceTypes.map(serviceType => {
                existingServiceTypes.push(serviceType);
            });
            const serviceTypes = `{"` + existingServiceTypes.join(`","`) + `"}`;
            const sql = SQL`UPDATE buservicemapping set service_types=${serviceTypes} where business_unit=${request.businessUnit}`;

            return fastify.pg
                .query(sql)
                .then(res => {
                    return res.rowCount == 0 ? null : res.rows;
                })
                .catch(err => {
                    fastify.log.error({
                        err,
                        message: "Request Failed while updating business unit"
                    });
                    repositoryErrorHandler(err);
                });

        } else {
            return fastify.businessunitRepository.addBusinessUnit(request);
        }

    };


    fastify.decorate("businessunitRepository", {
        getBusinessUnit,
        getBusinessUnitWithServiceTypes,
        updateBusinessUnit,
        addBusinessUnit
    });
    next();
});
