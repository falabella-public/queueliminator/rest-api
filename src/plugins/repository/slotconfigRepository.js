const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");

const errorHandlerFactory = require("../../utilities/errorHandler");
module.exports = fp((fastify, options, next) => {
  const { repositoryErrorHandler } = errorHandlerFactory(fastify);
  const getAll = async (queryParams = {}) => {
    fastify.log.info({
      message: "Invoking repository to fetch all slotconfigs"
    });

    const sql = SQL`SELECT sconfig.*, st.no_of_people_allowed_default, 
                    st.no_of_people_allowed_max, st.qr_code_validity_buffer
                    from slotconfigs as sconfig join stores as st on sconfig.store_id= st.uuid`;

    if (queryParams.storeid)
      sql.append(
        SQL` where store_id = ${queryParams.storeid} order by sconfig.day_of_week`
      );

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching slotconfigs"
      });
      repositoryErrorHandler(err);
    }
  };

  const getBasedOnDays = async (day, storeId) => {
    fastify.log.info({
      message: "Invoking repository to fetch all slotconfigs based on days"
    });

    const sql = SQL`SELECT sconfig.*, st.no_of_people_allowed_default, 
                    st.no_of_people_allowed_max, st.qr_code_validity_buffer
                    from slotconfigs as sconfig join stores as st on sconfig.store_id= st.uuid
                    where sconfig.store_id= ${storeId} and sconfig.day_of_week = ${day} and is_open`;
    try {
      const result = await fastify.pg.readreplica.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching slotconfigs"
      });
      repositoryErrorHandler(err);
    }
  };

  const getBasedOnDay = async (day, storeId) => {
    fastify.log.info({
      message: "Invoking repository to fetch all slotconfigs based on day"
    });
    const sql = SQL`SELECT store_start_time, store_end_time, start_time, end_time from slotconfigs 
                    where store_id= ${storeId} and day_of_week = ${day} and is_open`;
    return fastify.pg.readreplica
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching slotconfigs by day"
        });
        repositoryErrorHandler(err);
      });
  };

  const update = async request => {
    fastify.log.info({
      message: "Invoking repository to insert new slotconfigs"
    });
    const { storeId, configs } = request;

    const queries = [];

    configs.map(config => {
      const {
        isOpen,
        startTime,
        endTime,
        dayOfWeek,
        storeStartTime,
        storeEndTime
      } = config;
      const query = SQL`update slotconfigs set start_time = ${startTime}, end_time = ${endTime}, 
                    is_open = ${isOpen},
                    store_start_time = ${storeStartTime}, store_end_time = ${storeEndTime}`;
      query.append(
        SQL` where store_id=${storeId} and day_of_week=${dayOfWeek}`
      );
      queries.push(fastify.pg.query(query));
    });
    return Promise.all(queries)
      .then(response => {
        return { message: "Updated Successfully" + response.rowCount };
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while inserting slots"
        });
        repositoryErrorHandler(err);
      });
  };

  fastify.decorate("slotconfigRepository", {
    getAll,
    update,
    getBasedOnDays,
    getBasedOnDay
  });
  next();
});
