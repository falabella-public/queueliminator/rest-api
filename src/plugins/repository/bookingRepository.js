const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");
const { v4: uuidv4 } = require("uuid");
const errorHandlerFactory = require("../../utilities/errorHandler");
const bookings = require("../../utilities/bookings");
const bookingConfig = require("../config/bookings.json");
const transformDateWise = require("../../utilities/bookingsUtils");
const moment = require("moment");
const moment_timezone = require("moment-timezone");

module.exports = fp((fastify, options, next) => {
  const { repositoryErrorHandler } = errorHandlerFactory(fastify);
  const getAll = async reqParams => {
    fastify.log.info({
      message: "Invoking repository to fetch all bookings"
    });

    const sql = SQL`SELECT uuid, slot_id, no_of_people,service_type from bookings where user_id = ${reqParams.userid} ORDER BY booking_time`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching bookings"
      });
      repositoryErrorHandler(err);
    }
  };

  const getLatestBooking = async reqParams => {
    fastify.log.info({
      message: "Invoking repository to fetch all bookings"
    });

    const sql = SQL`SELECT * from bookings`;

    if (reqParams.userid) {
      sql.append(SQL` where user_id = ${reqParams.userid}`);
    }

    sql.append(SQL` ORDER BY booking_time desc limit 1`);

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching bookings"
      });
      repositoryErrorHandler(err);
    }
  };

  const getActiveBookings = async reqParams => {
    fastify.log.info({
      message: "Invoking repository to fetch active bookings"
    });

    const sql = SQL`SELECT * from bookings WHERE status = 'active'`;

    if (reqParams.userid) {
      sql.append(SQL` AND user_id = ${reqParams.userid}`);
    }

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching active bookings"
      });
      repositoryErrorHandler(err);
    }
  };

  const getById = async id => {
    fastify.log.info({
      message: "Invoking repository to fetch booking details"
    });
    const sql = SQL`SELECT * from bookings where uuid=${id}`;
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching bookings"
      });
      repositoryErrorHandler(err);
    }
  };

  const checkExistingBooking = async (slotId, userId) => {
    fastify.log.info({
      message: "Invoking repository to check exisiting booking"
    });
    if (!slotId || !userId) {
      return 1;
    }
    const sql = SQL`SELECT count(uuid) as bookings from bookings where slot_id=${slotId} and user_id=${userId}`;
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? 0 : result.rows[0].bookings;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while checking existing bookings"
      });
      repositoryErrorHandler(err);
    }
  };

  const setBookingState = async (id, state) => {
    fastify.log.info({
      message: "updating state of booking"
    });

    const now = new Date().toISOString();
    let sql;
    if (state === "checkedin") {
      sql = SQL`update bookings SET status=${state}, checkin_time=${now}`;
    }
    if (state === "checkedout") {
      sql = SQL`update bookings SET status=${state}, checkout_time=${now}`;
    }
    if (state === "expired") {
      sql = SQL`update bookings SET status=${state}`;
    }
    sql.append(SQL` where uuid=${id}`);
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while updating booking state"
      });
      repositoryErrorHandler(err);
    }
  };

  const insert = async booking => {
    fastify.log.info({
      message: "Invoking repository to create a new booking"
    });
    let emailId = booking.emailId;
    if (!emailId) {
      emailId = null;
    }
    let phoneNo = booking.userTel;
    if (!phoneNo) {
      phoneNo = null;
    }
    let name = booking.userName;
    if (!name) {
      name = null;
    }
    let source = booking.source;
    if (!source) {
      source = null;
    }
    let termsConditions = booking.acceptTermsConditions;
    if (!termsConditions) {
      termsConditions = false;
    }

    let acceptUseOfMyInformation = booking.acceptUseOfMyInformation;
    if (!acceptUseOfMyInformation) {
      acceptUseOfMyInformation = false;
    }

    let acceptReadTermsConditions = booking.acceptReadTermsConditions;
    if (!acceptReadTermsConditions) {
      acceptReadTermsConditions = false;
    }

    let userIdType = booking.userIdType;
    if (!userIdType) {
      userIdType = null;
    }

    const now = new Date().toISOString();
    const sql = SQL`INSERT INTO bookings(
          uuid,
          user_id,
          slot_id,
          no_of_people,
          booking_time,
          service_type,
          customer_type,
          email_id,
          customer_name,
          customer_phone_number,
          source,
          accept_terms_conditions,
          accept_use_of_my_information,
          accept_read_terms_conditions,
          user_id_type
        )
        values (
        ${uuidv4()},
        ${booking.userId},
        ${booking.slotId}, 
        ${booking.noOfPeople},
        ${now},
        ${booking.serviceTypes},
        ${booking.customerType},
        ${emailId},
        ${name},
        ${phoneNo},
        ${source},
        ${termsConditions},
        ${acceptUseOfMyInformation},
        ${acceptReadTermsConditions},
        ${userIdType}
       ) RETURNING uuid, user_id, slot_id, no_of_people, service_type`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while creating booking"
      });
      repositoryErrorHandler(err);
    }
  };

  const getBookings = async request => {
    fastify.log.info({
      message: "Invoking repository to fetch bookings"
    });
    const openTime = new Date(request.fromDate);
    let slotStoreOpenTime;
    let slotStoreCloseTime;

    const slotconfigs = SQL` select start_time, end_time from slotconfigs where store_id=${
      request.storeid
    } and day_of_week=${openTime.getDay()}`;
    const slotconfigsResult = await fastify.pg.query(slotconfigs);
    if (slotconfigsResult.rowCount > 0) {
      slotStoreOpenTime = new Date(slotconfigsResult.rows[0].startTime);
      slotStoreCloseTime = new Date(slotconfigsResult.rows[0].endTime);
    } else {
      slotStoreOpenTime.setUTCHours(6);
      slotStoreOpenTime.setUTCMinutes(0);
      slotStoreOpenTime.setUTCSeconds(0);
      slotStoreCloseTime.setUTCHours(3);
      slotStoreCloseTime.setUTCMinutes(0);
      slotStoreCloseTime.setUTCSeconds(0);
    }
    openTime.setUTCHours(slotStoreOpenTime.getUTCHours());
    openTime.setUTCMinutes(slotStoreOpenTime.getUTCMinutes());
    openTime.setUTCSeconds(slotStoreOpenTime.getUTCSeconds());

    const closeTime = new Date(request.toDate);
    closeTime.setUTCHours(slotStoreCloseTime.getUTCHours());
    closeTime.setUTCMinutes(slotStoreCloseTime.getUTCMinutes());
    closeTime.setUTCSeconds(slotStoreCloseTime.getUTCSeconds());

    if (closeTime.getUTCHours() <= openTime.getUTCHours()) {
      closeTime.setDate(closeTime.getDate() + 1);
    }
    const totalBookings = SQL`select DATE(start_time at time zone 'utc') as day, count(bookings.slot_id) as total_bookings 
                    from bookings, slots 
                    where bookings.slot_id=slots.uuid and start_time >= ${openTime.toISOString()} 
                    and end_time <= ${closeTime.toISOString()} and slots.store_id=${
      request.storeid
    }
                    group by 1 order by 1`;

    return fastify.pg
      .query(totalBookings)
      .then(bookingsResult => {
        return bookingsResult.rowCount <= 0 ? null : bookingsResult.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching bookings"
        });
        repositoryErrorHandler(err);
      });
  };
  const getBookingDetail = async request => {
    fastify.log.info({
      message: "Invoking repository to fetch slot/booking details"
    });

    const occupancyCount = SQL` select occupancy_count, time_zone from stores, storeadditionalinfo 
                                where stores.uuid=${request.storeid} and stores.uuid=storeadditionalinfo.store_id`;

    const result = await fastify.pg.query(occupancyCount);
    const peopleInsideStore = result.rows[0];
    const timeZone = peopleInsideStore.timeZone;
    let slotStoreOpenTime = new Date();
    let slotStoreCloseTime = new Date();

    const openTime = new Date(request.fromDate);
    if (request.startTime && request.endTime) {
      slotStoreOpenTime = new Date(request.startTime);
      slotStoreCloseTime = new Date(request.endTime);
    } else {
      const slotconfigs = SQL` select store_start_time, store_end_time from slotconfigs where store_id=${
        request.storeid
      } and day_of_week=${openTime.getDay()}`;
      const slotconfigsResult = await fastify.pg.query(slotconfigs);
      if (slotconfigsResult.rowCount > 0) {
        slotStoreOpenTime = new Date(slotconfigsResult.rows[0].storeStartTime);
        slotStoreCloseTime = new Date(slotconfigsResult.rows[0].storeEndTime);
      } else {
        slotStoreOpenTime.setUTCHours(6);
        slotStoreOpenTime.setUTCMinutes(0);
        slotStoreOpenTime.setUTCSeconds(0);
        slotStoreCloseTime.setUTCHours(3);
        slotStoreCloseTime.setUTCMinutes(0);
        slotStoreCloseTime.setUTCSeconds(0);
      }
    }

    openTime.setUTCHours(slotStoreOpenTime.getUTCHours());
    openTime.setUTCMinutes(slotStoreOpenTime.getUTCMinutes());
    openTime.setUTCSeconds(slotStoreOpenTime.getUTCSeconds());

    const closeTime = new Date(request.toDate);
    closeTime.setUTCHours(slotStoreCloseTime.getUTCHours());
    closeTime.setUTCMinutes(slotStoreCloseTime.getUTCMinutes());
    closeTime.setUTCSeconds(slotStoreCloseTime.getUTCSeconds());

    if (closeTime.getUTCHours() <= openTime.getUTCHours()) {
      closeTime.setDate(closeTime.getDate() + 1);
    }

    const currentDate = new Date();
    if (closeTime.getTime() < currentDate.getTime()) {
      peopleInsideStore.occupancyCount = 0;
    }

    fastify.log.info({
      message:
        "store open and closed time : " +
        openTime.toISOString() +
        " , " +
        closeTime.toISOString()
    });

    const totalBookings = SQL`select DATE(start_time at time zone ${timeZone}) as day, count(bookings.slot_id) as total_bookings 
                    from bookings, slots 
                    where bookings.slot_id=slots.uuid and start_time >= ${openTime.toISOString()} 
                    and end_time <= ${closeTime.toISOString()} and slots.store_id=${
      request.storeid
    }
                    group by 1 order by 1`;
    const checkIns = SQL`select DATE(start_time at time zone ${timeZone}) as day, count(bookings.no_of_people) as total_checked_in
                    from bookings, slots 
                    where bookings.slot_id=slots.uuid and start_time >= ${openTime.toISOString()} 
                    and end_time <=${closeTime.toISOString()} and slots.store_id=${
      request.storeid
    }
                    and bookings.checkin_time is not null
                    group by 1 order by 1`;

    const walkins = SQL` select DATE(created_at at time zone ${timeZone}) as day , sum(no_of_people) as totalWalkins from walkincustomers 
                         where created_at between ${openTime.toISOString()} 
                         and ${closeTime.toISOString()} and store_id=${
      request.storeid
    } and type = 'walkin'
                         group by day order by day`;
    const custNames = SQL`select user_id from bookings,slots, stores 
                          where bookings.slot_id=slots.uuid and stores.uuid=slots.store_id
                          and store_type='office' and start_time >= ${openTime.toISOString()} 
                          and end_time <=${closeTime.toISOString()} and store_id=${
      request.storeid
    }`;
    const promises = [];
    promises.push(fastify.pg.query(totalBookings));
    promises.push(fastify.pg.query(checkIns));
    promises.push(fastify.pg.query(walkins));
    promises.push(fastify.pg.query(custNames));
    return Promise.all(promises)
      .then(values => {
        const bookings = values[0].rows;
        const qrCodeCheckins = values[1].rows;
        const walkin = values[2].rows;
        const customerNames = values[3].rows;
        const responseData = transformDateWise.transformDateWise(
          bookings,
          qrCodeCheckins,
          walkin
        );

        return { peopleInsideStore, responseData, customerNames };
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching bookings"
        });
        repositoryErrorHandler(err);
      });
  };

  const getAllBuBookings = async (id, queryParams) => {
    fastify.log.info({
      message: "Invoking repository to fetch booking details per BU"
    });

    let idHyphen = id;
    if (idHyphen.includes("-")) {
      idHyphen = idHyphen.replace("-", "");
    } else {
      idHyphen =
        idHyphen.substring(0, idHyphen.length - 1) +
        "-" +
        idHyphen.substring(idHyphen.length - 1, idHyphen.length);
    }
    const sql = SQL`select bookings.uuid, bookings.no_of_people, bookings.service_type, bookings.status, bookings.user_id, checkout_time, stores.business_unit, 
                    store_name, store_type, stores.uuid as store_id, start_time, end_time, slots.uuid as slot_id, time_zone
                    from bookings, slots, stores, storeadditionalinfo 
                    where bookings.slot_id=slots.uuid and slots.store_id=stores.uuid and storeadditionalinfo.store_id=stores.uuid
                    and bookings.user_id in (${id},${idHyphen})`;

    if (queryParams && queryParams.country) {
      sql.append(SQL` and country=${queryParams.country}`);
    }
    if (queryParams && queryParams.office) {
      sql.append(SQL` and store_type='office'`);
    } else {
      sql.append(SQL` and store_type !='office'`);
    }
    if (bookingConfig.expiredHourLimit) {
      const bookingStartTime = new Date();
      bookingStartTime.setUTCHours(
        bookingStartTime.getUTCHours() - bookingConfig.expiredHourLimit
      );
      sql.append(SQL` and start_time > ${bookingStartTime.toISOString()}`);
    }
    sql.append(SQL` ORDER BY start_time asc`);

    try {
      const result = await fastify.pg.query(sql);
      if (result.rowCount > 0) {
        const currentTime = new Date();
        const dataMap = new Map();
        for (let i = 0; i < result.rows.length; i++) {
          let addElement = true;
          const element = result.rows[i];
          if (element.endTime < currentTime && element.status == "active") {
            element.status = "expired";
          }
          if (element.status === "checkedout") {
            const checkoutTime = new Date(element.checkoutTime);
            if (
              !bookings.date_diff(
                checkoutTime,
                currentTime,
                bookingConfig.checkedOutHourLimit
              )
            ) {
              addElement = false;
            }
          }
          if (addElement) {
            const timeZoneDate = moment_timezone.tz(
              element.startTime,
              element.timeZone
            );
            const date = timeZoneDate.format("YYYY-MM-DD");
            if (dataMap.has(date)) {
              const values = dataMap.get(date);
              values.push(element);
            } else {
              const elements = [];
              elements.push(element);
              dataMap.set(date, elements);
            }
          }
        }

        return transformDateWise.transformMap(dataMap);
      } else {
        return null;
      }
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching bookings"
      });
      repositoryErrorHandler(err);
    }
  };

  const expirePastBookings = async () => {
    fastify.log.info({
      message: "Invoking repository to expire past bookings"
    });
    const currentDate = new Date();
    const sql = SQL`UPDATE bookings SET status='expired' 
                    from slots 
                    where slots.uuid=bookings.slot_id and bookings.status in('active','checkedin') 
                    and bookings.booking_time < ${currentDate.toISOString()}
                    and slots.end_time < ${currentDate.toISOString()}`;
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while expiring past bookings"
      });
      repositoryErrorHandler(err);
    }
  };

  const getRawBookingData = async request => {
    fastify.log.info({
      message: "Invoking repository to fetch bookings raw details"
    });

    const openTime = new Date(request.fromDate);
    const closeTime = new Date(request.toDate);
    const slotStoreOpenTime = new Date(request.startTime);
    const slotStoreCloseTime = new Date(request.endTime);

    openTime.setUTCHours(slotStoreOpenTime.getUTCHours());
    openTime.setUTCMinutes(slotStoreOpenTime.getUTCMinutes());
    openTime.setUTCSeconds(slotStoreOpenTime.getUTCSeconds());

    closeTime.setUTCHours(slotStoreCloseTime.getUTCHours());
    closeTime.setUTCMinutes(slotStoreCloseTime.getUTCMinutes());
    closeTime.setUTCSeconds(slotStoreCloseTime.getUTCSeconds());

    if (closeTime.getUTCHours() <= openTime.getUTCHours()) {
      closeTime.setDate(closeTime.getDate() + 1);
    }

    fastify.log.info({
      message:
        "store open and closed time for raw booking data download : " +
        openTime.toISOString() +
        " , " +
        closeTime.toISOString()
    });
    const country = request.selectedCountry;
    const bu = request.selectedBu;
    const historyValues = [];
    historyValues.push(`'${uuidv4()}'`);
    historyValues.push(`'bookings'`);
    if (request.userId) {
      historyValues.push(`'${request.userId}'`);
    } else {
      historyValues.push("null");
    }
    const user = await fastify.userRepository.getUserDetail(
      request.userId,
      "store"
    );
    const totalBookings = SQL`select country,business_unit,location,store_name, 
                              user_id, no_of_people, booking_time, start_time, 
                              end_time, checkin_time, checkout_time, bookings.status,
                              COALESCE(email_id,'') as email_id,COALESCE(customer_name,'') as customer_name, COALESCE(customer_phone_number,'') as customer_phone_number,
                              COALESCE(accept_terms_conditions,true) as accept_terms_conditions,  accept_use_of_my_information, accept_read_terms_conditions, bookings.service_type
                              from bookings, slots, stores 
                              where bookings.slot_id=slots.uuid and start_time >= ${openTime.toISOString()} 
                              and end_time <= ${closeTime.toISOString()} and slots.store_id=stores.uuid`;
    if (user) {
      if (user[0].isSuperUser) {
        if (country) {
          totalBookings.append(SQL` and country=${country}`);
          historyValues.push(`'${country}'`);
        } else {
          historyValues.push("null");
        }
        if (bu) {
          totalBookings.append(SQL` and business_unit=${bu}`);
          historyValues.push(`'${bu}'`);
        } else {
          historyValues.push("null");
        }
        if (request.storeid) {
          totalBookings.append(SQL` and slots.store_id=${request.storeid}`);
          historyValues.push(`'${request.storeid}'`);
        } else {
          historyValues.push("null");
        }
      } else if (user[0].storeId) {
        totalBookings.append(SQL` and slots.store_id=${user[0].storeId}`);
        historyValues.push("null");
        historyValues.push("null");
        historyValues.push(`'${user[0].storeId}'`);
      } else if (country || bu) {
        const userBusinessUnit = new Set();
        let isBUAccess = false;
        user.forEach(userRole => {
          if (userRole.country === country) {
            userBusinessUnit.add(userRole.businessUnit);
          }
          if (userRole.businessUnit === bu) {
            isBUAccess = true;
          }
        });
        if (country) {
          if (bu && isBUAccess) {
            totalBookings.append(
              SQL` and business_unit=${bu} and country=${country}`
            );
            historyValues.push(`'${country}'`);
            historyValues.push(`'${bu}'`);
          } else if (userBusinessUnit.size > 0) {
            const arrBusinessUnits = [...userBusinessUnit];
            const businessUnits = arrBusinessUnits.map(id => SQL`${id}`);
            totalBookings
              .append(SQL` and country=${country} and business_unit in (`)
              .append(totalBookings.glue(businessUnits, ","))
              .append(SQL`)`);
            historyValues.push(`'${country}'`);
            historyValues.push(`'${arrBusinessUnits.join(" ")}'`);
          } else {
            return null;
          }
        } else if (bu && isBUAccess) {
          if (country) {
            totalBookings.append(
              SQL` and business_unit=${bu} and country=${country}`
            );
            historyValues.push(`'${country}'`);
            historyValues.push(`'${bu}'`);
          } else {
            totalBookings.append(SQL` and business_unit=${bu}`);
            historyValues.push("null");
            historyValues.push(`'${bu}'`);
          }
        } else {
          return null;
        }
        if (request.storeid) {
          totalBookings.append(SQL` and slots.store_id=${request.storeid}`);
          historyValues.push(`'${request.storeid}'`);
        } else {
          historyValues.push("null");
        }
      }
    } else {
      return null;
    }
    const now = new Date();
    historyValues.push(`'${openTime.toISOString()}'`);
    historyValues.push(`'${closeTime.toISOString()}'`);
    historyValues.push(`'${now.toISOString()}'`);
    const promises = [];
    promises.push(fastify.pg.readreplica.query(totalBookings));
    return Promise.all(promises)
      .then(values => {
        const historySql = `insert into downloadhistory(uuid,download_type,user_id,country,business_unit,store_id,from_date,to_date, downloaded_at)
                             values (${historyValues.join(",")})`;
        fastify.pg.query(historySql);
        const bookings = values[0].rows;

        return { bookings };
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching bookings data"
        });
        repositoryErrorHandler(err);
      });
  };
  const getRawWalkinData = async request => {
    fastify.log.info({
      message: "Invoking repository to fetch walkin raw details"
    });

    const openTime = new Date(request.fromDate);
    const closeTime = new Date(request.toDate);
    const slotStoreOpenTime = new Date(request.startTime);
    const slotStoreCloseTime = new Date(request.endTime);

    openTime.setUTCHours(slotStoreOpenTime.getUTCHours());
    openTime.setUTCMinutes(slotStoreOpenTime.getUTCMinutes());
    openTime.setUTCSeconds(slotStoreOpenTime.getUTCSeconds());

    closeTime.setUTCHours(slotStoreCloseTime.getUTCHours());
    closeTime.setUTCMinutes(slotStoreCloseTime.getUTCMinutes());
    closeTime.setUTCSeconds(slotStoreCloseTime.getUTCSeconds());

    if (closeTime.getUTCHours() <= openTime.getUTCHours()) {
      closeTime.setDate(closeTime.getDate() + 1);
    }

    fastify.log.info({
      message:
        "store open and closed time for walkin raw data download : " +
        openTime.toISOString() +
        " , " +
        closeTime.toISOString()
    });
    const country = request.selectedCountry;
    const bu = request.selectedBu;
    let interval = 'hour';
    if(request.interval) {
      interval = request.interval;
    }
    const historyValues = [];
    historyValues.push(`'${uuidv4()}'`);
    historyValues.push(`'walkins'`);
    if (request.userId) {
      historyValues.push(`'${request.userId}'`);
    } else {
      historyValues.push("null");
    }
    const user = await fastify.userRepository.getUserDetail(
      request.userId,
      "store"
    );

    const walkins = SQL` select country,business_unit,location,store_name, date_trunc(${interval}, created_at) as walkin_at , (sum(no_of_people)::integer) as no_of_people,type 
                         from walkincustomers, stores 
                         where created_at between ${openTime.toISOString()} 
                         and ${closeTime.toISOString()} and stores.uuid=walkincustomers.store_id`;
    if (user) {
      if (user[0].isSuperUser) {
        if (country) {
          walkins.append(SQL` and country=${country}`);
          historyValues.push(`'${country}'`);
        } else {
          historyValues.push("null");
        }
        if (bu) {
          walkins.append(SQL` and business_unit=${bu}`);
          historyValues.push(`'${bu}'`);
        } else {
          historyValues.push("null");
        }
        if (request.storeid) {
          walkins.append(SQL` and store_id=${request.storeid}`);
          historyValues.push(`'${request.storeid}'`);
        } else {
          historyValues.push("null");
        }
      } else if (user[0].storeId) {
        walkins.append(SQL` and walkincustomers.store_id=${user[0].storeId}`);
        historyValues.push("null");
        historyValues.push("null");
        historyValues.push(`'${user[0].storeId}'`);
      } else if (country || bu) {
        const userBusinessUnit = new Set();
        let isBUAccess = false;
        user.forEach(userRole => {
          if (userRole.country === country) {
            userBusinessUnit.add(userRole.businessUnit);
          }
          if (userRole.businessUnit === bu) {
            isBUAccess = true;
          }
        });
        if (country) {
          if (bu && isBUAccess) {
            walkins.append(
              SQL` and business_unit=${bu} and country=${country}`
            );
            historyValues.push(`'${country}'`);
            historyValues.push(`'${bu}'`);
          } else if (userBusinessUnit.size > 0) {
            const arrBusinessUnits = [...userBusinessUnit];
            const businessUnits = arrBusinessUnits.map(id => SQL`${id}`);
            walkins
              .append(SQL` and country=${country} and business_unit in (`)
              .append(walkins.glue(businessUnits, ","))
              .append(SQL`)`);
            historyValues.push(`'${country}'`);
            historyValues.push(`'${arrBusinessUnits.join(" ")}'`);
          } else {
            return null;
          }
        } else if (bu && isBUAccess) {
          if (country) {
            walkins.append(
              SQL` and business_unit=${bu} and country=${country}`
            );
            historyValues.push(`'${country}'`);
            historyValues.push(`'${bu}'`);
          } else {
            walkins.append(SQL` and business_unit=${bu}`);
            historyValues.push("null");
            historyValues.push(`'${bu}'`);
          }
        } else {
          return null;
        }
        if (request.storeid) {
          walkins.append(SQL` and walkincustomers.store_id=${request.storeid}`);
          historyValues.push(`'${request.storeid}'`);
        } else {
          historyValues.push("null");
        }
      }
    } else {
      return null;
    }
    const now = new Date();
    historyValues.push(`'${openTime.toISOString()}'`);
    historyValues.push(`'${closeTime.toISOString()}'`);
    historyValues.push(`'${now.toISOString()}'`);
    const promises = [];

    walkins.append(
      SQL` group by country,business_unit,location,store_name, walkin_at, type`
    );
    promises.push(fastify.pg.readreplica.query(walkins));
    return Promise.all(promises)
      .then(values => {
        const historySql = `insert into downloadhistory(uuid,download_type,user_id,country,business_unit,store_id,from_date,to_date, downloaded_at)
                             values (${historyValues.join(",")})`;
        fastify.pg.query(historySql);
        const walkin = values[0].rows;
        return { walkin };
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while fetching  walkings data"
        });
        repositoryErrorHandler(err);
      });
  };
  const bookingsByBU = async request => {
    fastify.log.info({
      message: "Invoking repository to fetch BU bookings"
    });
    const fromDate = new Date(request.fromDate);
    const startTime = new Date(request.startTime);
    fromDate.setUTCHours(
      startTime.getUTCHours(),
      startTime.getUTCMinutes(),
      startTime.getUTCSeconds()
    );
    const toDate = new Date(request.toDate);
    const endTime = new Date(request.endTime);

    const { businessUnit, country, storeId, emailid } = request;
    toDate.setUTCHours(
      endTime.getUTCHours(),
      endTime.getUTCMinutes(),
      endTime.getUTCSeconds()
    );
    if (toDate.getUTCHours() <= fromDate.getUTCHours()) {
      toDate.setDate(toDate.getDate() + 1);
    }
    fastify.log.info({
      message:
        "store open and closed time for excutive summary: " +
        fromDate.toISOString() +
        " , " +
        toDate.toISOString()
    });
    const user = await fastify.userRepository.getUserDetail(emailid, "store");

    if (user) {
      const userBusinessUnit = new Set();
      let isBUAccess = false;
      user.forEach(userRole => {
        if (userRole.country === country) {
          userBusinessUnit.add(userRole.businessUnit);
        }
        if (userRole.businessUnit === businessUnit) {
          isBUAccess = true;
        }
      });

      const bookings = SQL`select stores.uuid,business_unit, store_name,  country,count(user_id) as no_of_bookings, sum(no_of_people) as no_of_people_booked
                        from bookings, slots, stores
                        where bookings.slot_id=slots.uuid and start_time >= ${fromDate.toISOString()} and end_time <= ${toDate.toISOString()}
                        and slots.store_id=stores.uuid`;

      const checkins = SQL`select stores.uuid, count(checkin_time) as checkins
                        from bookings, slots, stores
                        where bookings.slot_id=slots.uuid and start_time >= ${fromDate.toISOString()} and end_time <= ${toDate.toISOString()}
                        and slots.store_id=stores.uuid and checkin_time is not null`;

      const checkouts = SQL`select stores.uuid, count(checkout_time) as checkouts
                        from bookings, slots, stores
                        where bookings.slot_id=slots.uuid and start_time >= ${fromDate.toISOString()} and end_time <= ${toDate.toISOString()}
                        and slots.store_id=stores.uuid and checkout_time is not null`;
      const walkins = SQL`select stores.uuid, sum(no_of_people) as no_of_people_walked_in ,business_unit, store_name,  country
                        from walkincustomers, stores
                        where walkincustomers.store_id=stores.uuid and type='walkin' and created_at >= ${fromDate.toISOString()} and created_at <= ${toDate.toISOString()}`;
      const walkouts = SQL`select stores.uuid, sum(no_of_people) as no_of_people_walked_out ,business_unit, store_name,  country
                        from walkincustomers, stores
                        where walkincustomers.store_id=stores.uuid and type='walkout' and created_at >= ${fromDate.toISOString()} and created_at <= ${toDate.toISOString()}`;
      const stores = SQL`select count(uuid) from stores`;

      if (!user[0].isSuperUser) {
        if (user[0].storeId) {
          bookings.append(SQL` and stores.uuid=${user[0].storeId}`);
          checkins.append(SQL` and stores.uuid=${user[0].storeId}`);
          checkouts.append(SQL` and stores.uuid=${user[0].storeId}`);
          walkins.append(SQL` and stores.uuid=${user[0].storeId}`);
          walkouts.append(SQL` and stores.uuid=${user[0].storeId}`);
          stores.append(SQL` where stores.uuid=${user[0].storeId}`);
        } else if (country || businessUnit) {
          if (country) {
            if (businessUnit && isBUAccess) {
              bookings.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              checkins.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              checkouts.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              walkins.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              walkouts.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              stores.append(
                SQL` where business_unit = ${businessUnit} and country = ${country}`
              );
            } else if (userBusinessUnit.size > 0) {
              const arrBusinessUnits = [...userBusinessUnit];
              const businessUnits = arrBusinessUnits.map(id => SQL`${id}`);
              bookings
                .append(SQL` and country = ${country} and business_unit in (`)
                .append(bookings.glue(businessUnits, ","))
                .append(SQL`)`);
              checkins
                .append(SQL` and country = ${country} and business_unit in (`)
                .append(bookings.glue(businessUnits, ","))
                .append(SQL`)`);
              checkouts
                .append(SQL` and country = ${country} and business_unit in (`)
                .append(bookings.glue(businessUnits, ","))
                .append(SQL`)`);
              walkins
                .append(SQL` and country = ${country} and business_unit in (`)
                .append(bookings.glue(businessUnits, ","))
                .append(SQL`)`);
              walkouts
                .append(SQL` and country = ${country} and business_unit in (`)
                .append(bookings.glue(businessUnits, ","))
                .append(SQL`)`);
              stores
                .append(
                  SQL` where  country = ${country} and business_unit in (`
                )
                .append(bookings.glue(businessUnits, ","))
                .append(SQL`)`);
            } else {
              return null;
            }
          } else if (businessUnit && isBUAccess) {
            if (country) {
              bookings.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              checkins.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              checkouts.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              walkins.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              walkouts.append(
                SQL` and business_unit = ${businessUnit} and country = ${country}`
              );
              stores.append(
                SQL` where business_unit = ${businessUnit} and country = ${country}`
              );
            } else {
              bookings.append(SQL` and business_unit = ${businessUnit}`);
              checkins.append(SQL` and business_unit = ${businessUnit}`);
              checkouts.append(SQL` and business_unit = ${businessUnit}`);
              walkins.append(SQL` and business_unit = ${businessUnit}`);
              walkouts.append(SQL` and business_unit = ${businessUnit}`);
              stores.append(SQL` where business_unit = ${businessUnit}`);
            }
          } else {
            return null;
          }
          if (storeId) {
            bookings.append(SQL` and stores.uuid=${storeId}`);
            checkins.append(SQL` and stores.uuid=${storeId}`);
            checkouts.append(SQL` and stores.uuid=${storeId}`);
            walkins.append(SQL` and stores.uuid=${storeId}`);
            walkouts.append(SQL` and stores.uuid=${storeId}`);
            stores.append(SQL` and stores.uuid=${storeId}`);
          }
        } else {
          return null;
        }
      }

      bookings.append(
        SQL` group by business_unit, store_name, stores.uuid ,country`
      );
      checkins.append(SQL` group by stores.uuid`);
      checkouts.append(SQL` group by stores.uuid`);
      walkins.append(
        SQL` group by business_unit, store_name, stores.uuid ,country`
      );
      walkouts.append(
        SQL` group by business_unit, store_name, stores.uuid ,country`
      );
      const promises = [
        fastify.pg.readreplica.query(bookings),
        fastify.pg.readreplica.query(checkins),
        fastify.pg.readreplica.query(checkouts),
        fastify.pg.readreplica.query(walkins),
        fastify.pg.readreplica.query(stores),
        fastify.pg.readreplica.query(walkouts)
      ];

      return Promise.all(promises)
        .then(results => {
          return transformDateWise.transformBUWise(results);
        })
        .catch(err => {
          fastify.log.error({
            err,
            message: "Request Failed while fetching bookings and walkings data"
          });
          repositoryErrorHandler(err);
        });
    } else {
      return null;
    }
  };
  const createStoreInMallBooking = async (booking, selectedSlot) => {
    fastify.log.info({
      message: "Invoking repository to book store in mall"
    });
    const stores = booking.stores;
    const userId = booking.userId;
    const response = [];
    if (!stores || stores.length == 0) {
      fastify.log.info({
        message: "no store associated to mall"
      });
      return response;
    } else {
      const storeIds = [];
      stores.map(store => {
        storeIds.push(store.uuid);
      });

      const slotStartTime = new Date(selectedSlot.startTime);
      const slotEndTime = new Date(selectedSlot.endTime);
      slotStartTime.setUTCSeconds(slotStartTime.getUTCSeconds() - 1);

      const storesData = await fastify.storeRepository.getStoreByIds(storeIds);
      const serviceTypes = await fastify.servicetypesRepository.getStoreInMallServiceType(
        storeIds
      );

      const serviceTypeMap = new Map();
      serviceTypes.map(storeServiceType => {
        serviceTypeMap.set(
          storeServiceType.storeId,
          storeServiceType.serviceType
        );
      });
      const slotMap = new Map();
      for (let storeCount = 0; storeCount < storesData.length; storeCount++) {
        const storeData = storesData[storeCount];
        const defaultServiceType = serviceTypeMap.get(storeData.uuid);
        if (defaultServiceType) {
          const serviceType = defaultServiceType;
          fastify.log.info({
            message: "service type ",
            serviceType
          });
          const slot = await fastify.slotRepository.getStoreSlot(
            storeData.uuid,
            slotStartTime,
            slotEndTime,
            serviceType,
            booking.noOfPeople
          );
          if (slot) {
            slotMap.set(storeData.uuid, slot);
          }
        }
      }
      const differentTimeSlotMap = new Map();
      if (slotMap.size > 0) {
        slotMap.forEach((slots, storeId) => {
          if (differentTimeSlotMap.size === 0) {
            differentTimeSlotMap.set(storeId, slots[0]);
          } else if (slots.length === 1) {
            differentTimeSlotMap.set(storeId, slots[0]);
          } else {
            if (!differentTimeSlotMap.has(storeId)) {
              slots.forEach(slot => {
                if (!differentTimeSlotMap.has(storeId)) {
                  const slotStartTime = slot.startTime;
                  const slotEndTime = slot.endTime;
                  let lastSlotStartTime;
                  differentTimeSlotMap.forEach(value => {
                    if (
                      lastSlotStartTime &&
                      lastSlotStartTime < value.startTime
                    ) {
                      lastSlotStartTime = value.startTime;
                    } else if (!lastSlotStartTime) {
                      lastSlotStartTime = value.startTime;
                    }
                  });
                  if (
                    slotStartTime > lastSlotStartTime ||
                    slotEndTime <= lastSlotStartTime
                  ) {
                    differentTimeSlotMap.set(storeId, slot);
                  }
                }
              });
              if (!differentTimeSlotMap.has(storeId)) {
                differentTimeSlotMap.set(storeId, slots[0]);
              }
            }
          }
        });
      }

      for (let storeCount = 0; storeCount < storesData.length; storeCount++) {
        const storeData = storesData[storeCount];
        const defaultServiceType = serviceTypeMap.get(storeData.uuid);
        if (defaultServiceType) {
          const serviceType = defaultServiceType;
          let slot = differentTimeSlotMap.get(storeData.uuid);
          if (!slot) {
            //get next available slot
            slot = await fastify.slotRepository.getNextAvailableSlot(
              storeData.uuid,
              serviceType,
              booking.noOfPeople
            );
            if (!slot) {
              response.push({
                storeId: storeData.uuid,
                storeName: storeData.storeName,
                businessUnit: storeData.businessUnit,
                error: "NO_SLOT_AVAILABLE"
              });
            } else {
              let nextAvailableSlot;
              if (storeData.timeZone) {
                const startDate = moment.utc(slot.startTime);
                const date = moment_timezone.tz(startDate, storeData.timeZone);
                nextAvailableSlot = date.format("DD/MM/YYYY hh:mm a");
              }
              response.push({
                storeId: storeData.uuid,
                storeName: storeData.storeName,
                businessUnit: storeData.businessUnit,
                nextAvailableSlot: nextAvailableSlot,
                error: "NO_CURRENT_SLOT_AVAILABLE"
              });
            }
          } else {
            const storeId = slot.storeId;
            if (booking.noOfPeople > slot.noOfPeopleAllowedMax) {
              response.push({
                storeId: storeId,
                storeName: storeData.storeName,
                businessUnit: storeData.businessUnit,
                error: "PEOPLE_COUNT_EXCEEDE"
              });
            } else {
              //check for existing booking in same slot
              const existingBooking = await fastify.bookingRepository.checkExistingBooking(
                slot.uuid,
                userId
              );
              if (existingBooking > 0) {
                response.push({
                  storeId: storeId,
                  storeName: storeData.storeName,
                  businessUnit: storeData.businessUnit,
                  error: "USER_ALREADY_HAS_BOOKING"
                });
              } else {
                const reservationStatus = await fastify.slotRepository.createReservation(
                  slot.uuid,
                  booking.noOfPeople
                );
                if (reservationStatus === null) {
                  response.push({
                    storeId: storeId,
                    storeName: storeData.storeName,
                    businessUnit: storeData.businessUnit,
                    error: "SLOT_RESERVATION_FAILED"
                  });
                } else {
                  const storeBooking = {
                    ...booking,
                    slotId: slot.uuid,
                    serviceTypes: [serviceType]
                  };

                  const data = await fastify.bookingRepository.insert(
                    storeBooking
                  );

                  response.push({
                    ...data,
                    startTime: slot.startTime,
                    endTime: slot.endTime,
                    businessUnit: storeData.businessUnit,
                    location: storeData.location,
                    storeName: storeData.storeName,
                    storeId: storeData.uuid,
                    timeZone: storeData.timeZone,
                    storeType: storeData.storeType
                  });
                  const now = new Date().toISOString();
                  if (
                    slot.startTime.toISOString() <= now &&
                    slot.endTime.toISOString() >= now
                  ) {
                    fastify.firebaseService.updateFirebase(
                      fastify.config,
                      storeData,
                      reservationStatus
                    );
                  }
                }
              }
            }
          }
        } else {
          response.push({
            storeId: storeData.uuid,
            storeName: storeData.storeName,
            businessUnit: storeData.businessUnit,
            error: "SERVICE_TYPE_NOT_AVAILABLE"
          });
        }
      }
    }
    return response;
  };
  fastify.decorate("bookingRepository", {
    getAll,
    getById,
    insert,
    setBookingState,
    getActiveBookings,
    getLatestBooking,
    getAllBuBookings,
    expirePastBookings,
    getBookingDetail,
    getRawBookingData,
    getRawWalkinData,
    bookingsByBU,
    createStoreInMallBooking,
    checkExistingBooking,
    getBookings
  });
  next();
});
