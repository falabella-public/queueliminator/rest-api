const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");
const uuidv4 = require("uuid/v4");

const errorHandlerFactory = require("../../utilities/errorHandler");
module.exports = fp((fastify, options, next) => {
  const { repositoryErrorHandler } = errorHandlerFactory(fastify);
  const getServiceTypes = async storeId => {
    fastify.log.info({
      message: "Invoking repository to get service types"
    });
    const sql = SQL`select uuid, service_type, qr_booking_capacity, walkin_capacity, 
                        duration, default_mall_service_type from servicetypeconfigs where store_id=${storeId} and status='active'`;
    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching service types of store: ",
        storeId
      });
      repositoryErrorHandler(err);
    }
  };

  const insert = async request => {
    fastify.log.info({
      message: "Invoking repository to insert service types"
    });
    const {
      storeId,
      serviceType,
      qrBookingCapacity,
      duration,
      walkinCapacity
    } = request;
    let defaultServiceType = false;
    if (request.defaultMallServiceType) {
      defaultServiceType = request.defaultMallServiceType;
    }
    const sql = SQL`insert into servicetypeconfigs(uuid, store_id, service_type, qr_booking_capacity, walkin_capacity, duration, default_mall_service_type) 
                        values(${uuidv4()},${storeId},${serviceType}, ${qrBookingCapacity}, ${walkinCapacity}, ${duration}, ${defaultServiceType});`;
    try {
      const result = await fastify.pg.query(sql);
      fastify.slotRepository.updateCapacity(storeId);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while inserting service types of store: ",
        storeId
      });
      repositoryErrorHandler(err);
    }
  };

  const update = async request => {
    fastify.log.info({
      message: "Invoking repository to update service types"
    });
    const { storeId, serviceTypes } = request;
    const queries = [];
    serviceTypes.map(serviceType => {
      const {
        qrBookingCapacity,
        duration,
        uuid,
        walkinCapacity,
        defaultMallServiceType
      } = serviceType;
      const query = SQL`update servicetypeconfigs set qr_booking_capacity=${qrBookingCapacity}, walkin_capacity = ${walkinCapacity},
                        duration = ${duration}, default_mall_service_type= ${defaultMallServiceType}  where store_id=${storeId} and uuid=${uuid}`;
      queries.push(fastify.pg.query(query));
    });
    return Promise.all(queries)
      .then(response => {
        fastify.slotRepository.updateCapacity(storeId);
        return {
          message:
            "Updated Successfully " + JSON.stringify(response.length) + " rows."
        };
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while updating service types"
        });
        repositoryErrorHandler(err);
      });
  };
  const deleteServiceType = async request => {
    fastify.log.info({
      message: "Invoking repository to delete service types"
    });
    const now = new Date();
    const sql = SQL`update servicetypeconfigs set status='deleted', deleted_by=${request.deletedBy}, deleted_at=${now} where uuid=${request.uuid}`;
    return fastify.pg
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while deleting service types"
        });
        repositoryErrorHandler(err);
      });
  };

  const getStoreInMallServiceType = async storeIds => {
    fastify.log.info({
      message: "Invoking repository to get store in mall service type"
    });

    const sql = `SELECT service_type, store_id FROM servicetypeconfigs 
                     WHERE store_id in ('${storeIds.join(
                       "','"
                     )}') and default_mall_service_type`;
    return fastify.pg
      .query(sql)
      .then(result => {
        return result.rowCount <= 0 ? null : result.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while getting store in mall service type"
        });
        repositoryErrorHandler(err);
      });
  };
  fastify.decorate("servicetypesRepository", {
    getServiceTypes,
    update,
    insert,
    deleteServiceType,
    getStoreInMallServiceType
  });
  next();
});
