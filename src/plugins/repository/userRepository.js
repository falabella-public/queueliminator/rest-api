const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");
const uuidv4 = require("uuid/v4");

const errorHandlerFactory = require("../../utilities/errorHandler");
const tranformCountryWise = require("../../utilities/usersUtils");
module.exports = fp((fastify, options, next) => {
  const { repositoryErrorHandler } = errorHandlerFactory(fastify);

  const getUserDetail = async (email, userRole) => {
    fastify.log.info({
      message: "Invoking repository to fetch user data"
    });
    try {
      const sql = SQL`SELECT users.user_id, users.uuid, users.created_at, users.updated_at, is_super_user, store_id, country, business_unit, user_email, user_name, user_role, only_guard_user_creation_access, is_dashboard_download_access 
                      FROM users LEFT JOIN dashboardprivilege on dashboardprivilege.user_id=users.uuid and dashboardprivilege.status='active'
                      where LOWER(user_email)=LOWER(${email}) and users.status='active'`;
      if (userRole) {
        sql.append(SQL` and user_role=${userRole}`);
      }
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows;
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching user data"
      });
      repositoryErrorHandler(err);
    }
  };
  const getByEmail = async email => {
    fastify.log.info({
      message: "Invoking repository to fetch user by email"
    });

    try {
      const userData = await fastify.userRepository.getUserDetail(email);
      if (userData) {
        const countryBUs = [];

        userData.forEach(userDashboardPrivilege => {
          if (
            userDashboardPrivilege.country ||
            userDashboardPrivilege.businessUnit
          ) {
            const countryBU = {
              country: userDashboardPrivilege.country,
              businessUnit: userDashboardPrivilege.businessUnit
            };
            countryBUs.push(countryBU);
          }
        });

        const response = {
          isSuperUser: userData[0].isSuperUser,
          userEmail: userData[0].userEmail,
          userName: userData[0].userName,
          userRole: userData[0].userRole,
          storeId: userData[0].storeId,
          onlyGuardAccess: userData[0].onlyGuardUserCreationAccess,
          downloadAccess: userData[0].isDashboardDownloadAccess,
          countryBUs: countryBUs,
          uuid: userData[0].uuid,
          createdAt: userData[0].createdAt,
          updatedAt: userData[0].updatedAt,
          userId: userData[0].userId
        };

        if (userData[0].storeId) {
          const store = await fastify.storeRepository.getByIdForApp(
            userData[0].storeId
          );
          response.storeName = store.storeName;
        }

        return response;
      } else {
        return null;
      }
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching user by email"
      });
      repositoryErrorHandler(err);
    }
  };

  const getByEmailAndRole = async (email, role) => {
    fastify.log.info({
      message: "Invoking repository to fetch user by email and role"
    });
    const sql = SQL`SELECT user_email, user_role FROM users where user_email=${email} AND user_role=${role} and status='active'`;

    try {
      const result = await fastify.pg.query(sql);
      return result.rowCount <= 0 ? null : result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching user by email and role"
      });
      repositoryErrorHandler(err);
    }
  };

  const getUserStores = async email => {
    fastify.log.info({
      message: "Invoking repository to fetch user stores"
    });
    try {
      const userData = await fastify.userRepository.getUserDetail(email);

      let storeSql;
      if (userData) {
        if (userData[0].isSuperUser) {
          //user has super user role
          storeSql = `SELECT country, business_unit FROM stores`;
        } else if (userData[0].storeId) {
          //user has store only access
          storeSql = `SELECT country,business_unit
                         FROM users, stores where users.store_id=stores.uuid and LOWER(user_email)=LOWER('${email}') 
                         AND user_role='${userData[0].userRole}' and users.status='active'`;
        } else {
          //check for country and business unit level roles
          const countries = [];
          const businessUnits = [];
          const map = new Map();
          userData.forEach(userDashboardPrivilege => {
            if (userDashboardPrivilege.country) {
              if (!map.has(userDashboardPrivilege.country)) {
                countries.push(userDashboardPrivilege.country);
              }
              map.set(
                userDashboardPrivilege.country,
                userDashboardPrivilege.country
              );
            }
            if (userDashboardPrivilege.businessUnit) {
              if (!map.has(userDashboardPrivilege.businessUnit)) {
                businessUnits.push(userDashboardPrivilege.businessUnit);
              }
              map.set(
                userDashboardPrivilege.businessUnit,
                userDashboardPrivilege.businessUnit
              );
            }
          });
          if (countries.length > 0 && businessUnits.length > 0) {
            storeSql = `SELECT DISTINCT country, business_unit 
                           FROM stores 
                           where country in ('${countries.join(
                             "','"
                           )}') and business_unit in ('${businessUnits.join(
              "','"
            )}')`;
          } else if (countries.length > 0) {
            storeSql = `SELECT DISTINCT country, business_unit 
            FROM stores 
            where country in ('${countries.join("','")}')`;
          } else if (businessUnits.length > 0) {
            storeSql = `SELECT DISTINCT country, business_unit 
            FROM stores 
            where business_unit in ('${businessUnits.join("','")}')`;
          }
        }
      }

      if (storeSql) {
        storeSql += ` group by country, business_unit  order by country, business_unit`;

        const storeResult = await fastify.pg.query(storeSql);
        if (storeResult.rowCount > 0) {
          return tranformCountryWise(storeResult.rows);
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while fetching user by email and role"
      });
      repositoryErrorHandler(err);
    }
  };

  const addUser = async request => {
    fastify.log.info({
      message: "Invoking repository to add guard"
    });
    const {
      storeId,
      userName,
      userEmail,
      userRole,
      countryBU,
      onlyGuardAccess,
      downloadAccess
    } = request;
    const existingUser = SQL`select status from users where user_email=${userEmail
      .trim()
      .toLowerCase()}`;
    const existingUserResult = await fastify.pg.query(existingUser);

    let sql;
    if (existingUserResult.rowCount > 0) {
      const userStatus = existingUserResult.rows[0].status;
      if (userStatus == "active") {
        return "USER ALREADY EXISTS";
      } else {
        sql = SQL`update users set status='active', user_id=${userName}, user_name=${userName},
                  updated_at=now(), only_guard_user_creation_access=${onlyGuardAccess}, is_dashboard_download_access=${downloadAccess}`;
        if (userRole) {
          sql.append(SQL`,user_role = ${userRole.trim().toLowerCase()}`);
        } else {
          sql.append(SQL`,user_role ='guard'`);
        }

        if (storeId) {
          sql.append(SQL`,store_id = ${storeId.trim().toLowerCase()}`);
        } else {
          sql.append(SQL`,store_id = null`);
        }
        sql.append(
          SQL` where user_email = ${userEmail
            .trim()
            .toLowerCase()} RETURNING uuid`
        );
      }
    } else {
      sql = SQL`insert into users(uuid,user_id,user_name,user_email,created_at,updated_at, only_guard_user_creation_access, is_dashboard_download_access, user_role,store_id) 
      values(${uuidv4()},${userName},${userName},${userEmail
        .trim()
        .toLowerCase()},now(),now(), ${onlyGuardAccess}, ${downloadAccess}`;
      if (userRole) {
        sql.append(SQL`,${userRole.trim().toLowerCase()}`);
      } else {
        sql.append(SQL`,'guard'`);
      }

      if (storeId) {
        sql.append(SQL`,${storeId.trim().toLowerCase()}`);
      } else {
        sql.append(SQL`,null`);
      }

      sql.append(SQL`) RETURNING uuid`);
    }

    try {
      const result = await fastify.pg.query(sql);
      if (result.rowCount <= 0) {
        return null;
      }
      if (countryBU && countryBU.length > 0) {
        for (
          let countryBUCount = 0;
          countryBUCount < countryBU.length;
          countryBUCount++
        ) {
          const { country, businessUnit } = countryBU[countryBUCount];
          if (country || businessUnit) {
            const dashBoardSql = SQL`insert into dashboardprivilege(uuid,user_id,created_at,updated_at, country, business_unit) 
                                     values(${uuidv4()},${
              result.rows[0].uuid
            },now(),now()`;
            if (country) {
              dashBoardSql.append(SQL`,${country.trim()}`);
            } else {
              dashBoardSql.append(SQL`, null`);
            }
            if (businessUnit) {
              dashBoardSql.append(SQL`,${businessUnit.trim()}`);
            } else {
              dashBoardSql.append(SQL`, null`);
            }
            dashBoardSql.append(SQL`)`);

            await fastify.pg.query(dashBoardSql);
          }
        }
      }
      result.rows[0];
    } catch (err) {
      fastify.log.error({
        err,
        message: "Request Failed while adding guard"
      });
      repositoryErrorHandler(err);
    }
  };

  const getUsers = async (id, params) => {
    fastify.log.info({
      message: "Invoking repository to get store users"
    });
    const storeId = id;
    const emailId = params.emailid;

    if (!storeId || !emailId) {
      fastify.log.error({
        message: "storeId or emailId is not supplied"
      });
      return null;
    }

    const storeSql = SQL`select country, business_unit from stores where uuid=${storeId}`;
    const promises = [];
    promises.push(fastify.userRepository.getUserDetail(emailId));
    promises.push(fastify.pg.query(storeSql));
    return Promise.all(promises)
      .then(results => {
        const userResult = results[0];
        const storeResult = results[1];
        if (!userResult) {
          fastify.log.error({
            message: "Logged in user doesn't have any dashboard privileges"
          });
          return null;
        }
        const storeBu = storeResult.rows[0].businessUnit;
        const storeCountry = storeResult.rows[0].country;
        let userBu;
        let userCountry;
        for (let i = 0; i < userResult.length; i++) {
          if (userResult[i].userRole == "store") {
            if (storeBu == userResult[i].businessUnit) {
              userBu = userResult[i].businessUnit;
            }
            if (storeCountry == userResult[i].country) {
              userCountry = userResult[i].country;
            }
          }
        }

        const storeUserSql = SQL`select user_id, user_name, user_email, user_role, is_super_user, store_id, null as country, null as business_unit 
                               from users where status='active' and store_id=${storeId}`;
        const superUserSql = SQL`select user_id, user_name, user_email, user_role, is_super_user, store_id, null as country, null as business_unit 
                               from users where status='active' and is_super_user`;
        const buUserSql = SQL`select users.user_id, user_name, user_email, user_role, is_super_user, store_id, country, business_unit 
                            from users, dashboardprivilege where users.status='active' and 
                            dashboardprivilege.status='active' and dashboardprivilege.user_id=users.uuid and business_unit=${storeBu}
                            and dashboardprivilege.country is null`;
        const buCountryUserSql = SQL`select users.user_id, user_name, user_email, user_role, is_super_user, store_id, country, business_unit 
                                  from users, dashboardprivilege where users.status='active' and 
                                  dashboardprivilege.status='active' and dashboardprivilege.user_id=users.uuid and 
                                  business_unit=${storeBu} and dashboardprivilege.country = ${storeCountry}`;

        if (userResult[0].isSuperUser) {
          storeUserSql
            .append(SQL` UNION `)
            .append(superUserSql)
            .append(SQL` UNION `)
            .append(buUserSql)
            .append(SQL` UNION `)
            .append(buCountryUserSql);
        } else if (userCountry && userBu) {
          storeUserSql.append(SQL` UNION `).append(buCountryUserSql);
        } else if (userBu) {
          storeUserSql
            .append(SQL` UNION `)
            .append(buUserSql)
            .append(SQL` UNION `)
            .append(buCountryUserSql);
        }

        return fastify.pg
          .query(storeUserSql)
          .then(result => {
            return result.rows;
          })
          .catch(err => {
            fastify.log.error({
              err,
              message: "Request Failed while adding guard"
            });
            repositoryErrorHandler(err);
          });
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while adding guard"
        });
        repositoryErrorHandler(err);
      });
  };

  const deleteUser = async body => {
    fastify.log.info({
      message: "received delete user request"
    });
    const { userEmail, deletedByEmail, userRole } = body;

    if (!userEmail && !userRole && !deletedByEmail) {
      fastify.log.error({
        message: "Not a valid request"
      });
      return null;
    }
    const sql = SQL`update users set status = 'deleted', deleted_by = ${deletedByEmail}, updated_at = now() where user_email = ${userEmail}
        and user_role = ${userRole} RETURNING uuid`;

    return fastify.pg
      .query(sql)
      .then(res => {
        if (res.rowCount > 0) {
          const uuid = res.rows[0].uuid;
          const dashboardSql = SQL`update dashboardprivilege set status = 'deleted', deleted_by = ${deletedByEmail}, updated_at = now() where user_id = ${uuid}`;
          return fastify.pg.query(dashboardSql).then(dashboardRes => {
            return dashboardRes.rowCount == 0 ? res.rows : dashboardRes.rows[0];
          });
        } else {
          return res.rows;
        }
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while deleting user"
        });
        repositoryErrorHandler(err);
      });
  };
  const addBu = async body => {
    fastify.log.info({
      message: "Invoking repository to add country/bu"
    });
    const { userId, countryBUs } = body;
    const insertDashboardQueries = countryBUs.map(countryBU => {
      const country = countryBU.country ? countryBU.country : "";
      const bu = countryBU.businessUnit ? countryBU.businessUnit : "";
      return `insert into dashboardprivilege(uuid,user_id,created_at,updated_at, country, business_unit) 
              values('${uuidv4()}','${userId}',now(),now(), '${country}', '${bu}')`;
    });

    return fastify.pg
      .query(insertDashboardQueries.join(";"))
      .then(res => {
        return res.rowCount <= 0 ? null : res.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while additing country/bu"
        });
        repositoryErrorHandler(err);
      });
  };
  const deleteBu = async body => {
    fastify.log.info({
      message: "Invoking repository to delete country/bu"
    });
    const { userId, countryBUs, deletedBy } = body;
    const updateDashboardQueries = countryBUs.map(countryBU => {
      const country = countryBU.country;
      const bu = countryBU.businessUnit;
      let query = `update dashboardprivilege set status='deleted',updated_at=now(), deleted_by='${deletedBy}'
              where user_id='${userId}'`;
      if (country) {
        query += `and country='${country}'`;
      }
      if (bu) {
        query += `and business_unit='${bu}'`;
      }
      return query;
    });

    return fastify.pg
      .query(updateDashboardQueries.join(";"))
      .then(res => {
        return res.rowCount <= 0 ? null : res.rows;
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while additing country/bu"
        });
        repositoryErrorHandler(err);
      });
  };

  const getUserRoleAssociation = async (id, body) => {
    fastify.log.info({
      message: "Invoking repository to get user role association"
    });
    const { requestedBy } = body;
    const userData = await fastify.userRepository.getUserDetail(requestedBy);
    if (!userData || !userData[0].isSuperUser) {
      return null;
    }
    return fastify.userRepository
      .getUserDetail(id)
      .then(res => {
        if (res) {
          const firstRecord = res[0];
          if (firstRecord.storeId) {
            const sql = SQL`SELECT store_name, business_unit, country FROM stores where uuid=${firstRecord.storeId}`;
            return fastify.pg
              .query(sql)
              .then(storeData => {
                if (storeData.rowCount > 0) {
                  firstRecord.storeId = storeData.rows[0].storeName;
                  firstRecord.country = storeData.rows[0].country;
                  firstRecord.businessUnit = storeData.rows[0].businessUnit;
                }
                return res;
              })
              .catch(err => {
                fastify.log.error({
                  err,
                  message: "Request Failed while getting store information"
                });
                repositoryErrorHandler(err);
              });
          } else {
            return res;
          }
        } else {
          return res;
        }
      })
      .catch(err => {
        fastify.log.error({
          err,
          message: "Request Failed while getting user role association"
        });
        repositoryErrorHandler(err);
      });
  };

  fastify.decorate("userRepository", {
    getByEmail,
    getByEmailAndRole,
    getUserStores,
    addUser,
    getUsers,
    deleteUser,
    getUserDetail,
    addBu,
    deleteBu,
    getUserRoleAssociation
  });
  next();
});
