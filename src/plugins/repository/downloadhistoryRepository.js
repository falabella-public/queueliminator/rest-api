const fp = require("fastify-plugin");
const SQL = require("@nearform/sql");

const errorHandlerFactory = require("../../utilities/errorHandler");
module.exports = fp((fastify, options, next) => {
  const { repositoryErrorHandler } = errorHandlerFactory(fastify);

  const getdownloadHistory = async request => {
    fastify.log.info({
      message: "Invoking repository to get download history"
    });
    if (request.userId) {
      const user = await fastify.userRepository.getUserDetail(request.userId);

      if (user && user[0].isSuperUser) {
        const fromDate = new Date(request.fromDate);
        const toDate = new Date(request.toDate);
        const fromTime = new Date(request.startTime);
        const toTime = new Date(request.endTime);

        fromDate.setUTCHours(fromTime.getUTCHours());
        fromDate.setUTCMinutes(fromTime.getUTCMinutes());
        fromDate.setUTCSeconds(fromTime.getUTCSeconds());

        toDate.setUTCHours(toTime.getUTCHours());
        toDate.setUTCMinutes(toTime.getUTCMinutes());
        toDate.setUTCSeconds(toTime.getUTCSeconds());

        if (toDate.getUTCHours() <= fromDate.getUTCHours()) {
          toDate.setDate(toDate.getDate() + 1);
        }

        fastify.log.info({
          message:
            "start and time of download history : " +
            fromDate.toISOString() +
            " , " +
            toDate.toISOString()
        });
        const country = request.selectedCountry;
        const bu = request.selectedBu;

        const sql = SQL`SELECT * FROM downloadhistory WHERE from_date >= ${fromDate.toISOString()} AND to_date <= ${toDate.toISOString()}`;

        if (country) {
          sql.append(SQL` and country=${country}`);
        }
        if (bu) {
          sql.append(SQL` and business_unit=${bu}`);
        }
        if (request.storeid) {
          sql.append(SQL` and store_id=${request.storeid}`);
        }
        return fastify.pg.readreplica
          .query(sql)
          .then(result => {
            return result.rowCount <= 0 ? null : result.rows;
          })
          .catch(err => {
            fastify.log.error({
              err,
              message: "Request Failed while deleting service types"
            });
            repositoryErrorHandler(err);
          });
      }
      return null;
    }
    return null;
  };
  fastify.decorate("downloadhistoryRepository", {
    getdownloadHistory
  });
  next();
});
