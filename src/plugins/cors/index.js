const fastifyCors = require("fastify-cors");
const fastifyPlugin = require("fastify-plugin");
module.exports = fastifyPlugin(async fastify => {
  fastify.register(fastifyCors, {});
});
