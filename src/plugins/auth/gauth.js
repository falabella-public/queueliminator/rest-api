const cache = require("memory-cache");

const verify = async (token, userrole, fastify, client) => {
  const { WEB_CLIENT_ID, APP_CLIENT_ID, CACHE_TTL } = fastify.config;

  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: [WEB_CLIENT_ID, APP_CLIENT_ID]
  });
  const payload = ticket.getPayload();
  const emailId = payload.email;
  if (payload) {
    if (!cache.get(emailId)) {
      const user = await fastify.userRepository.getByEmailAndRole(
        emailId,
        userrole
      );
      if (!user) {
        throw new Error();
      }
      cache.put(emailId, emailId + userrole, CACHE_TTL);
    }
    return payload;
  } else {
    throw new Error();
  }
};
module.exports = { verify };
