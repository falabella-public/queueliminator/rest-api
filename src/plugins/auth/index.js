/* eslint-disable no-console */
const fp = require("fastify-plugin");
const jwt = require("jsonwebtoken");
const { OAuth2Client } = require("google-auth-library");
const microsoftAuth = require("./microsoftauth");
const gAuth = require("./gauth");

module.exports = fp(async (fastify, opts, next) => {
  const { WEB_CLIENT_ID } = fastify.config;
  const client = new OAuth2Client(WEB_CLIENT_ID);

  const pathPrefix = "/api/v1";
  const jwtValidationPaths = [
    `get:${pathPrefix}/slots/generateslots`,
    `get:${pathPrefix}/stores`,
    `get:${pathPrefix}/slots`,
    `get:${pathPrefix}/bookings`,
    `post:${pathPrefix}/bookings`,
    `get:${pathPrefix}/bookings/bookingIdPlaceholder/qr`,
    `get:${pathPrefix}/booking/latest`,
    `get:${pathPrefix}/stores/resetoccupancy`,
    `get:${pathPrefix}/bookings/expirepastbookings`,
    `post:${pathPrefix}/general/recovery`,
    `put:${pathPrefix}/stores/updatecount`
  ];

  const pathStartWith = path => {
    for (let index = 0; index < jwtValidationPaths.length; index++) {
      const element = jwtValidationPaths[index];
      if (path.startsWith(element)) {
        return true;
      }
    }
    return false;
  };

  fastify.addHook("preHandler", (req, res, done) => {
    const { authorization, userrole } = req.headers;
    const xAppengineCron = req.headers["x-appengine-cron"];
    const path = `${req.raw.method.toLowerCase()}:${req.raw.url}`;
    fastify.log.info("PATH", path);

    if (
      req.raw.method.toLowerCase() == "options" ||
      path == "post:/api/v1/general/slack_request" ||
      path == "post:/api/v1/general/slack_command" ||
      path == "post:/api/v1/general/slack/actions" ||
      path.includes("get:/api/v1/general/") ||
      xAppengineCron ||
      path.startsWith("get:/documentation")
    ) {
      done();
    } else if (authorization && authorization.length) {
      const token = authorization.split(" ")[1];
      const decodedData = jwt.decode(token);

      if (
        decodedData &&
        !(decodedData.email || decodedData.preferred_username) &&
        pathStartWith(path)
      ) {
        try {
          jwt.verify(token, fastify.config.JWT_SECRET);
          done();
        } catch (err) {
          fastify.log.error(err, "Error decoding");
          res.code(401);
          done(new Error());
        }
      } else {
        fastify.log.info("GOOGLE_OAUTH_LOGIN/MICROSOFT_OAUTH_LOGIN");
        let authClient = "";
        if (decodedData.preferred_username) {
          authClient = microsoftAuth.verify(token, userrole, fastify);
        } else if (decodedData.email) {
          authClient = gAuth.verify(token, userrole, fastify, client);
        } else {
          fastify.log.error("USER_NOT_FOUND");
          res.code(401);
          done(new Error());
        }
        return authClient
          .then(decoded => {
            req.auth = {
              email: decoded.email,
              email_verified: decoded.email_verified,
              sub: decoded.sub,
              aud: decoded.aud,
              name: decoded.name,
              iat: decoded.iat,
              jti: decoded.jti,
              at_hash: decoded.at_hash,
              isAnonymous: false,
              token
            };
          })
          .catch(err => {
            fastify.log.error(err, "USER_NOT_FOUND");
            res.code(401);
            done(new Error());
          });
      }
    } else {
      res.code(403);
      done(new Error());
    }
  });

  next();
});
