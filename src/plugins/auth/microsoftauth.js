const aadJwt = require("azure-ad-jwt");
const cache = require("memory-cache");

const verify = async (token, userrole, fastify) => {
  const promise = new Promise(function(resolve, reject) {
    aadJwt.verify(token, "null", function(err, result) {
      if (result) {
        resolve(result);
      } else {
        reject(err);
      }
    });
  });
  const result = await promise;
  const requestTid = result.tid;
  const { TID, CACHE_TTL } = fastify.config;
  if (requestTid != TID) {
    throw new Error();
  }
  const emailId = result.preferred_username.trim().toLowerCase();
  if (result) {
    if (!cache.get(emailId)) {
      const user = await fastify.userRepository.getByEmailAndRole(
        emailId,
        userrole
      );
      if (!user) {
        throw new Error();
      }
      cache.put(emailId, emailId + userrole, CACHE_TTL);
    }
    return result;
  } else {
    throw new Error();
  }
};

module.exports = { verify };
