const ApiError = require("./ApiError");
const fastifyPlugin = require("fastify-plugin");

const statusCodes = require("http").STATUS_CODES;

module.exports = fastifyPlugin(async fastify => {
  fastify.setNotFoundHandler(async (request, reply) => {
    reply.code(404).send({
      errors: [
        {
          status: "404",
          title: "Not Found"
        }
      ]
    });
  });

  fastify.setErrorHandler((error, request, reply) => {
    const statusCode =
      Array.isArray(error) && error.length
        ? error[0].status
        : error.status ||
          error.statusCode ||
          reply.statusCode ||
          reply.res.statusCode ||
          500;
    let errors = [];
    if (error.validation) {
      errors = error.validation.map(validationError => ({
        code: statusCodes["400"],
        status: 400,
        title: "Validation Error",
        detail: `${validationError.schemaPath} ${validationError.message}`
      }));
      reply.code(400).send({ errors });
      return;
    }

    if (Array.isArray(error)) {
      errors = error.map(err => ({
        id: err.id,
        code: err.code || statusCodes[statusCode + ""],
        status: err.status,
        title: err.title,
        detail: err.detail || err.message
      }));
    } else {
      errors = [
        {
          id: error.id,
          code: error.code || statusCodes[statusCode + ""],
          status: statusCode,
          title: error.title,
          detail: error.detail || error.message,
        }
      ];
    }
    reply.code(statusCode).send({ errors });
    return;
  });
  fastify.decorate("coreErrors", {
    ApiError
  });
});
