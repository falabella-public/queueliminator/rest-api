SET timezone = 'UTC';

CREATE TABLE public.stores (
"uuid" UUID NOT NULL,
"country" text NOT NULL,
"business_unit" text NOT NULL,
"location" text NOT NULL,
"store_name" text NOT NULL,
"booking_enabled" boolean NOT NULL,
"store_status" text  NOT NULL,
"store_area" integer  NOT NULL,
"recommended_limit" integer NOT NULL,
"legal_limit" integer NOT NULL,
"staff_count" integer NOT NULL,
"occupancy_count" integer NOT NULL default 0,
"customer_type" text[],
"advance_booking_limit_days" integer default 1,
"qr_code_validity_buffer" integer default 0,
"updated_at" timestamp with time zone NOT NULL,
"no_of_people_allowed_default" integer default 1,
"no_of_people_allowed_max" integer default 2,
"store_type" text,
"automatic_center_occupancy_count" integer NOT NULL default 0,
"allow_current_slot_booking" BOOLEAN default true
)

WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX stores_unique_idx ON stores (uuid, country, business_unit,store_status, store_name, location);

CREATE TABLE public.slotconfigs (
    "uuid" UUID  NOT NULL UNIQUE,
    "store_id" UUID NOT NULL,
    "customer_type" text NOT NULL,
    "start_time" timestamp with time zone NOT NULL,
    "end_time" timestamp with time zone NOT NULL,
    "is_open" BOOLEAN default true,
    "day_of_week"  integer NOT NULL default 0,
    "staff_count" integer NOT NULL default 1,
    "service_type_available" boolean NOT NULL default FALSE,
    "store_start_time" timestamp with time zone NOT NULL default now(),
    "store_end_time" timestamp with time zone NOT NULL default now()
)
WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX slotconfigs_unique_idx ON slotconfigs (uuid,store_id,start_time,end_time,store_start_time,store_end_time,day_of_week);


CREATE TABLE public.slots (
    "uuid" UUID NOT NULL UNIQUE,
    "store_id" UUID NOT NULL,
    "start_time" timestamp with time zone NOT NULL,
    "end_time" timestamp with time zone NOT NULL,
    "duration" integer NOT NULL,
    "service_type" text NOT NULL,
    "capacity" integer NOT NULL,
    "reserved_capacity" integer NOT NULL default 0,
    "customer_type" text NULL,
    "show_ups" integer NOT NULL default 0,
    "no_of_people_allowed_default" integer NOT NULL,
    "no_of_people_allowed_max" integer NOT NULL,
    "qr_code_validity_buffer" integer NOT NULL,
    "service_type_id" UUID,
    "status" text default 'active' NOT NULL
)WITH (
	OIDS=FALSE
);

CREATE INDEX slots_unique_idx ON slots (store_id,start_time,end_time, status);


CREATE TABLE public.bookings (
    "uuid" UUID NOT NULL,
    "user_id" text NOT NULL,
    "slot_id" UUID NOT NULL,
    "no_of_people" integer NOT NULL,
    "booking_time" timestamp with time zone NOT NULL,
    "checkin_time" timestamp with time zone,
    "checkout_time" timestamp with time zone,
    "customer_type" text,
    "service_type" text[],
    "status" text default 'active' NOT NULL,
    "email_id" text,
    "customer_name" text,
    "customer_phone_number" text,
    "source" text,
    "accept_terms_conditions" boolean,
    "accept_use_of_my_information" boolean default false,
    "accept_read_terms_conditions" boolean default false,
    "user_id_type" text
)WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX bookings_unique_idx ON bookings (uuid,slot_id,booking_time,status, no_of_people, checkin_time, checkout_time);

CREATE TABLE public.users (
    "uuid" UUID NOT NULL,
    "user_id" text,
    "user_name" text NOT NULL,
    "user_email" text,
    "user_role" text NOT NULL,
    "store_id" UUID,
    "is_super_user" boolean default false NOT NULL,
    "only_guard_user_creation_access" boolean default false NOT NULL,
    "is_dashboard_download_access" boolean default true NOT NULL,
    "created_at" timestamp with time zone NOT NULL,
    "updated_at" timestamp with time zone NOT NULL,
    "status" text default 'active',
    "deleted_by" text
  )WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX users_unique_idx ON users (uuid);
CREATE UNIQUE INDEX users_role_unique_idx ON users (user_email,status, user_role);

CREATE TABLE public.walkincustomers (
    "uuid" UUID NOT NULL,
    "store_id" UUID NOT NULL,
    "no_of_people" integer NOT NULL,
    "created_at" timestamp with time zone NOT NULL,
    "slot_id" UUID,
    "type" text default 'walkin'
)WITH (
	OIDS=FALSE
);
CREATE INDEX walkincustomers_store_date_idx ON walkincustomers (store_id,created_at,type);

CREATE TABLE public.storeadditionalinfo (
    "uuid" UUID NOT NULL,
    "store_id" UUID NOT NULL,
    "std_code" integer,
    "terms_condition" text,
    "time_zone" text,
    "terms_conditions_additional" text[],
    "no_of_stores_allowed_max" integer default 0,
    "mall_id" UUID NULL
)WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX storeadditionalinfo_unique_idx ON storeadditionalinfo (uuid, store_id);

CREATE TABLE public.servicetypeconfigs (
    "uuid" UUID NOT NULL,
    "store_id" UUID NOT NULL,
    "service_type" text NOT NULL,
    "qr_booking_capacity" integer NOT NULL,
    "walkin_capacity" integer NOT NULL default 0,
    "duration" integer NOT NULL,
    "status" text default 'active',
    "deleted_by" text,
    "deleted_at" timestamp with time zone,
    "default_mall_service_type" boolean default false
)WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX servicetypeconfigs_unique_idx ON servicetypeconfigs (uuid);
CREATE UNIQUE INDEX servicetypeconfigs_unique_values_idx ON servicetypeconfigs (store_id, service_type);

CREATE TABLE public.buservicemapping (
    "uuid" UUID NOT NULL,
    "business_unit" text NOT NULL,
    "service_types" text[] NOT NULL
)WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX buservicemapping_unique_idx ON buservicemapping (uuid, business_unit);

CREATE TABLE public.downloadhistory (
    "uuid" UUID NOT NULL,
    "download_type" text NOT NULL,
    "user_id" text,
    "country" text,
    "business_unit" text,
    "store_id" UUID,
    "from_date" timestamp with time zone,
    "to_date" timestamp with time zone,
    "downloaded_at" timestamp with time zone
)WITH (
	OIDS=FALSE
);

CREATE INDEX downloadhistory_idx ON downloadhistory (uuid, from_date, to_date);

CREATE TABLE public.dashboardprivilege (
    "uuid" UUID NOT NULL,
    "country" text,
    "business_unit" text,
    "status" text default 'active' NOT NULL,
    "user_id" UUID NOT NULL,
    "created_at" timestamp with time zone NOT NULL,
    "updated_at" timestamp with time zone NOT NULL,
    "deleted_by" text
)WITH (
	OIDS=FALSE
);

CREATE UNIQUE INDEX dashboardprivilege_unique_idx ON dashboardprivilege (uuid);
CREATE INDEX dashboardprivilege_idx ON dashboardprivilege (uuid, user_id, business_unit, country, status);