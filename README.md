# QueuEliminator

QueuEliminator provides restful services for book an online slot in the physical premises like stores, malls, offices etc.

### How it is started?
Due to Covid-19, Governments across the world published guidelines to follow social distancing by allowing limited occupancy in the stores, malls, offices and the places which were needed to visit for daily needs. Falabella came up with an idea to have an online booking system which can spread crowd in different timeslots during the day without losing customers. This solution provides online booking, booking validation, walk-in, walkout, store/mall/office (any physical premises) onboarding functionalities. The solution is not limited to these functionality only and provides various other APIs which can be referenced in swagger documentation. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. This will allow individual to understand the functionality of all the rest APIs. The entire repo is deployable on any server/cloud with manual and CI/CD integration.

### Prerequisites

Following things would be needed to run this project on local and on any server.

```
npm
node >= 10.0.0 and <= 12.17.0
postgreSQL
```

### Installation steps

A step by step series of examples that tell you how to get a development env running.

#### 1. Database setup
Run the SQL script mentioned in [Database Schema](db-config/migrations/001.do.create_stores_slots_bookings_tables.sql) on a postgreSQL database.

#### 2. Web server setup
Rename .env.example to .env and change all the placeholders in it. The description of each environment variables is given below - 

**.env**
```
DB_PORT=<DB_PORT> - replace <DB_PORT> with database port
DB_NAME=<DB_NAME> - replace <DB_NAME> with database name
DB_USER=<DB_USER> - replace <DB_USER> with database user name for authentication
DB_HOST=<DB_HOST> - replace <DB_HOST> with database ip address or hostname
DB_REPLICA_HOST=<DB_REPLICA_HOST> - replace <DB_REPLICA_HOST> with database replica ip address or hostname. If there is no replica then provide database ip address or hostname
DB_PASSWORD=<DB_PASSWORD> - replace <DB_PASSWORD> with database password.
DB_MAX_CONNECTION=100
HOST=0.0.0.0
PORT=<PORT> - replace <PORT> with port number at which u want to start the node server.
LOG_LEVEL=info
WEB_CLIENT_ID=<WEB_CLIENT_ID> - replace <WEB_CLIENT_ID> with gmail web client id for OAuth2 token verification.
APP_CLIENT_ID=<APP_CLIENT_ID> - replace <APP_CLIENT_ID> with gmail app client id for OAuth2 token verification
JWT_SECRET=<JWT_SECRET> - replace <JWT_SECRET> with JWT secret for basic token authentication
TID=<TID> - replace <TID> with tenant ID registered with azure authentication
AUTHENTICATION=true
IS_SWAGGER_DOC_ENABLED=true
```
`Note`:- If you have not registered your apps with Google or Azure AD, You can leave the corresponding env variables value as empty and mark `AUTHENTICATION` as `false`.

#### 3. Installing dependencies and starting web server

1. `npm install` - run this only once to install all the dependency packages.
2. `npm run dev` - this will run the application in development mode and node server will be available at http://localhost:{PORT}

### API authentication

All APIs are authenticated by basic token, gmail and azure active directory. src/plugins/auth/index.js has the specifications of all APIs. APIs added under jwtValidationPaths are protected by basic auth token and rest all protected by either gmail or azure active directory based on the token provided in authorization header of each request. In case you want to move any API from gmail/azure active directory authentication to basic then add its relative path in jwtValidationPaths array.

`Note:-` You can disable the authentication for these apis by changing the .env variable `AUTHENTICATION` to `false`.  Refer the links for detailed documentation for registering the app with [Google](https://developers.google.com/identity/sign-in/web/backend-auth) / [Azure AD](https://docs.microsoft.com/en-us/azure/api-management/api-management-howto-protect-backend-with-aad)

### Calling APIs using swagger

Open the URL [http://localhost:{PORT}/documentation](http://localhost:{PORT}/documentation) and select the API you want to hit with required parameters. These APIs can also be called using any rest client like postman and similar. If you want to disable the swagger documentation to the project, change the value of `IS_SWAGGER_DOC_ENABLED` env variable to `false`.

Note:- Replace {PORT} in above url with corresponding env variable mentioned in above steps.

### Data setup - Create a Store/Premise and Generate slots

This api is meant for setting up a store/premise with certain configurations for the slot bookings.

1. Execute `/api/v1/stores/onboardstore` API for onboarding or creating a new store/office/mall or any physical premises. Please follow the request payload mentioned in swagger documentation. Please make sure that data is populated in stores, storeadditionalinfo and slotconfigs tables.

2. After step #1 successful execution, execute `/api/v1/slots/generateslots`. This will generate slots as per serviceTypes and configs given in step #1 request payload.

### APIs sequence for booking a slot in a store

Below is the sequence to make a booking. For more APIs information please follow swagger documentation.

1. GET - `/api/v1/stores/businessunits?country=<country>` - get all BUs in a country like CL (chile), IN (India), CO (colombia) etc.
2. GET - `/api/v1/stores?businessunit=<business_unit>&source=<origin>&country=<country>` - get all locations and stores in particular BU for a country. Source is optional and being used for tracking from where the call is coming to API.
3. GET - `/api/v1/slots?storeid=<store_id>` - get all slots of a store. store_id can be obtained from #2 API response which is present as `uuid` in response.
4. POST - `/api/v1/bookings` - please follow swagger documentation for request body. Swagger doc can be found at [http://localhost:{PORT}/documentation](http://localhost:{PORT}/documentation)

## Built With

* [Node](https://nodejs.org) - Web server
* [PostgreSQL](https://www.postgresql.org/) - Database server
* [npm](https://www.npmjs.com/) - Dependency Management

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/falabella-public/queueliminator/rest-api/-/tags). 

## Authors

**Falabella** - *Initial work* - [QueuEliminator](https://gitlab.com/falabella-public/queueliminator)

See also the list of [contributors](https://gitlab.com/falabella-public/queueliminator/rest-api/-/project_members) who participated in this project.

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
